---
layout: post
title: Un bon VPS pour pas cher
date: '2014-01-13 13:00:53'
tags:
- server
- vps
- kvm
---

![logo-1](/content/images/2017/08/logo-1.png)

J’étais a la recherche d’un VPS pas cher installé en France afin de pouvoir faire un proxy sock avec SSH pour pouvoir accéder a certain contenu.

J’ai eut l’agréable surprise de découvrir [pulseheberg.com](http://pulseheberg.com/)! Ils proposent des [VPS](http://pulseheberg.com/vps-lc.php), des [KVM](http://pulseheberg.com/vps-kvm.php), des serveurs dédiés ainsi que des serveurs [Minecraft](https://minecraft.net/), [Counter strike](http://blog.counter-strike.net/) etc…

 

J’ai pris un **VPS L** avec a l’origine 2 coeurs (2.5 GHz), 2 Go de Ram et 200 Go de d’espace dique le tout pour **9 euros par mois seulement**.

Par chance je suis tombé sur une promotion et je me retrouve avec **4 coeurs, 4 Go de Ram et 400 Go d’espace disque**!

 

De plus le service est vraiment agréable et rapide grâce a leur [compte twitter](https://twitter.com/Pulseheberg). L’entreprise a ouvert en Février 2013 d’après les informations que j’ai trouvé sur le net, mais vous pouvez y aller sans crainte.

Si vous êtes intéressé utilisez [ce lien pour commander votre VPS](http://www.pulseheberg.com/clients/aff.php?aff=090)!