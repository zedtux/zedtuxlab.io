---
layout: post
title: Stoquez vos containers sur Quay.io
date: '2014-05-25 12:17:57'
tags:
- docker
---

Si vous venez de créer votre première image Docker, peut-être voudriez-vous expédier votre container dans un dépôt ?

Ceci est faisable dans des dépôt publique et même privé.
Un excellent service est [Quay.io](https://quay.io/).

Vous pouvez créer autant de dépôts publique que vous désirez, par contre si vous désirez créer un dépôt privé vous devrez payer $12 par mois comme précisé dans [leur page de prix](https://quay.io/plans/).

Docker.io vous proposent aussi un service dépôt pour vos images Docker.
Il est aussi gratuit pour les images docker publique et payant pour les dépôts privé.
Par contre il est moins cher que Quay.io puisqu'il vous en coutera que $7 par mois comme décrit dans [leur pages de prix](https://index.docker.io/plans/).

## brewformulas.org

Dans l'exemple que j'utilise dans mon article [Containériser avec Docker](http://blog.zedroot.org/containeriser-avec-docker/), j'ai créé une image principale qui contient le code source et les gems, puis 2 tagues.

Je vais vous expliquer comment les expédier sur Quay.io.

### Création du dépôt

Pour commencer il faut créer un dépôt sur Quay.io.

Une fois que vous vous êtes inscrit, cliquez le bouton **Create a new repository**.

Séléctionnez l'option **Public** pour la section **Repository Visibility**.
Ensuite séléctionnez **(Empty repository)** pour la section **Initialize repository**.

(J'ai essayé d'utiliser l'option **Initialize from a Dockerfile**, qui permet de uniquement copier/coller le contenu d'un Dockerfile, puis un serveur va exécuter la construction du fichier, mais la disponibilité de ces serveurs pour exécuter votre Dockerfile étant apparemment trop faible, j'ai attendu un petit moment sans jamais voir mon fichier pris en compte, donc je préfère le faire moi même.)

### Préparation pour l'envoie

Avant de pouvoir envoyer notre image, il faut:

 - Se connecter à Quay.io dans la ligne de commande. Donc ouvrez un terminal, puis exécutez ceci: `sudo docker login quay.io`
 - Taguer vos images pour Quay.io.


Pour taguer les images créé pour brewformulas.org, il faut exécuter ces lignes:

	sudo docker tag zedtux/brewformulas.org quay.io/zedtux/brewformulas.org
    sudo docker tag zedtux/brewformulas.org:web quay.io/zedtux/brewformulas.org:web
    sudo docker tag zedtux/brewformulas.org:worker quay.io/zedtux/brewformulas.org:worker

La première ligne tag l'image principale, puis les deux suivante tague les tagues **web** et **worker**.

Maintenant si vous regarder l'état de vos images docker:

<script src="https://gist.github.com/zedtux/c72a43af8f91e4a8dacb.js"></script>

### Envoie de vos images sur Quay.io

Maintenant que vous êtes prêt, nous allons envoyer nos images:

	sudo docker push quay.io/zedtux/brewformulas.org
    sudo docker push quay.io/zedtux/brewformulas.org:web
    sudo docker push quay.io/zedtux/brewformulas.org:worker

Ça peux prendre un petit moment selon la taille de vos images, donc prenez bien le temps de vérifier que vos images fonctionne correctement :-)

Une fois l'envoie terminée, si vous vous rendez sur quay.io dans le dépôt vous devriez voire quelque chose comme cela:

![quay-io_zedtux-1](/content/images/2017/08/quay-io_zedtux-1.png)


Maintenant vous pouvez utiliser ce dépôt pour faire un déploiement ou pour partager votre image avec vos amis.