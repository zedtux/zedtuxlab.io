---
layout: post
title: Trouver ou découvrir des formules Homebrew (OSX)
date: '2014-01-14 18:57:43'
tags:
- ruby
- mes-developpements
---

Si vous êtes un utilisateur d'[OS X](http://www.apple.com/osx/) d'[Apple](http://www.apple.com/) et que vous êtes un développeur, peut-être utilisez vous [Homebrew](http://brew.sh/) ?

Lorsque vous faites un `brew update` ne vous êtes vous jamais interrogé sur ces nouvelles formules avec parfois un nom assez particulier ?

Bien entendu, une option est d'exécuter `brew info` suivit du nom de la formule, mais vous n'obtiendrez que les instructions d'installation/configuration si vous êtes chanceux, autrement vous n'aurez que l'URL de la page du site de la formule.

## BrewFormulas.org

[BrewFormulas.org](http://brewformulas.org/) est le dernier site en [Ruby On Rails](http://rubyonrails.org/) que j'ai développé et qui a pour but de fournir la liste complete des formules disponible, une recherche, et surtout une description plus clair des formules.

### A2ps, un exemple

Si vous vous rendez sur à l'adresse http://brewformulas.org/A2ps vous verrez la fiche descriptive de la formule A2ps.

Cette fiche est composé d'une description récupéré automatiquement (parfois moins bien que d'autres) depuis l'adresse contenue dans la formule.

Ensuite vous retrouvez l'URL de la page web de la formula ainsi que la version de la formule.

Et finalement vous trouverez les dependences et les confits.
Ici vous pourrez naviguer très simplement en cliquant sur leur noms.


## Conclusion

En conclusion ce site vous serviera lorsque vous voudrez prendre quelques minutes pour explorer l'existant et découvrir des formules.

Toutes les fonctionnalités sont implémenté et seule des améliorations sont prévue comme améliorer la récupération de descriptions depuis la page web de la formule, ou encore le thême.