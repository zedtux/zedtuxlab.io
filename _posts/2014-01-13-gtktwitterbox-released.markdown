---
layout: post
title: GtkTwitterBox released!
date: '2014-01-13 12:55:28'
tags:
- python
- my-developments
---

During the development of my application layer firewall named Douane, I developed a [Gtk](http://www.gtk.org/) 3 widget to display the latest tweets from the [douaneapp Twitter account](https://twitter.com/douaneapp).

I wrote it in [python](http://www.python.org/) 3 and published it on [Github](https://github.com/). You can find the code at https://github.com/zedtux/gtktwitterbox.