---
layout: post
title: Fig/docker-compose clean old "run" containers
date: '2015-05-28 18:42:08'
tags:
- docker
---

In the case you're using either [fig](http://www.fig.sh/) or [docker-compose](http://docs.docker.com/compose/), after a while, you will have a lot of old containers created when using the `run` command.

You can clean them all with the following command:

    docker rm `docker ps --no-trunc -aq`

This command will try to remove all the containers but will fail on running once.

#### See also

You shoudl also have a look at the my article to [clean the `<none>` containers](http://blog.zedroot.org/remove-docker-images-no-more-used/).