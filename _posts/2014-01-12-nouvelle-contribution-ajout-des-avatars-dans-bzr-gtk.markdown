---
layout: post
title: 'Nouvelle contribution: Ajout des avatars dans bzr-gtk'
date: '2014-01-12 20:26:30'
tags:
- vcs
- bazaar
- contribution
- mes-developpements
- my-developments
---

![bzr_gtk_gravatar](/content/images/2017/08/bzr_gtk_gravatar.png)

Depuis la semaine dernière je travaillais pour me détendre sur l’ajout des avatars dans [bzr-gtk](https://launchpad.net/bzr-gtk) (Outil de visualisation de l’historique d’un dépôt [Bazaar](http://bazaar.canonical.com/)).

Mon merge request a été accepté hier soir. Cette fonctionnalité sera donc intégré dans la prochaine version! :) 