---
layout: post
title: "'$ rails s' ne démarre pas le serveur et affiche l'aide"
date: '2014-05-01 09:33:02'
---

Pour démarrer le serveur Rails d'une application Ruby On Rails il faut exécuter:

    $ rails s

Cependant il se peux que lorsque vous le ferez vous vous retrouviez avec la documentation de Rails au lieu du démarrage du serveur:

    Usage:
      rails new APP_PATH [options]
      
    ...

Ici le probleme est que le dossier bin/ est ignoré par git (regardez dans le fichier .gitignore), ce qui est normal car versionner les fichiers binaires n'est pas recommandé, et de plus l'or du déploiement, ces fichiers binaires seront regénéré.

Il faut donc regénérer le binaire de rails avec la commande suivante (trouvé sur [ce lien](http://rubyjunction.us/if-you-get-a-usage-printout-when-you-are-trying-to-run--rails-s-)):

    bundle exec rails new .

Et répondez 'n' (non) a toutes les questions.

Maintenant le démarrage du serveur fonctionnera.