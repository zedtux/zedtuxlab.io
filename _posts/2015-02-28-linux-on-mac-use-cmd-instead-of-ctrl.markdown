---
layout: post
title: 'Linux On Mac: Use CMD as of on OSX'
date: '2015-02-28 18:27:33'
tags:
- linux-on-mac
---

On OS X, you're always using the `CMD` (or command) key in order to copy/paste, launch spotlight or switch applications.
On Linux, the `CMD` key is considered as the `Super` key (coming from Windows) and it is poorly used.
An OSX user would be disappointed to have to use the `CTRL` key instead of `CMD`, especially that this key is used in 80% of the time.

#### Switch CTRL and CMD

The first thing to do is to switch those keys so that you will be able to copy / paste for example.

Open the GNOME Tweak tool application and go to the **Typing** section where you have to change the **Alt/Win key behavior** as following:

![gnome-tweak_typing_alt-win-key](/content/images/2017/08/gnome-tweak_typing_alt-win-key.png)

Now you can use the `CMD` key instead of `CTRL` everywhere. : )

#### Change the application switch accelerator

On OSX, the `CMD` key is used also to switch applications.

To have the same behavior on GNOME, open the Settings window, and go to the **Shortcuts** tab of the *Keyboard* panel.

Now select **Navigation** and re-define the **Switch applications** shortcut by double clicking the line and then pressing `CMD` + `Tab`.

![GNOME-change-switch-application-shortcut](/content/images/2017/08/GNOME-change-switch-application-shortcut.png)

Now you should feel like at home : )