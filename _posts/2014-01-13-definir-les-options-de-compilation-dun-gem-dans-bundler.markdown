---
layout: post
title: Définir les options de compilation d’un gem dans Bundler
date: '2014-01-13 13:01:54'
tags:
- ruby
---

Cet article est un memo pour moi.

Lorsque certain gem ont un script de compilation comme **postgres** or **pg**, il peux arriver que la compilation se termine en erreur car le chemin d’accès à `pg_config` n’est pas le même que celui qu’utilise le script de compilation par défaut.

Afin de régler ce problème il y a plusieurs solution mais une que j’ai trouvé intéressante est de pouvoir enregistrer dans Bundler une option de compilation (un drapeau avec une très mauvaise traduction) pour un gem en particulier qui se fait de la façon suivante:
	
	bundle config build.postgres --with-pg-config=/usr/local/bin/pg_config

De cette façon lorsque le gem postgres exécutera l’étape de compilation, l’option de compilation `--with-pg-config=/usr/local/bin/pg_config` sera passé au script configure.