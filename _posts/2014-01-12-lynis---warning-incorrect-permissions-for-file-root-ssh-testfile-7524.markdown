---
layout: post
title: 'Lynis – Warning: Incorrect permissions for file /root/.ssh [test:FILE-7524]'
date: '2014-01-12 20:48:10'
tags:
- linux
- server
- security
---

If Lynis list the following warning:

> Warning: Incorrect permissions for file /root/.ssh [test:FILE-7524]

you will fix it with the following command line:

	sudo chmod 700 /root/.ssh