---
layout: post
title: Testing a DateTime field from a Rails API with RSpec
date: '2018-03-07 13:30:34'
tags:
- rails
- testing
---

You are testing a simple API returning object's attributes including the `created_at` and the `updated_at` fields:

<script src="https://gist.github.com/zedtux/3107ac6deb1401c7b81763ac16613d2e.js"></script>

You write a simple RSpec test:

<script src="https://gist.github.com/zedtux/6c53d9b304b26acd68c0cf4066ab6927.js"></script>

But you got an issue comparing the dates:

```
      -  "created_at"=>"2018-03-07 14:22:36.199832700 +0100",
      +  "created_at"=>"2018-03-07T14:22:36.000+01:00",
```

So close ...

The solution is quite simple but took me few minutes in order to find it.
It's all about using [the `iso8601` method](https://apidock.com/ruby/DateTime/iso8601) in your Rspec files and you can pass the length of fractional seconds in parameter.