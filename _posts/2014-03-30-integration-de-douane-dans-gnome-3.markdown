---
layout: post
title: Intégration de Douane dans GNOME 3
date: '2014-03-30 14:55:25'
tags:
- douane
---

Je suis un fan de GNOME, surtout depuis la version 3. C'est donc l’environnement que j'utilise sur mon PC a la maison.

Aujourd'hui j'ai eut l'idée d'intégrer Douane dans GNOME 3 en utilisant le service de notifications (que je trouve vraiment génial!).

Si vous ne connaissez pas trop GNOME 3 et son service de notifications, je vous invite à regarder cette vidéo: http://www.youtube.com/watch?v=lepXx1kDelo (En anglais, mais c'est pas important).

## Le concept

Voici mon concept: Disons que vous venez tout juste d'installer Spotify, et de le lancer.
La première chose que vous aurez à faire, c'est vous authentifier.
Spotify va donc tenter de se connecter aux serveurs pour effectuer cette connexion.

Douane ne connait pas encore Spotify et donc va le bloquer, puis vous afficher ceci en bas de l'écran:

![douane_GNOME_3_mockup1](/content/images/2017/08/douane_GNOME_3_mockup1.png)

En passant votre souris sur la notification, vous verrez ceci:

![douane_GNOME_3_mockup2](/content/images/2017/08/douane_GNOME_3_mockup2.png)

Vous pourrez donc accepter ou refuser que Spotify accède à internet sans quitter votre application.

Maintenant si vous avez raté la notification, en laissant votre souris sur le bas de l'écran pendant quelques seconds, la zone de notifications apparait et vous verrez ceci:

![douane_GNOME_3_mockup3](/content/images/2017/08/douane_GNOME_3_mockup3.png)

Vous pourrez donc encore accepter ou refuser une activité réseau tres simplement :)


Si vous désirez donner votre avis, veuillez vous rendre sur ce ticket Github: https://github.com/Douane/Douane/issues/26