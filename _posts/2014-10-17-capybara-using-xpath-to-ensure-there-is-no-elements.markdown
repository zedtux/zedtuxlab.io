---
layout: post
title: Capybara using Xpath to ensure there is no elements
date: '2014-10-17 09:42:43'
tags:
- xpath
- capybara
---

Let's say you are in the case where you'd like to ensure an element has no child within your test running with Capybara.

You can perform this easily thanks to the `have_xpath` methods options:

	expect(page).to have_xpath('//div[@id="list"]/a', count: 0)

This will raise an exception in the case the `div` with the id **list** has some `a` elements.