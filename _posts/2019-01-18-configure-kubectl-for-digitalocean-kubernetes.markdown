---
layout: post
title: Configure kubectl for DigitalOcean Kubernetes
date: '2019-01-18 09:36:07'
tags:
- docker
- kubernetes
---

[DigitalOcean](https://www.digitalocean.com) host provider released [DigitalOcean Kubernetes](https://www.digitalocean.com/products/kubernetes/) which allows you to have a full k8s ([Kubernetes](https://kubernetes.io)) cluster in minutes with very few clicks.

The cool stuff here is that you'll gonna pay only the worker nodes, not the masters.
This means you can have a runnung cluster for $10/month with the smallest droplet they provide.

## kubectl

`kubectl` is the command line tool to manage and use your k8s cluster.

You can install it on macOS using [Homebrew](https://brew.sh) :

```
$ brew install kubernetes-cli
```

Or by installing the [Docker Desktop app](https://www.docker.com/products/docker-desktop).

## DO K8s with kubectl

`kubectl` command line tool uses a config file in order to access the Kubernetes cluster.
This config file could be easily downloaded from the DigitalOcean UI.

### Fetch the kubectl config file

Connect to the DO website and from the Kubernetes menu download the configuration file :

![digitalocean-kubernetes-download-config](/content/images/2019/01/digitalocean-kubernetes-download-config.png)

### Install the config file

`kubectl` is reading the config from the `~/.kube/config` file so just do the following :

```
$ mkdir ~/.kube
$ mv ~/Downloads/<cluster name>-kubeconfig.yaml ~/.kube/config
```

### Check all is working fine

Now you want to check that all works fine so you try to get [pods](https://kubernetes.io/docs/concepts/workloads/pods/pod/) from your cluster :

```
$ kubectl get pods --all-namespaces=true
NAMESPACE             NAME                                    READY   STATUS      RESTARTS   AGE
kube-system           coredns-7b5bcb98f8-fg4pq                1/1     Running     0          14d
kube-system           coredns-7b5bcb98f8-g8lp5                1/1     Running     0          14d
kube-system           csi-do-node-5p55z                       2/2     Running     0          14d
kube-system           csi-do-node-64c2z                       2/2     Running     0          14d
kube-system           csi-do-node-tb5fj                       2/2     Running     0          14d
kube-system           kube-proxy-serene-wiles-83l7            1/1     Running     1          14d
kube-system           kube-proxy-serene-wiles-83lc            1/1     Running     1          14d
kube-system           kube-proxy-serene-wiles-83lm            1/1     Running     1          14d
kube-system           kubernetes-dashboard-57df4db6b-8n97n    1/1     Running     0          14d
kube-system           metrics-server-fc6d4999b-mtjjd          1/1     Running     0          8d
```

## Next

Now you need to go through [the Kubernetes documentation](https://kubernetes.io/docs/home/?path=users&persona=app-developer&level=foundational) in order to understand all [the concepts](https://kubernetes.io/docs/concepts/) and how to deploy and manage apps in Kubernetes.

For people, like me, wanting to preserve their eyes while reading the documentation, I did [a dark theme for the Kubernetes webiste](https://userstyles.org/styles/167671/kubernetes-io-dark-theme) 😇.