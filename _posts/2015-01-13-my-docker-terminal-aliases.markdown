---
layout: post
title: My Docker terminal aliases
date: '2015-01-13 17:36:47'
tags:
- docker
- fig
---

In the case you developer using [Docker](http://docker.com), you have to adapt your environment in order to ease your life, like you did before.

For example, I had created a function `sc` (for script console) in order to start the Rails console no matter the version of Rails. Since I'm working with Docker, I have updated this function in order to use Docker when it is possible (with [Fig](http://fig.sh)).

I'm gonna to share with you my most used aliases :)

*As I'm developing mostly Rails applications, my aliases are for Rails .*

## All my aliases start with 'f'

You will see that all my aliases start with a `f` because I'm using Fig.

Fig is a very nice piece of software which allows you to orchestrate your Docker images. It's very useful when you're starting with Docker and you're no yet confortable with all the Docker commands.

Fig work with a YAML file where you define which images you need (Database, interpretor, and so on) and also how to build your image.

Here, as an example, the `fig.yml` file from my [Brewformulas.org](http://brewformulas.org) project:

<script src="https://gist.github.com/zedtux/e266cb9d38129af6fa09.js"></script>

You can find the file in my [Github repository](https://github.com/zedtux/brewformulas.org).

When you're done, you can then play with `fig up` to start up all the images, `fig ps` to see the running images and their statuses and more importantly (regarding this article) the `fig run` command which allows you to run a command within a docker image instance.

## Fig run

Whenever you want to execute a command in your **web** docker image instance (**web** referes to the `web` key from the YAML file), you can use `fig run web` and then your command.

However there is an output issue with Fig which truncate the output very often but I have some tricks for you :)
I have already opened the Github issue [docker/fig#458](https://github.com/docker/fig/issues/458).

## List of aliases


### frake

As you could image this alias will run `fig run web rake`. It's the most useful alias since you have to use [rake](http://docs.seattlerb.org/rake/) very often.

Run the migration scripts

    $ frake db:migrate

Rollback the database for the test environment

    $ frake db:rollback RAILS_ENV=test

Run the tests

    $ frake

Generate the Rails routes. Here is where the Fig output issue is the most disturbing. To solve the issue I'm using `less` so that moving up and down flush the output and solve the issue

    $ frake routes | less

### fbundle

The next alias which I use the most. It runs in the background `fig run web bundle`.

Install new gems

    $ fbundle package --all

Update the gems

    $ fbundle update

**Note:** Don't forget to re-build your fig image after having used those aliases with `fig build`.

### fbe

This one is running in the background `fib run web bundle exec` and is useful when you want to run tests.

Execute a cucumber feature file

    $ fbe cucumber features/signup.feature

Execute all the RSpec specs

    $ fbe rspec

### frails

In my case I use this one only for one usecase:

    $ frails g migration AddNameColumToPostsTable

You could use it to fire the Rails console

    $ frails c

### fcop

In the case you're, like me, using Rubocop, this alias will be useful. It runs in the background `fig run web bundle exec rubocop -R`

    $ fcop

## Where to add all those aliases?

To keep it simple I have created a `~/.fig_aliases` file in my home folder and added `source ~/.fig_aliases` in my `~/.zshrc` file (can be done also within the `~/.bashrc` for bash users).

## Conclusion

This really helps and can be improved a lot ! In the case you have other or better aliases, feel free to contact me on my Twitter account [@zedtux](https://twitter.com/zedtux)