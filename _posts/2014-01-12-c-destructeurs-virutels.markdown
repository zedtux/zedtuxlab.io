---
layout: post
title: 'C++: Destructeurs virutels'
date: '2014-01-12 17:21:51'
tags:
- c-2
---

Petit billet rapide à propos des destructeurs virtuels de classes.

## Qu’est ce qu’un destructeur virtuel de classe ?

C’est un simple destructeur de classe déclaré avec le mot clé virtual devant.

Comme ceci:

<script src="https://gist.github.com/zedtux/8387642.js"></script>

## Quand utiliser un destructeur virtuel ?

Les destructeurs virtuels seront indispensable lorsque vous ferez de l’héritage, mais ils seront à éviter à tous prix sans héritage !

### Avec héritage

Si vous déclarez une classe B qui sera hérité par A, il vous faudra placer le mot clé `virtual` afin que lorsque la classe A soit détruite, la classe B le soit de même.

Si vous ne le faites pas, vous risquez des fuites de mémoire. Ce qui n’est pas terrible :-p

### Sans héritage

Si vous placer virtual devant tout vos destructeurs sans faire d’héritage, l’impacte ne sera pas énorme, mais vous aller alourdir vos classes à la compilation … pour rien !

En effet, le compilateur ajoute des informations de compilation à votre classe à cause de virtual, qui ne servent à rien sans héritage.
Conclusion:

Détruisions **OUI**, mais détruisons intelligemment ! ;-) 