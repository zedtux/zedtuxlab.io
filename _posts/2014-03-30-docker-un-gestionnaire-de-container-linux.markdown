---
layout: post
title: Docker, un gestionnaire de containers Linux
date: '2014-03-30 09:11:40'
---

![homepage_docker_logo](/content/images/2017/08/homepage_docker_logo.png)

J'avais découvert [Docker](https://www.docker.io/) il y a quelques mois mais je ne l'avais pas étudié en détail.

Pour le boulot j'envisage de l'utiliser afin d'isoler l'environnement de développement pour les gens qui viennent développer.

## A quoi sert Docker ?

Docker est donc un gestionnaire, très léger et rapide, de containers Linux, c'est à dire qu'il vous permet de créer une image contenant tout ce dont vous avez besoin pour faire une chose bien précise, puis de placer cette image sur un serveur afin de la réutiliser sur n'importe quelle machine dés l’or que cette dernière peut faire tourner Docker.

Dans mon cas, je veux créer un environnement isolé avec toutes les dépendances nécessaires (base de données, bibliothèques, et autres) afin que lorsqu'un nouveau développeur arrive, il n'es qu'a installer Docker, puis charger l'image de l'entreprise, et commencer a coder.
Aussi cette image pourrait être utilisé pour faire tourner les tests. J'ai trouvé [baleen](https://github.com/kimh/baleen) qui a l'air de pouvoir le faire.

Tout l'intérêt de Docker est que lorsque vous lancez une image, tout démarre immédiatement est consomme très peu de ressources.

Un autre avantage de Docker est que si vous utilisez différents système d'exploitations, Docker fournira un environnement identique.

## Lorsque l'on utilise pas Linux ?

Au travail nous utilisons des Macs puisque nous développons en Ruby (meilleur plateforme pour ce langage selon moi).

Lorsque vous utilisez autre chose que Linux, il faudra donc utiliser [VirtualBox](https://www.virtualbox.org/) afin de monter une image d'une distribution Linux.
Pour OSX, par le passé, il fallait utiliser Vagrant, mais depuis peu il existe [boot2docker](https://github.com/boot2docker/boot2docker) comme [décrit dans la documentation de Docker](http://docs.docker.io/en/latest/installation/mac/). Cet outil va monter une image Linux très minimal afin que Docker puisse fonctionner.

Dans ce cas vous "perdez" un peu de l’intérêt de Docker car vous avez votre OS et un Linux en parallèle.
Cela dit, avec boot2docker vous démarrez une fois l'image, et puis vous irez aussi vite que sur une machine Linux.