---
layout: post
title: Nexuiz devient proprio, et sont fork est Xonotic
date: '2014-01-12 19:32:48'
tags:
- linux
- game
---

![nexuiz2](/content/images/2017/08/nexuiz2.png)

J’ai bien été peiné de voire au début l’achat de cet excellent jeu de shoot qui fonctionne sous Windows, Mac et Linux !

Mais bien heureusement, un fork libre de Nexuiz a vue le jours tout de suite derrière.

Le site du fork est http://www.xonotic.org/.

Source: http://www.cyrille-borne.com/index.php?post/2010/03/23/Le-vendu-du-jour-:-Nexuiz-quitte-le-monde-libre