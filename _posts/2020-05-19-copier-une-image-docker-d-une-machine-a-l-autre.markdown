---
layout: post
title: Copier une image Docker d'une machine à l'autre
date: '2020-05-19 10:14:47'
tags:
- docker
---

Ce matin, petite suprise, [quay.io](https://quay.io/) est tombé! 😱️

J'ai découvert cette information par la force des choses puisque je n'arrivais pas à installer le driver CNI [Flannel](https://github.com/coreos/flannel) de CoreOS avec ces erreurs:

```
Flannel container status changed to Init:0/1.
Flannel container status changed to Init:ErrImagePull.
Flannel container status changed to Init:ImagePullBackOff.
Flannel container status changed to Init:ErrImagePull.
Flannel container status changed to Init:ImagePullBackOff.
Flannel container status changed to Init:ErrImagePull.
Flannel container status changed to Init:ImagePullBackOff.
Flannel container status changed to Init:ErrImagePull.
```

Bref ... comment faire?

J'ai tout d'abord cherché un registre mirroir de cette image ... mais je n'ai rien trouvé qui semblait fiable.

Mais j'ai des serveurs où l'image est présente ... donc la question est la suivante:

## Comment transférer une image d'une machine à l'autre?

Et la réponse a été donnée par **kolypto** dans [cette réponse](https://stackoverflow.com/a/26226261/1996540):

```
docker save <image> | bzip2 | pv | \
     ssh user@host 'bunzip2 | docker load'
```

_Attention: Le paquet `pv` doit être installé._

Voici donc la procédure:

1. Sur le serveur d'où l'image sera copié, créez une clé SSH avec `ssh-keygen`
2. Importez la clé publique sur le serveur où l'image sera copiée
3. Repérez le nom de l'image Docker à copier:
```
# docker images | grep flannel | awk '{print $1":"$2}'
quay.io/coreos/flannel:v0.10.0-amd64
```
4. Copiez l'image:
```
# docker save quay.io/coreos/flannel:v0.10.0-amd64 | bzip2 | pv | \
    ssh root@<serveur de destination> 'bunzip2 | docker load'
11.5MiB 0:00:09 [1.19MiB/s] [                <=>                     ]
Loaded image: quay.io/coreos/flannel:v0.10.0-amd64
```

Voilà, l'image apparaît bien sur le serveur cible, et mon noeud master passe en **Ready** 🎉️
