---
layout: post
title: ActiveRecord Migration UI gem
date: '2019-06-20 15:30:20'
tags:
- ruby
- rails
---

Something that is painful to me with Rails is when doing a `git pull` on a branch with new migration scripts, and getting this error page:

![Default Ruby On Rails pending migration error page](https://gitlab.com/pharmony/active_record_migration_ui/raw/master/screenshots/rails-default-pending-migration-page.png)

This is nice, you know there are migration scripts to be executed, you even have the command to be executed in order to _solve_ the issue ... but that's all.

You have to open your terminal, go to the project's folder, run that command (or adapt it to Docker if you're using Docker), it will load the environment (so taking few seconds) then, finaly, run the migration scripts.

## Enter the ActiveRecord Migration UI gem

At [Pharmony](http://pharmony.lu/) we took the time to fix it so that now, all of that is now from the past! Look at that:

![ActiveRecord Migration UI gem](https://gitlab.com/pharmony/active_record_migration_ui/raw/master/screenshots/ar-migration-ui-page.png)

You just get a nice page, with the list of pending migration scripts, and a simple "Migrate now!" button.

As the environment is already loaded, as you're in a Rails app, you don't _waste_ time and just immediately migrate your database! 👌

### Installation

You just have to add the gem to your `Gemfile` from your Rails app, run the `bundle` command and restart your server.

Next time you got pending migration scripts, this page will show up and you'll know what you have to do! 😎

### One more thing

This gem will be enriched with many more features but you already have a ~~buggy~~ log view after the scripts ran:

![ActiveRecord Migration UI gem - migration logs](https://gitlab.com/pharmony/active_record_migration_ui/raw/master/screenshots/ar-migration-ui-logs.png)

Clicking the migrated script shows the logs of that migration.

## Where is the source code?

The gem is open source, under MIT licence, hosted on our Gitlab group: https://gitlab.com/pharmony/active_record_migration_ui.

Please feel free to contribute and report issues!

Help us to make it visible to the world by upvoting on [ProductHunt.com](http://producthunt.com):
<iframe style="border: none;" src="https://cards.producthunt.com/cards/posts/159086?v=1" width="500" height="405" frameborder="0" scrolling="no" allowfullscreen></iframe>