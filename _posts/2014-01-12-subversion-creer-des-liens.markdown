---
layout: post
title: 'Subversion: Créer des liens !'
date: '2014-01-12 16:55:12'
tags:
- subversion
- vcs
---

![GNOME CCMime](/content/images/2017/08/gnome_ccmime.png)

## Gné ??

L’idée, ici, est de créer un lien d’un dossier du repository, ou d’un autre repository au dossier courant.

Par exemple, j’ai un dossier `utils/` qui contient divers outils réutilisable n’importe où (ranger dans un dossier trunk bien entendu ;) ), que je veux lier à mon `projetA/`.

Donc, je veux que lorsque je face un __checkout__ de /projetA/ que le dossier /utils/ soit aussi pris et donc je veux en locale /projetA/utils/.

L’avantage, c’est que, si je modifie mon fichier `/utils/logging.h`, la modification sera automatiquement répandue partout !

## Subversion Propset

Pour créer un tel lien, il vous faut d’abord comprendre comment ca fonctionne ! :)

Tout ce fait avec la commande `svn propset`.

Cette commande va écrire dans les dossiers, des svn properties.

Par exemple, un `merge` ajoutera un `svn:mergeinfo` dans le dossier où à été effectué le merge.

Pour créer notre lien, ce qui va nous intéresser, c’est svn:externals.

`Attention:` Dans les version __inférieur__ à Subversion 1.6, vous ne pourrez lier qu’un dossier. Ce n’est qu’a partir de la version 1.6 que vous pouvez lier un fichier.

Créer un lien

Aller, c’est partis !

Reprenons notre exemple.

Je veux donc que, le contenu du dossier `/utils/trunk/` soit lié dans un dossier `/projetA/trunk/utils/`.

Bon, déjà, il faut faire un checkout du projet !

    svn co http://svn.mydomain.org/projetA/ projetA/
    cd projetA/

Maintenant, nous sommes dans le dossier projetA/ qui contient branches/, tags/ et trunk/.

Nous, on va ajouter une svn propertie au dossier trunk/.

    svn propset svn:externals “utils http://svn.mydomain.org/utils/trunk/” ./trunk/

Et voila !

Un `svn update` va créer le dossier utils/ avec le contenu stocké dans http://svn.mydomain.org/utils/trunk/ !

Si tout ca est bon, il ne reste plus qu’a commiter ! ;) 