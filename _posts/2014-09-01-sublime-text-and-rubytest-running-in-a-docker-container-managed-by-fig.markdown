---
layout: post
title: Sublime Text and RubyTest running in a Docker container managed by Fig
date: '2014-09-01 20:51:20'
tags:
- docker
---

### Presentation

[Docker](http://docker.com) is a well known application which ease the creation of Linux containers.

[Fig](http://www.fig.sh) is a docker orchestration tool which helps you (a lot) in the management of containers.

### Use case

Now let's say you're using [Sublime Text](http://www.sublimetext.com) as IDE and you're writing tests using the [RubyTest Sublime Text plugin](https://github.com/maltize/sublime-text-2-ruby-tests) (so for Ruby but should work the same for other languages) and you would like to use Docker in order to containerise your application.

To do so you've updated the RubyTest configuration in order to use `fig run web ...` .

### The issue

You will have the issue that Sublime Text is not able to show you the output from the container accessed using Fig, in the console window, as Fig is streaming the output in a way that Sublime Text can't handle.

### A solution

I have written this small Ruby script which will catch the stream from Fig and print it to the stdout in a way that makes Sublime Text to work as expected.

1. Create a file named `fig-to-sublime` in a folder included in your `$PATH` and copy/past the following:

	<script src="https://gist.github.com/zedtux/0441eba19da0bd14db92.js"></script>

2. Make it executable: `chmod +x /path/to/fig-to-sublime`
3. Update the `RubyTest.sublime-settings` file as the following:
	<script src="https://gist.github.com/zedtux/30fcf300fd1db9bf8c5b.js"></script>
4. Run your tests :)