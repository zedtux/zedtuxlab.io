---
layout: post
title: 'Douane: Current status of my Interactive Firewall for Linux'
date: '2014-01-13 12:45:24'
tags:
- my-developments
- douane
---

Like I said in [my previous article](/douane-status-and-roadmap/) that I’m back on the development of my Linux Interactive Firewall project, and I did a lot of progresses.

Let me share this with you :-)

## Current state

My stable **Linux Kernel Module** (LKM), written in C, is now passing network activities to the working **daemon** (named _douaned_ and written in C++) who has a [D-Bus](http://www.freedesktop.org/wiki/Software/dbus) server name `org.zedroot.Douane` publishing signals `NewIncomingActivity` and `NewOutgoingActivity` where I can connect using a Python project to receive all network activities.

## Something to show us?

Still not as it is only terminal outputs, but the next big step is the GUI!
That means my next article should have first screenshots!


Also I would like to announce that I already registered Douane into the [Ubuntu Software Center](http://http//www.ubuntu.com/ubuntu/features/find-more-apps) as a non-free application of a price of **$7**.

As soon as the first release will be done, I will submit my application to the review process.