---
layout: post
title: Corebird is a Twitter client for GNOME
date: '2015-04-19 14:58:24'
tags:
- debian
---

![corebird-2](/content/images/2017/08/corebird-2.png)

Corebird is an excellent Twitter client which has a lot of nice features.

Here is how to compile it on [Debian](https://www.debian.org/) (Jessie):

Version Francaise: http://blog.zedroot.org/corebird-un-client-twitter-pour-gnome/

### Clone the sources

First of all clone [the Git repository](https://github.com/baedert/corebird):

    $ git clone https://github.com/baedert/corebird.git
    $ cd corebird/

### Select the version

Corebird has different available versions that you can find in [the Github repository Releases page](https://github.com/baedert/corebird/releases).

Here I'm going to compile the version 1.0:

    $ git checkout 1.0

### Install the dependencies

In order to compile Corebird you will need some dependencies like the following:

    $ sudo apt-get install autoconf intltool libtool libsqlite3-dev librest-dev libgee-0.8-dev libjson-glib-dev libgstreamer-plugins-base1.0-dev valac libgtk-3-dev


### Compilation

Now let's start the compilation:

    $ ./autogen.sh --prefix=/usr
    $ make
    $ sudo make install

### Start Corebird

Now you can find a menu entry "corebird" or your can start it from a console:

    $ corebird

And you will have the following:

![Screenshot-from-2015-04-19-16-58-31-1](/content/images/2017/08/Screenshot-from-2015-04-19-16-58-31-1.png)