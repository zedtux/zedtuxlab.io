---
layout: post
title: 'Refactorisation: Renommer sans stress'
date: '2016-10-08 12:02:19'
tags:
- rails
- refactoring
---

Renommer une association, un attribute ou encore une méthode mène le développeur à devoir mettre à jour tout le code, mais cette tâche est difficile et dangereuse en cas d'oublis (erreurs 500).

Je travail actuellement sur la migration et le découpage d'une application monolithe, et la première étape consiste à extraire une partie du code commune à toute l'application.
Cette partie contient des modèles avec associations renommé comme ceci:

<script src="https://gist.github.com/zedtux/0059cbd26810fc594fea59c586018118.js"></script>

(Les modèles sont préfixés avec `Right` mais les associations sont renommées pour raccourcir leurs noms).

Cette technique est néfaste pour l'application lorsqu'elle grandie et se complexifie, car elle mène à ce genre de résultat:

Le modèle `RightLevel` est comme ceci:

<script src="https://gist.github.com/zedtux/3a4654ac9f07b85fd6ed41f7f3f1e751.js"></script>

Et maintenant à l'utilisation nous avons ceci:

<script src="https://gist.github.com/zedtux/cac726767c1ceb5b0304beae5e7a1ea0.js"></script>

Nous nous retrouvons avec `level.level` qui n'a aucun sens.

## Renommer sans stress

Pour corriger ce problème, j'ai renommé toutes les associations similaires à ce cas, mais il faut maintenant adapter le code pour éviter les erreurs.

Une technique simple et efficace, utilisé d'ailleurs par les développeurs de Rails, est d'utiliser les `DEPRECATION WARNINGS`.

Cela consiste a avoir 2 méthodes, l'ancienne déprécié et la nouvelle à utiliser à la place, et lorsque l'ancienne est appelé, un message est envoyé par `warn` puis d'appeler la nouvelle méthode.

Vous pouvez utiliser [la méthode de Rails](http://apidock.com/rails/Module/deprecate) disponible dans le framework, mais cette dernière indique que la méthode sera retiré du framework Rails, or ici il s'agit d'une méthode/association de notre application.

C'est pourquoi j'ai implémentés ma propre méthode donnant un résultat très proche de celui de Rails:

<script src="https://gist.github.com/zedtux/f7177c8d49179d297f7dabf8049634a6.js"></script>

Maintenant la méthode `deprecate` est disponible dans tous vos modèles et je l'ai donc ajouté comme ceci dans mes modèles:

<script src="https://gist.github.com/zedtux/5ffe0039be922188e911e27f5af071a9.js"></script>

Maintenant les deux noms sont disponibles `:levels` et `:right_levels` mais avec un message `DEPRECATION WARNING` si `:levels` est utilisé.

## Conclusion

Nous oublions parfois les méthodes les plus simples qui permettent d'améliorer le code sans impacter le coût pour l'entreprise.

Grâce à ces messages, nous allons pouvoir renommer au fur et à mesure, avec les gens de mon équipe, les différents emplacements où l'ancien nom est utilisé.