---
layout: post
title: 'Bash : Voir le top ten des commandes lancées'
date: '2014-01-12 20:19:31'
tags:
- linux
- command-line
---

Voici une commande qui affiche dans l’ordre décroissant les commandes les plus utilisées dans tout votre historique :

	history | awk ‘{print $2}’ | awk ‘BEGIN {FS=”|”}{print $1}’ | sort | uniq -c | sort -n | tail | sort -nr
    
Sur la machine du boulot :

    34 nano
    28 reload
    21 rake
    20 eprofile
    17 cucumber
    17 cd
    12 rgrep
    11 ls
    10 sudo

Et chez moi :

    297 sudo
    247 git
    217 bzr
    210 cd
    171 svn
    157 ls
    145 make
    99 quiestla/face.py
    70 ./tuxtremsplit.py