---
layout: post
title: Apple Numbers vs Microsoft Excel
date: '2014-10-25 18:03:04'
---

<blockquote class="twitter-tweet" lang="en"><p>I can&#39;t believe how easy is <a href="https://twitter.com/hashtag/Apple?src=hash">#Apple</a> <a href="https://twitter.com/hashtag/Numbers?src=hash">#Numbers</a> compare to <a href="https://twitter.com/hashtag/Microsoft?src=hash">#Microsoft</a> <a href="https://twitter.com/hashtag/Excel?src=hash">#Excel</a> ! Microsoft users are sadomasochist in my eyes. Stop complex stuffs</p>&mdash; ZedTuX (@zedtux) <a href="https://twitter.com/zedtux/status/526069619464085505">October 25, 2014</a></blockquote> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

I have given a try to [Apple Numbers](http://www.apple.com/mac/numbers/) application in order to build a spreadsheet and I have to say it's really nice but more importantly it's simple !

Someone did a video about Numbers. Just watch it and see the difference (starting at 12 minutes and 47 secondes):

<iframe width="560" height="315" src="//www.youtube.com/embed/N1F1WgAbV5o" frameborder="0" allowfullscreen></iframe>