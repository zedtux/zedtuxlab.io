---
layout: post
title: 'vrms: virtual Richard M. Stallman: 0.6%'
date: '2014-01-12 18:04:15'
tags:
- linux
- command-line
---

Comme c’est en ce moment la mode, voici les résultat chez moi ! :)

    $ vrms
    Non-free packages installed on zUbuntu

    linux-generic Complete Generic Linux kernel
    linux-restricted-modules- Non-free Linux 2.6.28 modules helper script
    linux-restricted-modules- Restricted Linux modules for generic kernels
    nvidia-180-kernel-source NVIDIA binary kernel module source
    nvidia-180-libvdpau Video Decode and Presentation API for Unix
    nvidia-180-modaliases Modaliases for the NVIDIA binary X.Org driver
    nvidia-glx-180 NVIDIA binary Xorg driver
    sun-java6-bin Sun Java(TM) Runtime Environment (JRE) 6 (architecture
    sun-java6-jre Sun Java(TM) Runtime Environment (JRE) 6 (architecture
    unrar Unarchiver for .rar files (non-free version)
    Reason: Modifications problematic
    xmind XMind – Brainstorming and Mind Mapping

    Non-free packages with status other than installed on zUbuntu

    mythtv-backend ( dei) A personal video recorder application (server)
    mythtv-database ( dei) A personal video recorder application (databas

    Contrib packages installed on zUbuntu

    gnormalize Audio file converter, normalizer and CD ripping utilit
    nvidia-settings Tool of configuring the NVIDIA graphics driver

    13 non-free packages, 0.6% of 2252 installed packages.
    2 contrib packages, 0.1% of 2252 installed packages.