---
layout: post
title: 'Douane: First screenshot of the firewall configurator'
date: '2014-01-13 12:49:03'
tags:
- my-developments
- douane
---

A new article following all previous one to show you the very first screen shot!

[I’m also speaking about it](https://coderwall.com/p/rgtyag) at [coderwall.com](https://coderwall.com/).

## Current status

I have created yesterday [a twitter account](https://twitter.com/douaneapp) for my application in order to inform all the world about my application and its updates. (Don’t forget to follow it!!)

Then I wrote a [Twitter](https://twitter.com/) interface class to show the 5 latest tweets from that account (you will see it from the screen shot).

The only remain thing to finish the Twitter part is to implement the “since” whom is “5m” for all tweet currently…


The next step is to implement the switch to enable / disable the Firewall.

## Something to show us?

Finally YES!

I’m proud to show you the very first screen shot of **DOUANE configurator**:

![Douane - firewall configurator screenshot for linux](/content/images/2017/08/douane_alpha_1_general_tab.png)

