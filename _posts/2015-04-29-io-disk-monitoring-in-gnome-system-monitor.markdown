---
layout: post
title: IO disk monitoring in GNOME system-monitor
date: '2015-04-29 21:12:37'
tags:
- gnome
---

This post is a note to find easily the place where the system-monitor project ticketing tool is and more especially the ticket about IO monitoring.

Ticketing system: https://bugzilla.gnome.org/page.cgi?id=browse.html&product=system-monitor
IO ticket: https://bugzilla.gnome.org/show_bug.cgi?id=499725