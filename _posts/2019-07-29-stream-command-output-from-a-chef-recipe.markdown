---
layout: post
title: Stream command output from a Chef recipe
date: '2019-07-29 13:37:02'
tags:
- chef
---

In this article I will show you how to see, in real-time, a command output from a Chef recipe.

## Executing a command, the chef way

As a command example, as I'm working with it right now, let's take the Kubernetes installation tool: [kubeadm](https://kubernetes.io/docs/reference/setup-tools/kubeadm/kubeadm/).

This app tells you what he is doing in its output.

I first did this:

<script src="https://gist.github.com/zedtux/681d2958f5bee3de6e7f691cd2786e81.js"></script>

While this runs the command successfully, all the command's output is invisible:

```
* execute[kubeadm init] action run
           - execute     kubeadm init     --token=vfopfv.fgum14jkv3ldssu8     --pod-network-cidr=10.244.0.0/16     --service-cidr=10.96.0.0/12     --service-dns-domain=cluster.local     --apiserver-advertise-address=172.28.128.200

```

## Executing a command, the Ruby way

Let's now make this real-time :

<script src="https://gist.github.com/zedtux/7030eda4a27ee0d2e02f0f054de8683c.js"></script>

Now you get the command output in real time:

```
         * ruby_block[kubeadm init] action run[init] Using Kubernetes version: v1.15.1
       [preflight] Running pre-flight checks
       [preflight] Pulling images required for setting up a Kubernetes cluster
       [preflight] This might take a minute or two, depending on the speed of your internet connection
       ...
```

🎉