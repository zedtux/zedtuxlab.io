---
layout: post
title: 'Douane: Late in the development'
date: '2014-01-13 12:54:13'
tags:
- my-developments
- douane
---

In a [previous article](/douane-release-date-announcement-and-status/) I announced a release date that I’m not able to reach as I had a lot to do in February and is the same for this month.

I hope to finish it soon as it doesn’t remain so many things to do:

 - Detect when application is killed/stopped in order to clean some information from the LKM
 - Finish the GUI (show the rules)
 - Change the start/stop button in order to ask root password to start/kill it
 - Implement the license mechanism
