---
layout: post
title: Boot-Repair est génial !
date: '2014-01-12 20:31:50'
tags:
- linux
---

Si vous rencontrez des difficultés à démarrer votre GNU/Linux, je vous conseil vivement [Boot-Repair](http://sourceforge.net/p/boot-repair/home/Home/) !

 

Prenez votre CD d’Ubuntu (par exemple), puis installez Boot-Repair (suivre ce lien pour Ubuntu: https://help.ubuntu.com/community/Boot-Repair) puis lancer Boot-Repair.

![Screenshot_Boot_Repair](/content/images/2017/08/Screenshot_Boot_Repair.png)

Cliquez sur le premier bouton et laissez faire !

Et tout fonctionnera !