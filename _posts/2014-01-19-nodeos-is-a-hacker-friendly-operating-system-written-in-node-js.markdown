---
layout: post
title: NodeOS is a hacker-friendly operating system written in Node.js
date: '2014-01-19 09:02:23'
---

[NodeOS](http://node-os.com/) is a brand new operating system with the following bug lines:

 - **Linux Kernel** NodeOS is a full OS built on top of the Linux kernel
 - **NodeJS Runtime** Node is the primary runtime — no bash here
 - **NPM Packages** NodeOS uses NPM as its primary package manager
 - **Hosted on Github** Open and easy to contribute to — pull request friendly

I don't know for you ... but when I'm reading this I'm seeing something really interesting !

It's too earlier right now to say anything about it but I wanted to share this as it looks so cool.

You can follow their [Twitter account](https://twitter.com/TheNodeOS) and/or subscribe to their news letter on http://node-os.com/.