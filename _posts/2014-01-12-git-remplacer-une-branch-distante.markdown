---
layout: post
title: 'Git: Remplacer une branch distante'
date: '2014-01-12 20:42:22'
tags:
- vcs
- git
---

## Situation initiale

Imaginez que vous travaillez sur une branche **develop**, qui donc contient les toutes dernières modifications, et une autre branche **demo**.
Disons que cette dernière est en retard d’un an.

Si vous essayez de faire un bête merge
	
    git checkout demo
    git merge develop

Vous risquez d’entrer en guerre avec toute une série de conflits.

## Remplacer une branche

Pour éviter ces conflits, vous pouvez remplacer la branche **demo** avec **develop**, puisque cette branche **demo** ne vous sert pas pour développer, et donc, ne contient rien a garder.

Vous devez donc créer une nouvelle branche en local qui s’appelle **demo**

    git checkout develop
    git checkout -b demo

Puis effacer la branche distante
	
	git push -f origin :demo

Puis renvoyer la nouvelle branche
	
	git push -f origin demo

Et voilà.