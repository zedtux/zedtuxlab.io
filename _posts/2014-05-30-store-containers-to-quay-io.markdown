---
layout: post
title: Store containers to Quay.io
date: '2014-05-30 16:20:26'
tags:
- docker
---

If you've just created your first Docker image, maybe you're looking for a place where to store this container ? A repository ?

There are few providers which allow you to store your containers in public or private repositories like [Quay.io](https://quay.io/).

You can create as much as you want of public repositories but you'll have to pay the at least $12 per months in order to create private repositories as it is described on [their pricing page](https://quay.io/plans/).

Docker.io allows you to have public repositories for free and private repositories starting at $7 as described on [their pricing page](https://index.docker.io/plans/).

Quay.io is highlighting how secure they are. It looks like they investigated a lot more about the security of storing.

## brewformulas.org

In my example I've described in my previous article [Containerize with Docker](http://blog.zedroot.org/containerize-with-docker/), I have created a main image and 2 tagged images.

I'm going to explain you how to push them to Quay.io.

## Repository creation

First of all you have to create a repository.

After having created an account, click the **Create a new repository** button.

Then select the **Public** option from the **Repository Visibility** section.
Next, in the **Initialize repository** section select **(Empty repository)**.

I have tried to use the **Initialize from a Dockerfile**, where you just copy/past the content of a Dockerfile, but the server availability was not enough and I have waited several minutes and never had my file executed.
So I'm going to show you the home made way. :-)

## Preparing for Shipment

Before to push our image we have to:

 - Login from the terminal to Quay.io using the `sudo docker login quay.io` command
 - Tag our images for Quay.io

In order to create the tags we have to execute the following lines:

    sudo docker tag zedtux/brewformulas.org quay.io/zedtux/brewformulas.org
    sudo docker tag zedtux/brewformulas.org:web quay.io/zedtux/brewformulas.org:web
    sudo docker tag zedtux/brewformulas.org:worker quay.io/zedtux/brewformulas.org:worker

The first line create the tag for the main image and then we create a tag for the **web** and **worker** images.

Now you should have the following Docker images:

<script src="https://gist.github.com/zedtux/c72a43af8f91e4a8dacb.js"></script>


## Shipment to Quay.io

Now you're ready to push the images:

	sudo docker push quay.io/zedtux/brewformulas.org
	sudo docker push quay.io/zedtux/brewformulas.org:web
	sudo docker push quay.io/zedtux/brewformulas.org:worker

It could takes some times depending on the size of your images and your internet connection.

When the upload is finished, you can see the following on Quay.io:

![quay-io_zedtux](/content/images/2017/08/quay-io_zedtux.png)

You can now use this repository in order to deploy or share your images. :-)