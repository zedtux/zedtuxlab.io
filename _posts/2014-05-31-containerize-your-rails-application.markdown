---
layout: post
title: Containerize your Rails application
date: '2014-05-31 08:39:05'
tags:
- docker
---

## Containerize ?

The principle if to create Linux container wherein you will install your application and all its dependencies.

After having created this container, you will be able to run it anywhere.

Linux containers are awesome when they are built to be scalabe. Then you just have to `run` more instances of your container on more servers, or even better, in the cloud so that you don't have to do any maintenance.

## Rails

In this article I'm going to describe how to deploy a Rails application I have developed, but of course no matter your application in a Rails application or not, you can use the Linux containers for anything you'd like.

The point of this article is to show how to deal with mutliple dependences, like databases, and to scale everything together.

## Example application

Maybe you already know it. It's a small Rails 4 application I have developed not so long time ago which store a description of the forumlas that you can install with [Homebrew](http://brew.sh/) on Mac.
It allows you also to navigate through the dependences of the formulas and to see the conflicts.

The application is [brewformulas.org](http://www.brewformulas.org/) and [the source is available on Github.com](https://github.com/zedtux/brewformulas.org).

## Technology stack

Here is the list of technologies the application uses:

 - [PostgreSQL](http://www.postgresql.org/) which could be replaced by any other databases supported by Rails.
 - [Redis](http://redis.io/) used as a broker for sidekiq
 - [Sidekiq](http://sidekiq.org/) which is the background jobs library
 - [Rails](http://rubyonrails.org/) the web framework for Ruby
 - [Ruby](https://www.ruby-lang.org/en/) the programmation language

In addition all the containers will run [newrelic](http://newrelic.com/) so that we could monitor them.

## Infrastructure

brewformulas.org project is small enough to run on a single server, including all the dependences, but we will use a server per dependence in order to be able to scale anyone of them.

This means, we will use 5 servers:

 - a load balancer
 - a PostgreSQL server
 - a Redis server
 - a Sidekiq server
 - an application server

Of course, buying dedicated servers is not an option here. We will go for cloud infrastructure in order to pay only what we use, which will be small instances.

# Containerize

This is the main point of this article. Creating a Linux container which will be responsible for one peace of brewformulas.org.

This step will be achive thanks to [Docker](https://www.docker.io/).
This application provide you a command line tool which ease the creation of Linux containers.
You can alreay start with [the official Docker tutorial](https://www.docker.io/gettingstarted/).

[Read my article "Containerize with Docker".](http://blog.zedroot.org/containerize-with-docker/)

# Store containers

Now that you have the Docker images ready, why not uploading them to a repository so that you will be able to share your images or deployment them.

[Read my article "Store containers to Quay.io".](http://blog.zedroot.org/store-containers-to-quay-io/)

# Deploy the containers

Finally you should deploy your containers and see yourself how easy it is to scale your application.

Of course you could use the amazing Amazon AWS (which has recently implemented Docker in their infrastructure) but I'm going to present you another platform.

[Read my article "Tutuuuuum".](http://blog.zedroot.org/tutuuuuum/)

# Conclusion

In my eyes, Docker is the future of application deployment.
How easy it is, and the mutliple platform which start to offer us different services about Docker containers, make me thinking that we will read a lot more in the future about it.

As of today, Docker is already use as a replacement for the famous packages managers like **apt-get** or **yum** like is doing [CoreOs](https://coreos.com/).
But this is another story. :-)