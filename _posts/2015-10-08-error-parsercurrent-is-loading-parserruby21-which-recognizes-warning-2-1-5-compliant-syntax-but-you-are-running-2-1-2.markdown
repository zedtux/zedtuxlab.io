---
layout: post
title: 'Error: parser/current is loading parser/ruby21, which recognizes warning:
  2.1.5-compliant syntax, but you are running 2.1.2'
date: '2015-10-08 09:16:33'
tags:
- rails
---

Short article about the following warning:

> Error: warning: parser/current is loading parser/ruby21, which recognizes warning: 2.1.5-compliant syntax, but you are running 2.1.2

## The meaning

First thing, what does that mean ? The message isn't really clear, you have to read it 2-3 times before to understand it.

This message come from [the parser gem](https://rubygems.org/gems/parser) (a dependency of [the rubocop gem](https://rubygems.org/gems/rubocop) in my case).
This gem is strongly linked to the Ruby version you're using, and when this warning pops up, it means you've upgraded the version of the gem which is for a newer Ruby version than the one you're running currently.

## The fix

The ~~fix~~ workarounds are the following:

 - Upgrade your Ruby
 - Downgrade the gem version

### Upgrade your Ruby

This solution could take times in case the upgrade impacts a lot your application, and it needs to be tested carefully, but in the other hand it will encourage you to always keep an up-to-date Ruby version, which is a critical thing.

### Downgrade the gem version

Temporary solution – consider the other fix in some days – which consist in downgrading to the right version of the gem.

#### How to find the right gem version to downgrade to ?

You can do it easily by looking at the history of the `lib/parser/current.rb` file of the parser gem:

![Screen-Shot-2015-10-08-at-11-13-06](/content/images/2017/08/Screen-Shot-2015-10-08-at-11-13-06.png)

From here you can see from the commit which is interesting for you the version of the gem from the `lib/parser/version.rb` file.