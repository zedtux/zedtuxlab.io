---
layout: post
title: Docker could be a good reason to move to Linux
date: '2014-08-31 19:06:20'
tags:
- docker
---

[Docker](http://docker.com) is an amazing peace of software which is really popular, with a growing community.

Docker uses a feature from the Linux kernel in order to isolate a complete environment and this is an issue for Mac and Windows users as they have to use [VirtualBox](https://www.virtualbox.org) in order to have a Linux box running where Docker could work.

This virtualization layer is causing some problems which prevent some Docker features to work properly out-of-the-box (like for instance the issue with the volumes using [boot2docker](http://boot2docker.io)).

Python developers are usualy working on Linux. They're using Docker in the best environment, so no virtualisation, so disk access or network issues.
Now regarding Ruby developers, most of the time working on OSX, they have to deal with this extra layer... Why not then move to Linux ?

I am asking my self more and more this question... Maybe installing a dual boot with [Elementary OS](http://elementaryos.org) :)

Let's see what will happen.