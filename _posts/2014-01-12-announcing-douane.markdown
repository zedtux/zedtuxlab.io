---
layout: post
title: Announcing Douane
date: '2014-01-12 20:33:49'
tags:
- my-developments
- douane
---

# What’s that?

**Douane** is an **Interactive Firewall for Linux >= 2.6**. like [TuxGuardian](http://tuxguardian.sourceforge.net/).

It means that when an application try to open a new connection to a remote server, and is unknown by Douane, then a dialog box allowing you to decide if you accept it or not will popup, blocking the application until you answer it.

Basically it’s another interface to Linux Netfilter, but more friendly user.

# Current state

The project will be splited in some pieces, and the very first one is a [Linux Kernel module](http://en.wikipedia.org/wiki/Kernel_module) (written in [C](http://en.wikipedia.org/wiki/C_%28programming_language%29)), and a daemon (written in [C++](http://en.wikipedia.org/wiki/C%2B%2B)).
I’m currently working on the Kernel module communications with User namespace application on my [Ubuntu](http://www.ubuntu.com/) **Oneiric 11.10**, with a [VirtualBox](https://www.virtualbox.org/) instance hosting Ubuntu Precise 12.04 (Kernel version 3.2.0).

The first release will only show the network activities without blocking anythings (because the kernel need to know all rules in order to block or not requests, and that part will be done after).

# Something to show us?

No. At least some debugs messages from the kernel, but I don’t think that you want it ;) 