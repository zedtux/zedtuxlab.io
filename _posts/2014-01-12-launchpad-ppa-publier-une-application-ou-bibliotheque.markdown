---
layout: post
title: 'Launchpad PPA : Publier une application ou bibliothèque'
date: '2014-01-12 20:18:20'
tags:
- ppa
- howto
---

![deb](/content/images/2017/08/deb.png)

Finalement, j’ai enfin pris le taureau par les cornes et étudié comment créer un dépôt **PPA** que [Launchpad.net](https://launchpad.net/) (créé par [Canonical](http://www.canonical.com/), la société à l’origine d’[Ubuntu](http://ubuntu-fr.org/)) met à disposition de ses membres.

Ça va me permettre de distribuer mes applications telles que TuXtremSplit très simplement, grâce à l’utilisation d’[apt](http://fr.wikipedia.org/wiki/Advanced_Packaging_Tool), et de add-apt-repository !

# Point de départ

Disons que vous avez un compte Launchpad, activé, avec le [code de conduite](https://launchpad.net/codeofconduct)[en] signé, et [une clé GPG importée dans Launchpad](https://help.launchpad.net/YourAccount/CreatingAnSSHKeyPair) :)

Je prendrai mon programme TuXtremSplit pour l’exemple puisqu’il est divisé en 2 :

 - Une librairie contenant le "moteur"
 - Un client console pour utiliser cette bibliothèque

Donc il va falloir préparer la bibliothèque en premier, puis la console, sachant que cette console va donc dépendre de la bibliothèque.

## C’est parti !

Bon, pour commencer, il faut donc les sources !

Ensuite, il va falloir créer un dossier de travail pour créer vos paquets, afin de l’envoyer vers un PPA.

## Création du plan de travail, récupération des sources

Je vous propose donc de récupérer les sources dans un dossier `developpements/` et de créer un dossier `empaquetage/`

Ensuite, placez vous dans le dossier `developpements/` puis avec un **VCS** (Versioning Control System[[Wikipedia](https://secure.wikimedia.org/wikipedia/fr/wiki/Gestion_de_versions)]) tel que [subversion](http://fr.wikipedia.org/wiki/Subversion_%28logiciel%29) récupérez les sources :

    svn co http://svn.zedroot.org/TuXtremSplit::v2/trunk/ tuxtremsplit

Vous aurez un dossier `tuxtremsplit/` qui contient :

    console/
    library/
    modules/
    …

Puis des fichiers dont un `CMakeLists.txt` qui est mon fichier de configuration pour [CMake](http://www.cmake.org/).


## Préparation

Nous allons préparer la zone d’empaquetage.

Nous allons créer un dossier `tuxtremsplit/` dans le dossier `empaquetage/` puis nous allons, dans le dossier `tuxtremsplit/`, créer les dossiers `console/` et `library/` :

    mkdir -p empaquetage/tuxtremsplit/console/
    mkdir empaquetage/tuxtremsplit/library/

Donc nous allons séparer le code de la bibliothèque et celui de la console afin d’avoir deux paquets différents.

Maintenant il nous reste à placer le code dans chaque dossier, afin de pouvoir passer à la suite ! :)

Pour ce faire, vous n’avez qu’à copier `developpements/tuxtremsplit/library/` dans `empaquetage/tuxtremsplit/library/`

    cp -R developpements/tuxtremsplit/library/ empaquetage/tuxtremsplit/library/

Rassurez vous, ce n’est pas une erreur de ma part d’avoir copié le dossier entier, car nous allons le renommer !

En effet, l’outil que nous allons utiliser bientôt a besoin d’être lancé dans un dossier nommé comme le sera le paquet, c’est-à-dire avec le nom du programme, un trait d’union, puis la version:

    mv empaquetage/tuxtremsplit/library/library/ empaquetage/tuxtremsplit/library/libxtm-1.0.1.1

Donc libxtm pour la bibliothèque, puis 1.0.1 pour la version. Le dernier 1 de la fin représente la version du paquet. Vous aurez très certainement à recommencer vos envois… et vous allez comprend pourquoi à ce moment là ! :)

Faisons pareil pour la console :

    cp -R developpements/tuxtremsplit/console/ empaquetage/tuxtremsplit/console/
    mv empaquetage/tuxtremsplit/console/console/ empaquetage/tuxtremsplit/console/tuxtremsplit-2.0.1.1

Ici pareil, le dossier `console/` est renommé en tuxtremsplit pour le nom du programme, puis sa version 2.0.1, puis la version du paquet.

## Nettoyage

Avant de passer à l’action, il reste à nettoyer les sources. En effet, Subversion crée des dossiers `.svn/` qui n’ont rien à faire dans nos paquets !

    find empaquetage/tuxtremsplit/ -name ‘.svn’ -type d -exec rm -rf {} \;

## Créer le fichier source compressé

Créer un paquet debian veux bien dire empaqueter un code source téléchargé quelque part, pour le mettre dans un trentenaire de type debian.

Il faudra donc donner une archive de ces sources.

    tar -czf empaquetage/tuxtremsplit/library/libxtm-1.0.1.1.tar.gz empaquetage/tuxtremsplit/library/libxtm-1.0.1.1/
    tar -czf empaquetage/tuxtremsplit/console/tuxtremsplit-2.0.1.1.tar.gz empaquetage/tuxtremsplit/console/tuxtremsplit-2.0.1.1/

## Identification

Nous allons finir la préparation de notre plan de travail par notre identification au prêt de Launchpad pour la création du paquet !

Il existe deux variables d’environnements qui contiennent votre nom/pseudo et votre email. Généralement, ont met ça dans le fichier `~/.bashrc` comme ceci (remplacez pour vous) :

    DEBEMAIL=adresse.email.de.votre.compte.launchpad@exemple.org
    DEBFULLNAME='Launchpad PPA for Nom Prénom'

## dh_make

Les choses sérieuses commencent ! :-D

dh_make est une commande qui va vous créer un répertoire debian/ dans les sources ainsi qu’un tas de fichiers (dont une série que l’on supprimera), qui sont indispensables pour créer un paquet debian !

Commençons par installer dh_make ! :-)

    sudo apt-get install dh-make

Maintenant ouvrez un terminal dans le répertoire `empaquetage/tuxtremsplit/library/libxtm-1.0.1.1/` et lancez cette commande :

    $ dh_make -l -f ../libxtm-1.0.1.1.tar.gz

    Maintainer name : Launchpad PPA for Nom Prénom
    Email-Address : adresse.email.de.votre.compte.launchpad@exemple.org
    Date : Thu, 28 Aug 2010 20:29:00 +0200
    Package Name : libxtm
    Version : 1.0.1.1
    License : blank
    Using dpatch : no
    Using quilt : no
    Type of Package : Library
    Hit <enter> to confirm:

Pressez “Entrée” pour valider. Maintenant le dossier `debian/` est apparu, et contient un tas de fichiers.

# Préparation des fichiers de paquet

## Nettoyage

Pour commencer, il n’y a que quelques fichiers qui nous intéressent. Le reste, on le vire :

    rm debian/*.ex; rm debian/*.EX

changelog

Le fichier changelog est très important et respecte aussi une syntaxe très stricte. (Nombre d’espaces import etc…). Pour nous faciliter la tâche, nous allons utiliser la commande dch qui permet d’éditer simplement ce fichier changelog.

Cette commande n’est pas encore installée, alors commençons par ça :

    sudo apt-get install devscripts

Comme c’est la première création du paquet, et que dch ajoute au fichier changelog, et ne remplace pas l’existant, nous allons effacer le fichier changelog, et le re-créer :

    rm debian/changelog

    dch –create –package libxtm -v 1.0.1.1-1 “Initial release”

Pour en finir avec le fichier de log, il faut initialiser la release avec :

    dch -r

Attention à bien sauvegarder ! Vous devez trouver le nom de la release d’Ubuntu dans le fichier de log.

Maintenant, quand vous voudrez ajouter une entrée au fichier, vous n’aurez qu’à faire :

    dch -i “Le nouveau log à ajouter”

C’est tout ! :-)

## control

Le fichier control est le fichier principal car il contient toutes les infos importantes sur le paquet comme les dépendances, la description, la version, etc. Ouvrez-le avec votre éditeur de fichier texte préféré et en route !

Commencons par le plus dur : `Build-Depends`. Ce champ contient donc les dépendances qui seront utilisées pour construire/compiler votre application.
Pour TuXtremSplit vous devez mettre :

    Build-Depends: debhelper (>= 7), cmake, libmagic-dev, libglibmm-2.4-dev, libglademm-2.4-dev, libgtkmm-2.4-dev, libboost-regex-dev, libssl-dev

Je vous mets directement le fichier complet, ça sera plus simple :

    Source: libxtm
    Priority: extra
    Maintainer: Launchpad PPA for Guillaume HAIN <zedtux@zedroot.org>
    Build-Depends: debhelper (>= 7.0.50~), cmake, libmagic-dev, libglibmm-2.4-dev, libglademm-2.4-dev, libgtkmm-2.4-dev, libboost-regex-dev, libssl-dev
    Standards-Version: 3.9.1
    Section: libs
    Homepage: http://trac.zedroot.org/wiki/TuXtremSplitV2
    
    Package: libxtm-dev
    Section: libdevel
    Architecture: any
    Depends: libxtm (= ${binary:Version}, ${misc:Depends})
    Description: TuXtremSplit’s XTM file managment library (dev)
    Development files.
    
    Package: libxtm
    Section: libs
    Architecture: any
    Depends: {shlibs:Depends}, ${misc:Depends}
    Description: TuXtremSplit’s XTM file managment library
    Manage the XTM file format ( from Xtremsplit windows application )

## copyright

Fichier important pour ne pas froisser un développeur ! ;)

Voici le mien :

    This work was packaged for Debian by:
    
    Launchpad PPA for Guillaume HAIN on Sat, 17 Apr 2010 02:41:59 +0200
    
    It was downloaded from:
    
    http://trac.zedroot.org/wiki/TuXtremSplitV2
    
    Upstream Author(s):
    
    zedtux
    
    Copyright:
    
    GPLv3 [see http://www.gnu.org/licenses/gpl-3.0.txt]
    
    License:
    
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    
    This package is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with this program. If not, see .
    
    On Debian systems, the complete text of the GNU General
    Public License version 3 can be found in `/usr/share/common-licenses/GPL-3′.
    
    The Debian packaging is:
    
    Copyright (C) 2010 Launchpad PPA for Guillaume HAIN
    
    # Please chose a license for your packaging work. If the program you package
    # uses a mainstream license, using the same license is the safest choice.
    # Please avoid to pick license terms that are more restrictive than the
    # packaged work, as it may make Debian’s contributions unacceptable upstream.
    # If you just want it to be GPL version 3, leave the following line in.
    
    and is licensed under the GPL version 3, see above.
    
    # Please also look if there are files or directories which have a
    # different copyright/license attached and list them here.

## libxtm.install

Ce fichier précise comment installer le fichier de bibliothèque. Vous devez donc y mettre :

    usr/lib/lib*.so.*

Pour prendre en compte les fichiers lib*.so.* (donc libxtm.so.0 par exemple)

## libxtm-dev.install

Comme le précédent, sauf qu’il contiendra les fichiers d’en-tête. Il faut y mettre :

    usr/include/*
    usr/lib/*.so

## Construction du paquet

Maintenant nous sommes prêts pour créer notre paquet deb ! (Toujours dans le dossier `empaquetage/tuxtremsplit/library/libxtm-1.0.1.1/`)

    debuild -S -sa

A la fin de cette étape, il vous sera demandé 2 fois de suite la passphrase de votre clé GPG pour signer les paquets (libxm et libxtm-dev).

# Envoie au PPA

Voici la dernière étape ! Envoyer tout ça au PPA pour qu’il lance la construction et pour qu’il mette en ligne votre paquet !

Pour ce faire, il faut utiliser la commande `dput`.

Dans le dossier `empaquetage/tuxtremsplit/library/` :

    dput ppa:zedtux/tuxtremsplit libxtm_1.0.1.1-1_source.changes

Vous trouverez pour votre PPA la commande à exécuter dans l’interface de Launchpad.

Vous allez recevoir un email dans quelques instants, vous indiquant si votre demande est acceptée ou pas, et en vous expliquant pourquoi.

Il ne vous reste plus qu’a surveiller la construction et voir en cas de problèmes le pourquoi et le fixer ! :-)