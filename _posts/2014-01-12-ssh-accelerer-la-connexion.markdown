---
layout: post
title: 'SSH: Accélérer la connexion'
date: '2014-01-12 17:42:12'
tags:
- command-line
---

![Terminal](/content/images/2017/08/Terminal_128x128-3.png)

Se connecter à SSH prend parfois un peu de temps, car SSH effectue une recherche DNS inversé, qui est tout à fais inutile.

Pour désactiver cette recherche à la connexion, il suffit de modifier le fichier `/etc/ssh/sshd_config` et d’y ajouter ceci:

    UseDNS no

Redémarrez SSH, et essayez de vous connecter de nouveau… vous devriez voire une différence ! ;-) 