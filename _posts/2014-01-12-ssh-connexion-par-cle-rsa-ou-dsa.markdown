---
layout: post
title: 'SSH: Connexion par clé RSA ou DSA'
date: '2014-01-12 18:03:27'
tags:
- server
- command-line
---

![Terminal](/content/images/2017/08/Terminal_128x128-2.png)

SSH est un outil très bien pensé, et est vraiment adapté aux besoin d’administration d’un serveur distant.

Ici, nous allons parler de la connexion aux moyens de clés **RSA** ou **DSA**.

## Attend ! C’est quoi ca DSA et RSA ??

Ce sont deux modules qui permettent de valider l’authenticité d’une connexion.

Lorsque [DSA](http://fr.wikipedia.org/wiki/Rivest_Shamir_Adleman) ne fait que signer la connexion, [RSA](http://fr.wikipedia.org/wiki/Digital_Signature_Algorithm) la signe et la crypte en plus.

## Les avantages

Les avantages d’utiliser une connexion signé et/ou crypté sont multiples.

Le premier est qu’il est plus sécurisé forcément. Les sniffeurs réseau ne pourront pas voire ce que vous faites.

Le serveur vous autorise la connexion, car il sait que c’est vous, et pas une personne d’autre (A condition de ne pas compromettre les clés !!)

Il est possible d’automatiser la connexion, ne plus avoir d’interactivité. (Peut être utile pour des scripts).

# Création des clés

Bon, comme vous vous doutez bien que je vais vous faire signer mais aussi crypter la connexion ! :)

    ssh-keygen

Par defaut, `ssh-keygen` utilise **RSA**. Si vous désiriez utiliser **DSA**, vous devez ajouter `-t dsa`.

    Enter file in which to save the key (…):

Ici, faites juste entrer pour valider.

    Enter passphrase (empty for no passphrase):

Ici, si vous désirez une connexion automatique, appuyez sur entrer, sinon.. entrer une passphrase :D

    Your identification has been saved in /home/zedtux/.ssh/id_rsa.
    Your public key has been saved in /home/zedtux/.ssh/id_rsa.pub.
    The key fingerprint is:
    2c:2f:6b:a1:2f:cc:58:ae:32:83:28:54:f4:19:4a:71 zedtux@zUbuntu
    The key’s randomart image is:
    +–[ RSA 2048]—-+
    | ..E          |
    | o..          |
    | o o o        |
    | o o .        |
    | . . S        |
    | . . .o       |
    |+ * ….        |
    |B . * .o      |
    |.+.. +o       |
    +—————–————————+

Voilà !

Les clés sont généré !

## Les clés ? Comment ca ? J’ai lancé qu’une fois ssh-keygen !!?

Oui, c’est normal.

Je dis les clés car il y en a deux de créé: La clé publique et la clé privé.

La clé publique est, comme son nom l’indique, une clé qui peut être mise à disposition du grand publique sans risques.

Elle est utile lorsque quelqu’un veux vous contacter ou vos passer quelque chose, mais rien qu’a vous.

La clés privé, quand à elle, est extrêmement sensible !!

Si vous vous la faisiez volé, toutes connexions signé par celle-ci, tout fichiers crypté par cette clé, tout ca seraient compromis, et vous seriez dans l’obligation de remplacer tout le jeu de clés !

Comme si vous perdiez les clés de chez vous…

# Passer votre clé privé au serveur distant

Pour qu’une connexion soit signé et/ou crytpé, il faut que les deux parties (vous et le distant), possédiez la clé publique.

Pour ce faire, il faut utiliser la commande ssh-copy-id :

    ssh-copy-id -i ~/.ssh/id_rsa.pub root@serveur_distant

# Connexion !

Allez, y a plus qu’a comme ont dis ! :)

Attention si vous avez paramétré le fichier de configuration de SSH sur la machine distante.

Il est possible de ne pas autoriser ou plustôt désactiver les connexions par RSA ou DSA.
Si vous aviez désactiver les ces modes, et/ou changé le chemin des clés, vérifiez les.

Connectez vous, et si vous n’avez pas mis de passphrase, vous devriez être connecté directement, sinon la passphrase est demandé.