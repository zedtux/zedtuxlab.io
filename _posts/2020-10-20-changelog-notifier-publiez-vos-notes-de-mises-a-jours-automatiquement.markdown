---
layout: post
title: "changelog-notifier: Publiez vos notes de mises à jours automatiquement"
date: '2020-10-20 19:00:00'
tags:
- gem
---

L'idée m'est venu lorsque j'ai vue la façon dont mon client principale publie ses notes de mises à jours dans Slack.

À la fin du déploiement, fait avec [Capistrano](https://capistranorb.com), il est demandé à l'utilisateur d'écrire les changements qu'apporte cette nouvelle version. Le texte entré est posté dans Slack et une entrée dans la base de données est créée.

Il y a plusieurs problèmes avec cette approche dont le principale qui est la rédaction des notes très limitée car il ne faut surtout pas utiliser de caractères spéciaux, d'émojis etc ... sinon ca plante.

Et surtout, le déploie se retrouve bloqué le temps que l'utilisateur termine.

## Le principe de changelog-notifier

Le principe est le suivant: Vous maintenez un `CHANGELOG.md` selon les critères décrits sur le site https://keepachangelog.com/fr, puis vous taggez vos commits avec votre numéro de version.

Lorsque vous déploirez votre application, tout à la fin, la gem va regarder les étiquettes git (ou git _tags_) et si une étiquette est trouvée avec un numéro de version, il sera utilisé pour rechercher la version dans votre fichier `CHANGELOG.md`.

Les notes de la version sont extraits puis publiées sur Slack, si il est configuré, et/ou dans votre base de données si les options ActiveRecord de la gem sont renseignées.

Au final vous obtiendrez quelque chose de similaire à ceci:

![changelog-notifier example](/content/images/2020/10/changelog-notifier_example.png)

## L'architecture de la gem

Un mot sur l'architecture qui apporte une grande flexibilité.

Elle est composée 3 grands ensembles:

* Entrypoints
* Parsers
* Adapters

### Entrypoints

Les points d'entrés sont les déclancheurs qui vont appeler la gem. À ce jour, seul le déclancheur Capistrano existe.

Il va donc s'insérer dans le flux de Capistrano pour s'exécuter à la fin d'un déploiement.

Il aura besoin d'une petite revue afin d'extraire le code en trop, qui sera commun aux autres déclancheurs (Un déclancheur pour Kubernetes est prévu pour la suite).

### Parsers

Les analyseurs sont le coeur de la gem puisqu'ils alimentent les adaptateurs (voir la suite) basé sur la version retrouvée, précédement, dans les étiquettes de git.

Acutellement seule l'analyseur Markdown, qui suit les conventions du site https://keepachangelog.com/fr, permet de récupérer les types de changements (ajout, motification, suppression etc ...), ainsi que leurs détails.

### Adapters

Les adaptateurs vont utiliser la note de mise à jours pour publier.

À l'heure où j'écris ces lignes, il en existe 2:

* Slack
* ActiveRecord

#### Slack

Celui-ci publie un message dans Slack tel que montré dans la capture d'écran plus haut avec des couleurs par types de changements.

#### ActiveRecord

Quant à lui, il va créer une instance du modèle que vous pourrez passer en paramètres depuis le point d'entré, afin qu'il renseigne les champs `version` et `release_note` (les noms des champs sont configurables).

## Conclusion

Je pense sincairement que c'est une excellente idée d'automatiser la publication des notes de versions lorsque l'on travail dans une équipe, et se baser sur des convensions simples tel que celles de https://keepachangelog.com/fr me parait être l'idéal pour une adoption simple et rapide.

J'espère vraiment que cette gem trouvera des gens pour l'utiliser et à qui cela facilitera la vie! 🤗

Vous trouverez la gem et sa documentation ici: https://gitlab.com/zedtux/changelog-notifier.
