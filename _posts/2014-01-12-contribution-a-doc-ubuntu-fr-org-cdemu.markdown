---
layout: post
title: 'Contribution à doc.ubuntu-fr.org: CDEmu'
date: '2014-01-12 19:52:36'
tags:
- ubuntu
- contribution
---

![logo](/content/images/2017/08/logo.png)

J'ai rédigé hier soir une page sur [CDEmu](http://cdemu.sourceforge.net/) puisqu’elle manquait à l’appelle ! :)

CDEmu est un équivalent à [Daemon Tools](http://www.daemon-tools.cc/) pour ceux qui connaissent, mais pour Linux.

Si vous êtes intéressé, je vous invite à aller lire ma documentation: http://doc.ubuntu-fr.org/cdemu