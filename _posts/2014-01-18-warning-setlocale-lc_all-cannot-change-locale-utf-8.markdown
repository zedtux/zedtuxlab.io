---
layout: post
title: 'warning: setlocale: LC_ALL: cannot change locale (*.UTF-8)'
date: '2014-01-18 12:45:34'
tags:
- maintenance
- server
---

Si vous avez cet avertissement dans un terminal:

> warning: setlocale: LC\_ALL: cannot change locale (*.UTF-8)

(Avec votre locale a la place de l'étoile)

Il vous faut exécuter la commande suivante:

    sudo locale-gen *.UTF-8

Par exemple is le locale est **en_US.UTF-8** alors il faudra faire:

    zedtux$ sudo locale-gen en_US.UTF-8
    [sudo] password for zedtux: 
    /bin/bash: warning: setlocale: LC_ALL: cannot change locale (en_US.UTF-8)
    Generating locales...
      en_US.UTF-8... done
    Generation complete.