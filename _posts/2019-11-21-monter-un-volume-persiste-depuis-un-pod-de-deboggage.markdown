---
layout: post
title: Monter un volume persisté depuis un pod de déboggage
date: '2019-11-21 16:09:03'
tags:
- kubernetes
---

Prenez le cas d'un `Persistent Volume` créé grâce à un `Persistent Volume Claim`, et que le(s) pod(s) qui l'utilise ne puisse pas démarrer mais que vous devez accéder aux fichiers stoqué afin de déboguer un problème, voici comment procéder.

Cet article sera très court car il ne fait que présenter le fichier YAML à utiliser avec la commande `kubectl` de Kubernetes afin de créer le `Pod` qui vous permettra d'accéder aux fichiers.

Disons que le PV que vous devez accéder ai été créé par un PVC nommé `mysql-data-0`, et que vous désirez monter les fichiers dans le dossier `/var/lib/mysql`, voici le fichier YAML necessaire:

<script src="https://gist.github.com/zedtux/79dd0b38d00a2bb6b4a825c88f6edb66.js"></script>

Le partie la plus intéressante est à la fin du fichier.

`spec.volumes[0].name` determine le nom du volume utilisable dans le `Pod`, puis `spec.volumes[0].persistentVolumeClaim.claimName` permet de cibler le PVC par son nom.

Ensuite, dans le container, le volume sera monté grâce à `spec.containers[0].volumeMounts[0]` qui séléctionne le volume par son nom (`spec.volumes[0].name`) et le monte dans le chemin définie par `spec.containers[0].volumeMounts[0].mountPath`.

Créez ce `Pod` avec `kubectl create -f chemin/vers/le/fichier.yaml`, et une fois le `Pod` prêt (`watch -n 1 kubectl get pods` pour surveiller quand le `Pod` est prêt), accédez-y avec la command `kubectl exec -it mysql-debug -- bash`.

Voilà, vous avez accés aux fichiers contenus dans le volume.