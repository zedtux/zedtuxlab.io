---
layout: post
title: Message for me !
date: '2014-01-12 12:35:28'
tags:
- the-it-crowd
---

![The IT Crowd - Richard Ayoade](/content/images/2017/08/richardayoade460.jpg)

Étant fan de [The It Crowd](http://fr.wikipedia.org/wiki/The_IT_Crowd), j’ai récupéré un son, que j’utilise dans [Pidgin](http://pidgin.im/) !! :)

Dans la saison 3, épisode 5, le sujet principal parle de Friend Face, le site web communautaire ( parodie de Facebook ), où nos héros découvrent les rencontres par internet.
Durant l’épisode, ils parlent du petit son qui est joué quand ils ont un nouveau message, et de la possibilité de le changer.

Moss ( le guayar sur la photo plus haut ), parle avec sa mère par ce biais, et a créé son propre son: avec sa voix il a enregistré “Message for me !!”.

J’ai extrait ce petit son, et je vous le partage à tous !! :-)

Bien entendu, tout ceci fait avec des logiciels libres !! :-p

[Téléchargez le avec ce lien](http://depot.zedroot.org/divers/it-crowd_message-for-me.ogg).