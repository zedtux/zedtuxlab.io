---
layout: post
title: 'Kitchen + Berksfile: Where are stored the cookbooks?'
date: '2019-08-12 12:59:11'
tags:
- chef
- kitchen
---

I had the case where I'm using a local cookbook (to debug it) and I wanted to check what is in the kitchen, but where are the files?

Let's summarise the case: I'm developing a cookbook which dependends from the docker-ce cookbook. This last one seems to have an issue, therefore I'm adding it to the `Berksfile` of my current cookbook and run `kitchen test <name>` again.

Now I'd like to see what was transfered to the Kitchen (something expected is not happening, I'd like to check if the docker-ce cookbook source code in the Kitchen is correct).

## But where are the files?

I read, on a blog post, that Berksfile is storing the cookbooks in `~/.berkshelf/cookbooks` ... but those folders doesn't exists.

Using the `top` command, and pressing `C` shows the full command line, which shows me all the arguments, and by chance, one of them is the path to the kitchen files!

So now I know that Kitchen files, and therefore synced cookbooks are stored in `/tmp/kitchen/`, and cookbooks in `/tmp/kitchen/coobkooks`.

That's it.