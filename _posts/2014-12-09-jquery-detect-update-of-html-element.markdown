---
layout: post
title: 'jQuery: Detect update of HTML element'
date: '2014-12-09 19:12:33'
tags:
- jquery
- javascript
---

While I was working on fixing some Cucumber tests in a Rails project having Twitter Boostrap and the unobtrusive\_flash gem, I was needing to see the update of the flash message in my application while the tests were running.

You can achive this with the following snippet:

<script src="https://gist.github.com/zedtux/0686cb3cede6998ab780.js"></script>

When the div having the class **unobtrusive-flash-container** is updated, the text is logged in to the console of your browser.

Source: http://stackoverflow.com/a/4979828/1996540