---
layout: post
title: 'OS X Java: No such file or directory – Contents/Home/Home/include (Errno::ENOENT)'
date: '2014-01-12 20:29:21'
tags:
- ruby
---

Je viens d’avoir ce petit soucis au boulot, et comme j’ai la solution, je la partage ! :)

Une fois passé à OS X Lion, et Java installé par le Software update, j’ai donc lancé l’installation des gems d’un project Ruby On Rails ayant comme dépendance rjb.

Mais pendant son installation, voici l’erreur que j’avais:

    Installing rjb (1.2.6) with native extensions /Users/zedtux/.rvm/rubies/ree-1.8.7-2011.03/lib/ruby/site_ruby/1.8/rubygems/installer.rb:533:in `build_extensions': ERROR: Failed to build gem native extension. (Gem::Installer::ExtensionBuildError)
    
    /Users/zedtux/.rvm/rubies/ree-1.8.7-2011.03/bin/ruby extconf.rb
    *** extconf.rb failed ***
    Could not create Makefile due to some reason, probably lack of
    necessary libraries and/or headers.  Check the mkmf.log file for more
    details.  You may need configuration options.
    
    Provided configuration options:
    --with-opt-dir
    --without-opt-dir
    --with-opt-include
    --without-opt-include=${opt-dir}/include
    --with-opt-lib
    --without-opt-lib=${opt-dir}/lib
    --with-make-prog
    --without-make-prog
    --srcdir=.
    --curdir
    --ruby=/Users/zedtux/.rvm/rubies/ree-1.8.7-2011.03/bin/ruby
    extconf.rb:40:in `open': No such file or directory - /System/Library/Java/JavaVirtualMachines/1.6.0.jdk/Contents/Home/Home/include (Errno::ENOENT)
    from extconf.rb:40
    
    Gem files will remain installed in /Users/zedtux/.rvm/gems/ree-1.8.7-2011.03/gems/rjb-1.2.6 for inspection.
    Results logged to /Users/zedtux/.rvm/gems/ree-1.8.7-2011.03/gems/rjb-1.2.6/ext/gem_make.out
    from /Users/zedtux/.rvm/rubies/ree-1.8.7-2011.03/lib/ruby/site_ruby/1.8/rubygems/installer.rb:486:in `each'
    from /Users/zedtux/.rvm/rubies/ree-1.8.7-2011.03/lib/ruby/site_ruby/1.8/rubygems/installer.rb:486:in `build_extensions'
    from /Users/zedtux/.rvm/rubies/ree-1.8.7-2011.03/lib/ruby/site_ruby/1.8/rubygems/installer.rb:159:in `install'
    from /Users/zedtux/.rvm/gems/ree-1.8.7-2011.03/gems/bundler-1.0.15/lib/bundler/source.rb:101:in `install'
    from /Users/zedtux/.rvm/gems/ree-1.8.7-2011.03/gems/bundler-1.0.15/lib/bundler/rubygems_integration.rb:78:in `preserve_paths'
    from /Users/zedtux/.rvm/gems/ree-1.8.7-2011.03/gems/bundler-1.0.15/lib/bundler/source.rb:91:in `install'
    from /Users/zedtux/.rvm/gems/ree-1.8.7-2011.03/gems/bundler-1.0.15/lib/bundler/installer.rb:58:in `run'
    from /Users/zedtux/.rvm/gems/ree-1.8.7-2011.03/gems/bundler-1.0.15/lib/bundler/rubygems_integration.rb:93:in `with_build_args'
    from /Users/zedtux/.rvm/gems/ree-1.8.7-2011.03/gems/bundler-1.0.15/lib/bundler/installer.rb:57:in `run'
    from /Users/zedtux/.rvm/gems/ree-1.8.7-2011.03/gems/bundler-1.0.15/lib/bundler/spec_set.rb:12:in `each'
    from /Users/zedtux/.rvm/gems/ree-1.8.7-2011.03/gems/bundler-1.0.15/lib/bundler/spec_set.rb:12:in `each'
    from /Users/zedtux/.rvm/gems/ree-1.8.7-2011.03/gems/bundler-1.0.15/lib/bundler/installer.rb:49:in `run'
    from /Users/zedtux/.rvm/gems/ree-1.8.7-2011.03/gems/bundler-1.0.15/lib/bundler/installer.rb:8:in `install'
    from /Users/zedtux/.rvm/gems/ree-1.8.7-2011.03/gems/bundler-1.0.15/lib/bundler/cli.rb:222:in `install'
    from /Users/zedtux/.rvm/gems/ree-1.8.7-2011.03/gems/bundler-1.0.15/lib/bundler/vendor/thor/task.rb:22:in `send'
    from /Users/zedtux/.rvm/gems/ree-1.8.7-2011.03/gems/bundler-1.0.15/lib/bundler/vendor/thor/task.rb:22:in `run'
    from /Users/zedtux/.rvm/gems/ree-1.8.7-2011.03/gems/bundler-1.0.15/lib/bundler/vendor/thor/invocation.rb:118:in `invoke_task'
    from /Users/zedtux/.rvm/gems/ree-1.8.7-2011.03/gems/bundler-1.0.15/lib/bundler/vendor/thor.rb:246:in `dispatch'
    from /Users/zedtux/.rvm/gems/ree-1.8.7-2011.03/gems/bundler-1.0.15/lib/bundler/vendor/thor/base.rb:389:in `start'
    from /Users/zedtux/.rvm/gems/ree-1.8.7-2011.03/gems/bundler-1.0.15/bin/bundle:13
    from /Users/zedtux/.rvm/gems/ree-1.8.7-2011.03/bin/bundle:19:in `load'
    from /Users/zedtux/.rvm/gems/ree-1.8.7-2011.03/bin/bundle:19

Mon JAVA_HOME étant définie comme ceci:

	export JAVA_HOME=$(/usr/libexec/java_home)

et donc contenant:

    echo $JAVA_HOME
    /Library/Java/JavaVirtualMachines/1.6.0_26-b03-383.jdk/Contents/Home

Pour corriger ce problème, il suffit de créer un lien symbolique manquant qui ajoutera le dossier include dans le JAVA_HOME comme ceci:

	sudo ln -s /System/Library/Frameworks/JavaVM.framework/Headers $JAVA_HOME/include

Maintenant l’installation de RJB re-fonctionne ! ;-)