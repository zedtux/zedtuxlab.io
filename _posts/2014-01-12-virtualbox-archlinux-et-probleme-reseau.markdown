---
layout: post
title: 'VirtualBox: Archlinux et problème réseau'
date: '2014-01-12 12:25:53'
tags:
- linux
---

![Logo Archlinux](/content/images/2014/Jan/128px_archlinux_icon_128svg.png)

Je vient d’installer [VirtualBox](https://www.virtualbox.org/) sous Ubuntu Jaunty, afin d’essayer [Archlinux](https://www.archlinux.org/), mais j’ai tout de suite eu un soucis de réseau.

Mes connaissances Linux ne m’ont pas beaucoup aidé sur ce coup ci et je me suis retrouvé dans une impasse.

J’ai donc fais appel à la communauté, et tout fonctionne maintenant grâce à ce poste : http://ubuntuforums.org/archive/index.php/t-762846.html

Voici la démarche à suivre, pour que le réseau fonctionne :

1. Lancer l’installation de Arch.
2. Une fois installé, il vous est possible de modifier les fichiers de configuration.

A cette étape, modifier le premier fichier `/etc/rc.conf`

Vous trouverez un peu plus bas :

    HOSTNAME=”localhost”

Qu’il faut changer avec ce que vous voulez. Moi j’ai mis arch … ;-)

Plus bas, encore une fois, vous devriez trouver ceci:

    eth0=”eth0 192.168.0.2 netmask 255.55.255.0 broadcast 192.168.0.255″

A changer en :

    eth0=”dhcp”

Et voilà ! C’est tout !
Vous devriez être capable d’accéder à internet, donc d’utiliser **pacman**.