---
layout: post
title: 'error: Could not load host key: /etc/ssh/ssh_host_ecdsa_key'
date: '2014-01-18 13:27:21'
tags:
- maintenance
- server
---

Souvent j'utilise la commande `tail` sur mes serveurs dans une fenêtre séparée afin de toujours avoir un oeil sur mes logs pendant que je bosse sur le serveur.

Ce coup-ci j'ai vue l'erreur suivante:

> Jan 18 14:52:14 vps sshd[800]: error: Could not load host key: /etc/ssh/ssh\_host\_ecdsa\_key

J'ai trouvé la solution dans [ce rapport de bug](https://bugs.launchpad.net/ubuntu/+source/openssh/+bug/1005440) sur launchpad.net:

    zedtux@vps:~$ sudo dpkg-reconfigure openssh-server
    [sudo] password for zedtux: 
    Creating SSH2 ECDSA key; this may take some time ...
    ssh stop/waiting
    ssh start/running, process 1123

Comme vous pouvez le voir une clé ECDSA est générée ce qui résou l'erreur.