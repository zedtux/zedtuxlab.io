---
layout: post
title: Restaurer sa prod, en 30 minutes, après un crash grâce à Docker
date: '2018-03-10 11:09:25'
tags:
- docker
---

L'environnement de production d'une application sur laquelle je travails depuis un moment déjà, s'est éffondré suite à un problème disque, et je vais la remettre en route en 30 minutes!

J'ai développé ce projet dans Docker depuis le premier jour, et j'ai donc naturellement utilisé Docker pour déployer l'application via la plate-forme [Docker Cloud](https://cloud.docker.com).

Le plus long, lor du re-déploiement, c'est le téléchargement des images Docker. :)

Comme Docker Cloud stoque le *Stack* de l'application, comprennez l'équivalent du fichier `docker-compose.yml`, et comme Docker Cloud peut gérer les serveurs sur lesquels tourne l'application, le re-déploiement se passe de la façon suivante:

1. Dans les *Node Clusters* je séléctionne celui de la production (nous en avons un par environnement) puis je fais un *scale* de 1 à 2 (Le cluster de production contient alors l'ancien serveur mort, et le fait de *scaler* de 1 à 2 force Docker Cloud de créer un nouveau serveur avec exactement la même comfiguration que le serveur HS).
    1. Dans cette étape, Docker Cloud créer un nouveau serveur
    2. Une fois le serveur démarré, il installe Docker
    3. Puis, une fois Docker installé et en route, il déploie ses images lui permettant de gérer le serveur: surveillance des paramètres et déploiements.
2. À partir de ce moment, je supprime l'ancien serveur, afin que Docker Cloud ne le prenne plus en compte.
3. Puis je reprends mon *Stack* de production et je fais un *Redeploy*.
    1. Ici les images sont téléchargées sur le nouveau serveur
    2. De nouvelles instances sont crées, l'application répond aux requêtes (après mise à jour du DNS).
4. Pour finir, je restaure la base de données grâce à l'image Docker que j'ai crée: [docker-pg_dump-to-swift](https://github.com/YourCursus/docker-pg_dump-to-swift).

## docker-pg_dump-to-swift

Je voulais écrire cet article afin de présenter cette image qui permet de terminer le re-déploiement de zéro très rapidement.

Cette image permet de sauvegarder une base de données [PostgreSQL](https://www.postgresql.org) (Versions supportés: 9.1 à 9.5), sur un espace [OpenStack swift](https://www.swiftstack.com/product/openstack-swift) de façon récurante.

L'interêt de stoquer les sauvegardes sur un espace swift c'est que [OVH fait une offre à 0,01 € par mois et par Go](https://www.ovh.com/fr/public-cloud/storage/object-storage/). Tant que vous ne stoquez pas 1 Go, le stoquage est gratuit. 😁

Mais cette image Docker ne fait pas que cela: elle permet aussi de restaurer une base de données, avec la dernière sauvegarde trouvé, simplement en lancant la commande `restorelast` (Voire le détail dans [le fichier `README.md` du projet](https://github.com/YourCursus/docker-pg_dump-to-swift/blob/master/README.md)).

### Comment restaurer la base de données via Docker Cloud?

Mon fichier *stack* chez Docker Cloud contient un noeux pour la sauvegarde chaque nuits à 1 heure:

<script src="https://gist.github.com/zedtux/8eee37a95fb910567ad27fa38dd09b27.js"></script>

Ici pas de `command` donc il lance celle par défaut de l'image: La sauvegarde, chaque nuits, de la base de données.

Maintenant je veux simplement restaurer ma base de données depuis la dernière sauvegarde, je duplique ce bloque, je le renomme, et j'y ajoute la commande `restorelast`:

<script src="https://gist.github.com/zedtux/49c12dda4be31a3c46496124815049bf.js"></script>

Une fois sauvegardé, Docker Cloud créer le nouveau container sur le serveur, je n'ai plus qu'à le lancer et surveiller les logs:

```bash
Restore Job started: Sat Mar 10 10:16:52 UTC 2018
Restauring app_production on 10.17.0.10:5432 using the file tmp/20180227-010001.sql ...
tmp/20180227-010001.sql [auth 0.672s, headers 1.039s, total 1.426s, 0.065 MB/s]
Re-creating app_production database ...
NOTICE:  database "app_production" does not exist, skipping
Importing dump file ...
SET
SET
SET
SET
SET
SET
SET
CREATE EXTENSION
COMMENT
CREATE EXTENSION
COMMENT
SET
SET
SET
CREATE TABLE
ALTER TABLE
CREATE SEQUENCE
ALTER TABLE
ALTER SEQUENCE
CREATE TABLE
...
CREATE INDEX
CREATE INDEX
REVOKE
REVOKE
GRANT
GRANT
```

Voilà! La base de données est restauré, et mon application est fin prête.

En 30 minutes, j'ai remis l'application en route.