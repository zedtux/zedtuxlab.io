---
layout: post
title: unexpectedly shrunk window xxxxxx:xxxxxx (repaired)
date: '2014-01-12 19:23:09'
tags:
- maintenance
- server
- security
---

![skull_pirate_small](/content/images/2017/08/skull_pirate_small.gif)

unexpectedly shrunk window xxxxxx:xxxxxx (repaired) !!!!!!??

Voilà le genre de log que je retrouve sur mon serveur ces derniers temps après les logs Treason Uncloacked.

# Ca veux dire quoi ?

Honnêtement… je sais quand apparaissent ces logs, où plustot, pourquoi, mais par contre, je ne sais pas si c’est une attaque de type DoS (Denial of Service), ou autres… mais ce que je sais, c’est que ca ralentis mon serveur et donc, je dois limiter au maximum ce problème.

# OK, comment ?

Là, je n’ai pas cherché pendant des heures, je passe par [fail2ban](http://www.fail2ban.org/) !

Le but est simple: Ajouter une règle pour que dès que ce log apparait, fail2ban banni l’adresse IP de la personne en cause pendant X temps.

# Le filtre fail2ban

Voici le filtre fail2ban que j’ai écris, placé dans `/etc/fail2ban/filter.d/shrunk-window.conf` :

    [Definition]
    failregex = TCP\: Peer <HOST>\:.* unexpectedly shrunk window.*repaired+
    ignoreregex =

Ensuite, pour activer le filtre, tout à la fin du fichier `/etc/fail2ban/jail.conf` :

    [shrunk-window]
    enabled = true
    filter = shrunk-window
    logpath = /var/log/kern.log
    port = all
    banaction= iptables-allports
    port = anyport
    maxretry = 1

Après un redémarrage de fail2ban et quelques heures d’attente, le résultat se fait resentir :
Logwatch me dit:

    ——————— Kernel Begin ————————

    1 Time(s): TCP: Peer 211.87.234.41:56842/52350 unexpectedly shrunk window 3774735988:3774737448 (repaired)
    1 Time(s): TCP: Peer 211.87.234.41:62249/52350 unexpectedly shrunk window 1413085657:1413091497 (repaired)

    ———————- Kernel End ————————-
    ——————— fail2ban-messages Begin ————————
    Banned services with Fail2Ban: Bans:Unbans
    shrunk-window: [ 2:2 ]
    211.87.234.41 2:2
    ———————- fail2ban-messages End ————————-

Voila, l’ip se faire bannir direct… :-)