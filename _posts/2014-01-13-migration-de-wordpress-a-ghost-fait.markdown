---
layout: post
title: 'Migration de Wordpress à Ghost: Fait!'
date: '2014-01-13 19:07:36'
tags:
- information-divers
---

Je suis ravis de vous présenter mon nouveau blog!

J'utilise, comme dis dans le titre, Ghost. C'est une nouvelle plat-form de blog écrit en Node.js.
(J'explique dans [cet article](/le-développement-en-mode-“non-blocking”/) ce qu'est Node.js :-))

## Pourquoi avoir changé ?

[Ghost](https://ghost.org/) est simple d'installation et d'utilisation. Il comporte très peu d'options et contient des fonctionnalitées vraiment excellentes!

Par exemple une des fonctionnalitées qui fait de Ghost une plat-form excellente est le rendu en temps réel de l'article que vous écrivez !

Une autre fonctionnalitée qui m'a convaincut de migrer de Wordpress à Ghost c'est que les articles sont écrits en Markdown.
Cela permet d'écrire plus rapidement des articles et ceux sans avoit de bar d'outils pour changer le text ou créer des liens etc...

Vous pouvez voire à quoi resemble Ghost à [la page des "features"](https://ghost.org/features/).
Il y a une vidéo qui vous montre aussi la partie Admin.
C'est en Anglais, mais les image sont en Francais elles. :-)

Très franchement je vous le conseille! :-)