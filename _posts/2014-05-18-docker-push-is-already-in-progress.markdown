---
layout: post
title: Docker push is already in progress
date: '2014-05-18 06:37:27'
tags:
- docker
---

Si vous obtenez le message suivant lorsque vous tentez de pousser votre image :

    2014/05/18 08:33:39 Error: push quay.io/zedtux/brewformulas.org is already in progress

Cela veux dire que vous avez fait un `CTRL + C` pendant un `docker push`.

Pour résoudre le problème, il vous faut redémarrer docker:

	sudo service docker restart

Et maintenant vous devriez pouvoir relancer la commande `docker push`.