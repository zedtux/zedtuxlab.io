---
layout: post
title: 'Douane: Daemon is in the pipe!'
date: '2014-01-12 20:36:09'
tags:
- my-developments
- douane
---

In [my previous article](/douane-kernel-module-stabilization/), I was speaking about stabilization of my Kernel module.

This is now quite done. The kernel module is working well (no more crashing of the Virtual Machine). I just have to keep in mind to check for memory leaks.

# Current state

Well now, the next step is the [Daemon](http://en.wikipedia.org/wiki/Daemon_%28computing%29)!

So I’m back to my C++ courses. I’d forgot a lot, but knowledge is coming back again. :-p

# Something to show us?

Erf… no. No graphical stuff to show you.