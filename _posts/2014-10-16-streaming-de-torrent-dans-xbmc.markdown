---
layout: post
title: Streaming de torrent dans XBMC
date: '2014-10-16 12:29:44'
---

Voici une extension pour XBMC qui permet de regarder des torrents en même temps que vous les téléchargez.

Le principe rappelle fortement [Popcorn Time](https://popcorntime.io).

L'auteur de l'extension a fait une vidéo que voici:

<iframe width="420" height="315" src="//www.youtube.com/embed/NQiC62ig3N0" frameborder="0" allowfullscreen></iframe>

L'installation est vraiment très simple puisqu'XBMC propose une option d'installation à partir d'un fichier ZIP et justement l'extension est disponible au [format ZIP](https://github.com/steeve/xbmctorrent/releases) (Téléchargez le fichier plugin.video.xbmctorrent-0.6.4.zip) et [les sources](https://github.com/steeve/xbmctorrent) sont aussi disponible.