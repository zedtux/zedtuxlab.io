---
layout: post
title: Ouvrir une console Rails avec Capistrano
date: '2015-10-15 13:41:16'
tags:
- rails
---

Voici comment écrire une "recipe" Capistrano qui vous permettra d'ouvrir une console Rails depuis votre machine locale.

Créez le fichier `lib/capistrano/tasks/rails.rake` et y copier/coller ceci:

<script src="https://gist.github.com/zedtux/1bddaaaf47bfef0001c3.js"></script>

Maintenant vous pouvez faire `cap staging rails:console` et vous avez une console en staging :)