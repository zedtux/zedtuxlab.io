---
layout: post
title: Corebird, un client Twitter pour GNOME
date: '2015-04-19 15:29:19'
tags:
- debian
---

![corebird](/content/images/2017/08/corebird.png)

Corebird est un client Twitter, avec plein de fonctionnalités interessante.

Voici comment le compiler sur [Debian](https://www.debian.org/) (Jessie).

English version: http://blog.zedroot.org/corebird-is-a-twitter-client-for-gnome/

### Cloner le code source

La première étape est bien sûr de recuperer le code source depuis [le dépôt Git](https://github.com/baedert/corebird):

    $ git clone https://github.com/baedert/corebird.git
    $ cd corebird/

### Selectionner la version

Par défaut vous serez sur la branche **master** ce qui veux dire la branche de développement.
Si vous désirez voir les dernières fonctionnalités vous pouvez rester sur cette branche mais si vous désirez l'installer pour l'utiliser (version stable) alors il vaut mieux choisir une version.

Corebird propose plusieurs versions que vous pourrez trouver sur [la page de version du dépôt Github](https://github.com/baedert/corebird/releases).

Ici je vais compiler la version 1.0:

    $ git checkout 1.0

### Installer les dépendances

Pour pouvoir compiler Corebird, il vous faudra certain paquets, peut-être pas encore installé sur votre système, comme ceci:

    $ sudo apt-get install autoconf intltool libtool libsqlite3-dev librest-dev libgee-0.8-dev libjson-glib-dev libgstreamer-plugins-base1.0-dev valac

### Compilation

Dernière étape, la compilation:

    $ ./autogen.sh --prefix=/usr
    $ make
    $ sudo make install

### Demarrer Corebird

Maintenant que le logiciel est installé, vous trouverez un menu "corebird" pour le lancer, ou sinon vous pouvez aussi le lancer dans une console:

    $ corebird

Et vous obtiendrez ceci:

![Screenshot-from-2015-04-19-16-58-31](/content/images/2017/08/Screenshot-from-2015-04-19-16-58-31.png)