---
layout: post
title: 'Linux On Mac: SSD unleached'
date: '2015-04-18 14:05:10'
tags:
- linux-on-mac
---

Recently I was running my Cucumber features in parallel and it was really slow (same speed as running normally the tests).

Using the `atop` command I've identified that the `DSK` (Disk) line was red all the time with poor performances.

Reading around about Linux and SSD drives lead me to update the `/etc/fstab` file in order to add flags to the mount command for my SSD drive as the following:

```
# <file system> <mount point>   <type>  <options>       <dump>  <pass>
# / was on /dev/sda5 during installation
UUID=befd193c-ff6a-4f62-a0a2-de044fe6abe8 /               ext4    defaults,noatime,nodiratime,errors=remount-ro 0       1
/dev/sdb1       /media/usb0     auto    rw,user,noauto  0       0
/dev/sdb2       /media/usb1     auto    rw,user,noauto  0       0
```

Also I have added the following lines:

```
# Have the /tmp folder in memory
tmpfs /tmp tmpfs defaults,size=100m 0 0

# Save logs in memory
ram /var/log tmpfs defaults,nosuid,nodev 0 0
```

After a reboot the disk spead has been increased a lot ! Now the tests are really running fast.