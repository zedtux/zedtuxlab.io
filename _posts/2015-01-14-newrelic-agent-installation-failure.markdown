---
layout: post
title: Newrelic agent installation failure
date: '2015-01-14 21:06:43'
tags:
- apt
- docker
---

I just started my script in order to build a new version of a Docker image with fresh code and it failed with the following error:

	dpkg: error processing newrelic-sysmond (--configure):
	 subprocess installed post-installation script returned error exit status 1
	Errors were encountered while processing:
	 newrelic-sysmond
	E: Sub-process /usr/bin/dpkg returned an error code (1)

This issue has been [reported to newrelic](https://discuss.newrelic.com/t/nsysmond-install-error/13335) but seems to be abandonned.
I have posted a comment in order to bump it, we will see.

Now how can I manage this issue ? Installing a previous, working, version ? Let's try it.

## List existing versions of an apt package

Looking around on the Internet pointed me to the following command:

    $ apt-cache madison newrelic-sysmond
    newrelic-sysmond |  2.0.0.105 | http://apt.newrelic.com/debian/ newrelic/non-free amd64 Packages
	newrelic-sysmond |  2.0.0.103 | http://apt.newrelic.com/debian/ newrelic/non-free amd64 Packages
	newrelic-sysmond |   1.5.1.93 | http://apt.newrelic.com/debian/ newrelic/non-free amd64 Packages
	newrelic-sysmond |   1.5.0.81 | http://apt.newrelic.com/debian/ newrelic/non-free amd64 Packages
	newrelic-sysmond |  1.4.3.478 | http://apt.newrelic.com/debian/ newrelic/non-free amd64 Packages
	newrelic-sysmond |  1.4.2.476 | http://apt.newrelic.com/debian/ newrelic/non-free amd64 Packages
	newrelic-sysmond |  1.4.0.471 | http://apt.newrelic.com/debian/ newrelic/non-free amd64 Packages
	newrelic-sysmond |  1.3.1.437 | http://apt.newrelic.com/debian/ newrelic/non-free amd64 Packages

It seems that this version 2.0.0.x has an issue, and I don't have the time right now to check this, so let's rollback to the version 1.5.1.93.

## Specifying apt package version

My `Dockerfile` has the following `RUN` instruction:

    RUN echo deb http://apt.newrelic.com/debian/ newrelic non-free >> /etc/apt/sources.list.d/newrelic.list && \
      curl https://download.newrelic.com/548C16BF.gpg | apt-key add - && \
      apt-get update && \
      apt-get install -y libpq-dev \
      newrelic-sysmond \
      libmagic-dev

This line add the newrelic Debian package registry URL, import their key and install some packages including the package `newrelic-sysmond`.

To define the version to install you have to add `=<version>` after the package name:

    RUN echo deb http://apt.newrelic.com/debian/ newrelic non-free >> /etc/apt/sources.list.d/newrelic.list && \
      curl https://download.newrelic.com/548C16BF.gpg | apt-key add - && \
      apt-get update && \
      apt-get install -y libpq-dev \
      newrelic-sysmond=1.5.1.93 \
      libmagic-dev

Now the building of the image is working, I can breathe.