---
layout: post
title: 'Pidgin: filtrer les Spam !'
date: '2014-01-12 18:07:10'
tags:
- linux
---

![Pidgin](/content/images/2017/08/pidgin.png)

Dans Pidgin, par défaut, tout le monde peut vous parler… Sans être dans votre liste d’amis !

Le problème pour moi, c’est que lorsque ma belle sœur a testé Ubuntu, elle a reçu du spam par MSN, alors que sous Windows cela n’arrivais pas !

Vraiment pas une bonne idée !

## Comment filtrer les spams pour toujours !?

Pidgin comporte une option pour ca… et c’est dommage qu’elle soit mal réglé par défaut !

Pour filtrer ces spams, il faut aller dans **Outils > Filtres**.

Puis sélectionner :

![Pidgin - Filters](/content/images/2017/08/pidgin_filters.png)

Et voilà !