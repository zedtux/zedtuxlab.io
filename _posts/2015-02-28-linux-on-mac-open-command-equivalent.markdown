---
layout: post
title: 'Linux On Mac: open command equivalent'
date: '2015-02-28 18:16:40'
tags:
- linux-on-mac
---

When I was developing with OS X, I was often using the `open` command which opens the given file with the right application.
For instance, giving a PDF path to the command opens Preview.

On Linux the equivalent is `see`.