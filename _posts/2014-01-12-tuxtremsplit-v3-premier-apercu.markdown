---
layout: post
title: 'TuXtremSplit v3: Premier apercu'
date: '2014-01-12 20:01:53'
tags:
- python
- mes-developpements
- my-developments
- tuxtremsplit
---

![tuxtremsplit_v3_0_actionwindow](/content/images/2017/08/tuxtremsplit_v3_0_actionwindow.png)

Voici un premier aperçu de la fenêtre dite d’action, qui affiche ce qui se passe.

Ici l’idée est que lorsque vous double-cliquerez sur un fichier .001.xtm (valide), TuXtremSplit se lancera automatiquement, affichant cette fenêtre.
J’ai trouvé bonne l’idée de la petite phrase détaillant le fichier contenu dans les fichiers XTM, sa taille etc…

Seule bémol pour le moment (je réfléchis à une solution), les fichiers auto-extractible généré par Xtremsplit sont reconnut comme application Windows à exécuter avec Wine. Cela dis, si le système d’identification des fichiers de [freedesktop.org](http://freedesktop.org/) est assez complet, peut-être il sera possible de détecter l’entête d’un XTM, et du coup, l’associer à TuXtremSplit.
Autre problème: Pour une raison inconnu, le développeur d’[Xtremsplit](http://xtremsplit.fr/) utilise [UPX (Ultimate Packer for eXecutables)](http://upx.sourceforge.net/) pour empaqueter le premier fichier .001.exe, du coup l’entête est offusqué… Donc à voire comment gérer cette drôle de situation.

**Si vous avez des propositions, idées, suggestions, n’hésitez pas, je suis ouvert !**

P.S: L’étoile ici est juste là temporairement, le temps d’essayer de trouver une icone plus adapté.