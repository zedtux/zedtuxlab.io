---
layout: post
title: 'Douane: Status and roadmap'
date: '2014-01-13 12:20:15'
tags:
- my-developments
- douane
---

After many months without any news ([my latest article](/douane-mockup/) was written in February) about my project Douane (an interactive firewall for Linux), I’m posting this small article just to inform you about my progress.

## Progress

The most important progress I did is the stabilisation of the **Linux Kernel Module**.
Before it was not possible to use it out of a Virtual Box due to random freezes.

I have finally found and fixed the problem so that now I’m using it directly without Virtual Machine.

## Roadmap

The roadmap is now defined as the following:

1. Implement a D-Bus server with at least a signal to receive network activities
2. Create a new python project (as it looks like Python is the official language for Ubuntu :-P) in order to show the activities and to register rules
3. Implement the dialog box to ask to the user when a new application try to connect to the outside.

When those points will be implemented, I will release a first public version, and start to promote Douane.

I’m also thinking a lot about making a bit of money with that project.
Having Douane in the **[Ubuntu Software center](http://www.ubuntu.com/ubuntu/features/find-more-apps)** ([wikipedia link](http://en.wikipedia.org/wiki/Ubuntu_Software_Center)) would be very great!

Of course, the price will be very low, it’s just to have a first experience with making money with developed application.
In addition to this I still want to have the maximum of Douane **open source**.