---
layout: post
title: Cookbook `apt' not found at expected cache location (ChefDK::CachedCookbookNotFound)
date: '2019-08-12 14:23:35'
tags:
- chef
---

You run your usual `chef update <path>` and got that strange `ChefDK::CachedCookbookNotFound` error? Here is the solution!

So you run your `chef update` command, but got the following output:

```
$ chef update policies/k8smaster.rb
Error: Failed to update Policyfile lock
Reason: (ChefDK::CachedCookbookNotFound) Cookbook `apt' not found at expected cache location `apt-7.1.1-supermarket.chef.io' (full path: `/Users/zedtux/.chefdk/cache/cookbooks/apt-7.1.1-supermarket.chef.io')
```

While it's not really clear what happened, and how I endend up in this situation, but at least I can fix it quite easily (until what is in the lock file is not that important) by removing the lock file (or renaming it) and then run `chef install <path>` instead.

Then a diff of both files could help to understand what happened, but at least you're good to go again!