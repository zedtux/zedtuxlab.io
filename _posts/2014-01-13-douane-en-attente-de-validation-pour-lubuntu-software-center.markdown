---
layout: post
title: Douane en attente de validation pour l’Ubuntu Software Center!
date: '2014-01-13 13:02:26'
tags:
- mes-developpements
- douane
---

Ca y es! Je viens de soumettre la première version de Douane a l’équipe de vérification du Ubuntu Software Center.

La version publié est la version 0.8.1 et devrais être très prochainement disponible!
Comme vous l’aurez noté sur le site de l’application (http://douaneapp.com/), l’application est payant, mais le prix est très faible, et de plus, vous bénéficierez des mises a jour gratuitement bien entendu, ceux jusqu’à la version 2.

La version 2 est planifié que dans bien longtemps! Donc vous découvrirez de nouvelles fonctionnalités. Tout dépendra du succès de l’application.

Je suis ouvert a toutes propositions de fonctionnalités, et même de nouveau design de l’interface de configuration.
Donc n’hésitez pas.

Pour tout bugs ou propositions, merci de les adresser sur le compte [Github Douane](https://github.com/Douane/Douane).