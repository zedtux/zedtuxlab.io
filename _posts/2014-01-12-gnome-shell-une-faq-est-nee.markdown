---
layout: post
title: 'Gnome Shell: Une FAQ est néé !'
date: '2014-01-12 19:34:51'
tags:
- gnome
---

![gnome](/content/images/2017/08/gnome.png)

L’équipe de Gnome Shell vient d’annoncer la création d’une [FAQ](http://fr.wikipedia.org/wiki/Foire_aux_questions) qui se trouve à cette adresse: http://live.gnome.org/GnomeShell/FAQ

**Jeremy Perry** a énuméré une liste de questions, puis **William Jon MacCann** y a répondu afin de les découpées en 3 parties :

 - **Général**: Une description de se qu’est Gnome Shell
 - **Utilisation**: Comment l’utiliser, quoi faire en cas de pépins etc…
 - **Avancée**: Plus de détails sur les techo qu’utilise Gnome Shell, comment tester ou participer.
