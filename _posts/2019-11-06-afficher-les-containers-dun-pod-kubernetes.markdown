---
layout: post
title: Afficher les containers d'un Pod Kubernetes
date: '2019-11-06 12:53:37'
tags:
- kubernetes
---

Un Pod est le plus petit block dans Kubernetes, et il peut faire tourner un ou plusieurs containers.

Disons que vous avez un pod avec 2 containers, et que vous désirez accéder aux logs d'un de ces deux containers, il vous faudra le nom du container pour lequel vous désirez voir les logs.

Voici donc une command qui construit un tableau avec une premère colonne pour afficher le nom du pod, puis une deuxième colonne avec le nom des containers:

```
kubectl get pods mysql-0 -o='custom-columns=NAME:.metadata.name,CONTAINERS:.spec.containers[*].name'
NAME      CONTAINERS
mysql-0   mysql,xtrabackup
```

Maintenant il vous est possible d'accéder aux logs d'un container:

```
kubectl logs mysql-0 --container=xtrabackup
```

**À noter:** Dans le cas où vous désirez les logs de tous les containers, peu importe l'ordre, vous pouvez y accéder facilement:

```
kubectl logs mysql-0 --all-containers=true
```
