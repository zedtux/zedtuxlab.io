---
layout: post
title: Lynis – Default umask in /etc/* could be more strict like 027 [test:AUTH-9328]
date: '2014-01-12 20:48:59'
tags:
- linux
- server
- security
---

If Lynis list the following suggestion:

> Default umask in /etc/profile could be more strict like 027 [test:AUTH-9328]

You will fix it by editing the file `/etc/login.defs` and changing the following line **from 022 to 027**:

	UMASK           027

(Was at line 151 for me)