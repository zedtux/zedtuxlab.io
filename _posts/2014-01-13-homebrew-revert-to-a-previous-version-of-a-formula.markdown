---
layout: post
title: 'Homebrew: Revert to a previous version of a formula'
date: '2014-01-13 12:15:05'
tags:
- osx
- homebrew
---

After having upgraded my installed ImageMagick with homebrew, I was in trouble to install the gem rmagick…

The only way to not lost too much time was to revert to the previous version.

I wasn’t able to use the switch command as I had cleaned installed formula, so here is the solution (Thanks to [tschundeee](http://stackoverflow.com/users/306478/tschundeee) from [StackOverflow](http://stackoverflow.com/questions/3987683/homebrew-install-specific-version-of-formula#answer-9832084)):

## Simple Workflow

### Step 1

Navigate to your homebrew base directory (usually this is /usr/local) Example:

	cd /usr/local

### Step 2

Enter brew versions FORMULANAME (FORMULANAME is the formula you want to install).

You will then get something like:

 	
    1.0.1 git checkout 1234567 Library/Formula/FORMULANAME.rb
    1.0.0 git checkout 0123456 Library/Formula/FORMULANAME.rb ...

### Step 3

Choose the desired version and check it out via copy and paste of the desired version line (leave out the version number in the beginning).

Example for getting 1.0.0:

	git checkout 0123456 Library/Formula/FORMULANAME.rb

### Step 4

Install the formula:

	brew install FORMULANAME

Then you're done!