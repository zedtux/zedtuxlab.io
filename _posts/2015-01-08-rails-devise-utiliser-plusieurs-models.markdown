---
layout: post
title: Rails + Devise utiliser plusieurs models
date: '2015-01-08 19:58:57'
tags:
- rails
- devise
---

Habituellement lorsque vous travaillez avec Devise vous aurez des utilisateurs. Dans ce cas, Devise est très simple d'utilisation: 1 ligne de commande et vous êtes prêt.

Mais lorsque vous désirez travailler avec plusieurs models tell qu'un model `User` et un model `Admin` alors Devise se révèle plus compliqué.
Il y a des personnes qui préfèrerons ne pas créer les deux models et plutôt utiliser des techniques telle que les [Concern (en)](http://api.rubyonrails.org/classes/ActiveSupport/Concern.html) ou autres.

Personnellement je désire avoir 2 models.

### Il y a du nouveau

Depuis le 13 Aout 2014 un nouveau helper a été ajouté dans Devise:

    3.3.0 - 2014-08-13
     - enhancements 
     	...
        - Adds devise_group, a macro to define controller helpers for multiple mappings at once. (by @dropletzz)
        ...

[Dropletzz](https://github.com/dropletzz) a implémentés une méthode dite "helper" qui vous permet de définir un group de "mappings" qui va générer des méthodes comme expliqué [dans la documentation](http://www.rubydoc.info/github/plataformatec/devise/master/Devise/Controllers/Helpers/ClassMethods:devise_group).


### Comment utilisé devise_group

Dans mon cas j'en ai besoin partout dans mon application donc j'ai ajouté le code suivant dans mon `ApplicationController`:

```ruby
class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  devise_group :person, contains: [:user, :admin]
  before_action :authenticate_person! # Ensure someone is logged in
end
```

Maintenant je peux utiliser `current_person` partout dans mon application où le code est commun aux classes `User` et `Admin`.
Les méthodes `current_user` et `current_admin` sont toujours disponible ce qui est cool.

### Merci

Je tiens a remercier Dropletzz :) ainsi que vous qui lisez cet article.