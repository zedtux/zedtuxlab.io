---
layout: post
title: Continuous Integration en C++ avec Hudson
date: '2014-01-12 12:13:25'
tags:
- c-2
- continuous-integration
---

![Hudson](/content/images/2014/Jan/hudson.png)

## Continuous quoi ?

Ça permet d’améliorer la qualité d’un programme.
Les développeurs vont écrire des tests unitaires, qui seront exécutés après la compilation afin de prévoir des dysfonctionnements dût aux changements commités dans le repository (Subversion par exemple).

Voire aussi: [Wikipedia](http://fr.wikipedia.org/wiki/Int%C3%A9gration_continue)

## Qui est ce Hudson ?

[Hudson](http://hudson-ci.org/) est une application, écrite en Java, dans laquelle il sera possible de créer des jobs qui compileront un projet, et exécuteront les tests unitaires et plus encore, aux heures ou événements définis. (_Night Build_ par exemple).
Ensuite, un rapport sera généré, puis envoyé par différents moyens: E-mails, Trac, …

# Installation

Je vais vous décrire la procédure d’installation pour Ubuntu Server 8.10, mais ça devrait marcher, à peu de chose près partout.

Hudson, peux fonctionner de 2 manières :

 - Soit en le lançant depuis un terminal, avec java
 - Soit en utilisant un _Java Application Server_

Nous allons utiliser la dernière méthode, car elle vous permettra, par la suite, de déployer plusieurs applications Java, par une interface web.

Il existe plusieurs _Java Application Server_ dont [Glassfish](https://glassfish.java.net/) de _Sun_ (Ou Oracle maintenant :D), [Tomcat](http://tomcat.apache.org/) d’_Apache_, pour les plus connuts.


GlassFish est plus gros et plus complet que Tomcat, et contient bien plus de fonctionnalitées... Mais comme dans notre cas nous voulons seulement faire tourner une application java … Nous nous contenterons de __Tomcat__.


## Installation de Tomcat

Pour installer Tomcat c’est du gâteau, comme toujours avec Ubuntu:

    sudo apt-get install tomcat6 tomcat6-admin

Si vous avez des règles iptables, n’oubliez pas d’ouvrir le port 8080.

Pour pouvoir accéder à la zone d’administration de Tomcat, il va falloir un compte.

Pour ce faire, il suffit de modifier le fichier `/etc/tomcat6/tomcat-users.xml`, et d’ajouter ces lignes :

    <role rolename=”manager”/>
    <user username=”admin” password=”your_password” roles=”manager”/>

Ensuite, redémarrez Tomcat :

    sudo /etc/init.d/tomcat6 restart

Maintenant, Tomcat est fonctionnel à l’adresse http://adresse.domaine.org:8080/
Et la zone admim: http://adresse.domaine.org:8080/manager/html

## Installation de Hudson

"Installation" n’est pas le bon terme. Ici on va _déployer_ Hudson.
Donc, il vous suffit de télécharger le [WAR d’Hudson](http://eclipse.org/downloads/download.php?file=/hudson/war/hudson-3.1.1.war&r=1), puis, dans la zone d’administration, utiliser le formulaire, tout en bas, avec le bouton parcourir.

Le WAR sera uploadé, puis copier au bon endroit ( dossier webapp ), et sera accessible à l’adresse http://adresse.domaine.org:8080/hudson/manage

Par contre, à partir de là, vous aurez un soucis.
Le _security manager_ (activé par défaut) empêchera hudson de fonctionner. Il faut donc le désactiver.
(Je n’ai pas encore trouvé de solution autre).

Cette opération est très simple, il suffit de modifier le script de lancement de Tomcat :

    sudo nano /etc/init.d/tomcat6

Vous y trouverez une ligne `TOMCAT6_SECURITY=yes` qu’il faut changer en `TOMCAT6_SECURITY=no`.

Puis redémarrer Tomcat.
Maintenant, vous devriez arriver sur Hudson.

# Création d’un Job pour C++

Nous voici au cœur de l’article: Compiler un projet C++ avec Hudson.

Première chose, il vous faut un `Makefile` parfait !
Il faut une commande pour nettoyer le projet (chez moi je l’appelle make mrproper), et le `make` normal.
Bien entendu, vous pourrez en exécuter d’autre …

Voici le `Makefile` dont je me sers pour mes projets C++ :

<script src="https://gist.github.com/zedtux/8383803.js"></script>

Passons à la création de la tâche en elle même, dans Hudson.

## Création du job

Rendez-vous dans Nouvelle tache pour avoir ceci:

![Hudson ajouter un job](/content/images/2014/Jan/hudson_newjob.jpg)

Donc il suffit d’entrer un titre pour ce job (Modifiable par la suite), sélectionner le **build free-style**.

## Configuration du job

Dans l’écran suivant, nous allons définir le projet sur le repository SVN, puis les commandes à exécuter, et quand les exécuter.

![Hudson configuration du job](/content/images/2014/Jan/hudson_setupjob.jpg)

Hudson doit avoir les droits en lecture sur le repository.
Dans mon cas, comme je travaille en Open Source, je n’ai aucun soucis, mon repository est accessible en anonyme.
Si hudson doit utiliser un compte pour accéder à votre repository, il faut le renseigner dans le lien que vous trouverez dans la doc ( Utiliser le pour voire la documentation en face du nom du repository ).

Ensuite vient la partie pour définir quand lancer le job.

Ici j’ai configuré mon job pour compiler toutes les nuits ( @midnight ), et aussi quand je committe dans le repository SVN ( il vérifie toutes les heures, d’où le @hourly ).

Juste après ceci, vous devez configurer le build en mode Execute Shell :

![Hudson configuration du job - Execute Shell](/content/images/2014/Jan/hudson_buildmode.jpg)

Et c’est là que tout ce joue !
Hudson exécutera toutes les commandes qui seront inscrite dans le textarea qui est apparut !
C’est donc là que va intervenir notre joli Makefile !! :)

![Hudson configuration du job - script](/content/images/2014/Jan/hudson_buildconf.jpg)

Hudson va tout d’abord faire un __checkout__ du repository s’il ne l’a jamais fait, sinon un __update__.
Ce qui créera un dossier `trunk/` dans le _workspace_, si vous utilisez le standard.

Donc dans un premier temps, on se rend dans le dossier `trunk/` puis on appelle directement un `make mrproper` pour nettoyer le projet, et le recompiler à 0.
Si vous ne faites pas de nettoyage du projet, le build aura de grandes chances de péter … car les fichier _.o_ ne seront pas à jour !
Et au final, on relance la compilation !

C’est tout !! :)

Maintenant, il va falloir installer TOUTES les dépendances de votre projet pour que la compilation fonctionne !
Dans un premier temps, vous êtes sûre d’avoir besoin de `g++` par exemple.

Pas de panique, vous n’aller pas devoir deviner ce qui manque !

 - Soit vous les connaissez, et c’est bon
 - Soit vous ne les connaissez pas, alors il suffit de lancer le build, puis d’aller voire ce que la console à donné en sortie.

Pour voir ce qu’il s’est passé dans la console, il suffit d’aller dans le job, puis sélectionner le build ( le dernier en général ;) ), puis de cliquer sur _Console Output_, ou sortie console.

Voilà, vous êtes prêt à avoir vos propre nightly build pour vos projets C++, ou même Java.