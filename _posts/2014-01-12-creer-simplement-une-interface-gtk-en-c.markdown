---
layout: post
title: Créer simplement une interface GTK en C++
date: '2014-01-12 02:34:11'
tags:
- c-2
- gtk
---

# Introduction

Aujourd’hui, développer une application, inclut très souvent la création d’une interface graphique.
Sous Linux, LA librairie graphique utilisé sous GNOME est GTK.

L’utilisation de cette librairie peut-être très fastidieux en C, par exemple, car vous devez créer tout vos controls, les emboîter les uns les autres et ceux … sans voire le résultat. ( Vous le verrez uniquement a l’exécution. )
Je trouve, d’ailleurs, paradoxale de faire une interface graphique sans la dessiner … mais bon.

Nous allons donc voire comment créer visuellement, et facilement, une interface GTK, et l’implémenter dans un code C++.

# Feuille de route

Voici un schéma simple, qui représente la hiérarchie à utiliser:

![feuille de route](/content/images/2014/Jan/glade_gtkmm.png)

# Créer l’interface

Donc, dans un premier temps, il faut utiliser Glade pour créer votre interface graphique.
Prenons un exemple simple:

![exemple d'application](/content/images/2014/Jan/hello.png)

Je pense que vous avez compris ce que fera notre exemple ;-)
Pour ceux qui sont à l’Ouest:
L’utilisateur entrera son nom, cliquera sur Appliquer, et le label Hello ! changera pour Hello sonnom !

Donc, nous obtenons un fichier glade. Ici appelé hello.glade. ([Télécharger hello.glade](http://depot.zedroot.org/divers/hello.glade.tar.gz))

J’ai laisser expret les noms d’origine de tout les controls pour que ce soit plus simple à suivre dans le code.

# Implémenter l’interface dans le code

Nous voici dans la partie la plus dur… et heureusement … c’est pas dur ! :-D

Donc, vue que l’on bosse en C++, on va créer une objet de notre fenêtre et ses controls.
Pour ce faire, on va utiliser l’excellent [GTKmm](http://www.gtkmm.org/)!
C’est une interface à GTK pour C++.

Le projet Hello sera composé de 3 fichiers:

 - hello.h
 - hello.cpp
 - main.cpp

Les hello.* c’est l’objet de la fenêtre, et le main ne sert uniquement qu’a initialiser GTK, charger le fichier glade et demander à GTK de lancer notre fenêtre.
Pour que notre projet C++ puisse utiliser les Widgets GTK, nous allons utiliser les [Derived Widgets](http://www.gtkmm.org/docs/gtkmm-2.4/docs/tutorial/html/sec-libglademm-using-derived-widgets.html).

## main.cpp

Commençons par le fichier main.cpp.
Comme dis plus haut, on va initialiser GTK, charger le fichier glade, puis appeler l’objet Hello que l’on aura créé:

<script src="https://gist.github.com/zedtux/8380046.js"></script>

Nous voici, ici, avec un main qui va pointer sur le fichier Glade.
Il est temps de créer notre Widget dérivé du fichier glade pour enfin afficher notre interface ! ;)

## hello.h

Bon le fichier header est simple.
La seule complexité ici, mais une fois compris ca n’en ai plus une, c’est le constructeur.
En effet, nous allons créer une objet héritant de Gtk::Window, et il va falloir faire un lien avec le fichier glade.

<script src="https://gist.github.com/zedtux/8380052.js"></script>

## hello.cpp

Voici le fichier hello.cpp:

<script src="https://gist.github.com/zedtux/8380057.js"></script>

Ici, tout ce que l’on fais c’est prendre la référence du pointeur glade venant du main.cpp à la ligne 5, et l’on donne une référence sur notre classe ayant hérité de Gtk::Window.
A noter que les lignes 4 et 5 auraient put être écrite dans les acollades du constructeur … mais ca fat plus de boulot pour rien. ( Merci Rniamo ;-) )

## Passer l'objet GTK

Il ne reste plus qu’a passer notre objet à GTK pour qu’il dessine notre interface !
Retour dans le __main.cpp__:
__Ligne 45, au todo__:

<script src="https://gist.github.com/zedtux/8380063.js"></script>

# Compilation

Il ne reste plus qu’a compiler le tout, et le lancer.
Serte nous aurons qu’une fenêtre sans réactions… mais ca va venir ;-)

    g++ -o test pkg-config libglademm-2.4 --cflags --libs *.cpp
    ./test

# Bouton Quitter

Occupons nous de ce bouton pour fermer notre fenêtre.
Il va donc falloir lier le signale `clicked()` du bouton à une méthode de notre classe, qui appellera le `hide()` de `Gtk::Window`.

Donc, première étape, créer un objet de notre bouton Quitter et une methode qui sera appellé au signal `clicked()` dans le __hello.h__:

<script src="https://gist.github.com/zedtux/8380066.js"></script>

Et dans notre __hello.cpp__, ajoutons l’appelle au bouton, son lien vers la methode `on_button_quit()` et ajoutons cette dernière :

<script src="https://gist.github.com/zedtux/8380072.js"></script>

Après recompilation, et lancement, vous pouvez fermer la fenêtre avec le bouton.

Maintenant, pour le bouton appliquer, qui va changer la caption du label, c'est exactement le même procédé pour le bouton.
Seule la le contenue de la méthode qui sera appelé, changera ! :-)

# Le bouton appliquer

Bon donc, on redéclare une bouton Gtk, on pointe dans le fichier Glade sur "button2", et on fait un lien vers une méthode.
Pour avoir un pointeur sur le label, c’est le même code que pour le bouton, sans le lien et le if, et il faut créer un `Gtk::Lable` vers "label2".
Ha oui, et pour le `Gtk:Entry` (la boite de texte pour entrer le nom), c’est pareil que pour le label.

Pour l’exemple, je l’ai appelé, sans originalité `on_button_apply()`.
Aussi, pour avoir un code clair, j’ai créé une méthode `sayHello()` qui prend le nom de la personne en paramètre et change le label. Mais ca aurai tout aussi bien put être fais entièrement dans la méthode `on_button_apply()`.

<script src="https://gist.github.com/zedtux/8380077.js"></script>

Voila, quand vous lancer le projet, vous aller pouvoir afficher votre nom dans le label.

#Conclusion

Je sais que ca peux paraitre long au vue de l’article, mais bon… c’est la première fois pour moi ce genre de chose, et j’ai certainement pas fais comme il le fallait..
Mais si vous regardez le code source, vous verrez que c’est enfantin … surtout comparé à la méthode du “je code l’interface graphique au lieu de la dessiner !” xD
En tout, le projet fais 6.1Ko… Et il doit y avoir a peine 50 lignes.

Je pense donc, qu’il est clairement plus interessant de coder avec Glade, rien que pour la facilité de Glade à modifier l’interface.
Surtout que vous n’avez pas à recompiler le code, chose à faire avec l’autre méthode !

Voici le projet au complet pour vous aider: [helloproject.tar.gz](http://depot.zedroot.org/divers/helloproject.tar.gz)