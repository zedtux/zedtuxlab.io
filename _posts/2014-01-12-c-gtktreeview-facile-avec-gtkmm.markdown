---
layout: post
title: 'C++: Gtk::TreeView facile avec GTKmm'
date: '2014-01-12 19:01:49'
tags:
- c-2
- gtk
---

![Exmeple application GTK avec Gtk::TreeView](/content/images/2017/08/gtkmm_treeview_example.png)

Je vais essayer d’expliquer dans cette article comment utiliser les [Gtk::TreeView](http://library.gnome.org/devel/gtkmm/unstable/classGtk_1_1TreeView.html) avec [GTKmm](http://www.gtkmm.org/) de manière simple ! :-)
Dans un premier temps, nous allons créer une liste simple, puis nous ajouterons, comme dans la capture au dessus, une image.

Comme à mon habitude, nous allons utiliser [Glade](http://glade.gnome.org/) pour dessiner l’interface graphique et créer une classe dérivé de notre fichier glade, grâce à GTKmm.
([Voire mon article précédent](/créer-simplement-une-interface-gtk-en-c/) pour voire en détails les classes dérivé de fichiers glade)

# Créer le fichier glade

Cette étape est la plus simple.
Si pour vous Glade reste quelque chose de difficile ( utiliser les Vbox et Hbox pour aligner les controls ), je vous conseil d’abord de vous entrainer à dessiner des interfaces.
Il m’y a pas vraiment de tutoriels sur Glade.. il faut pratiquer !

Donc, dans une nouvelle fenêtre placez une Vertical Box avec 2 lignes : la première pour la Gtk::TreeView, et la deuxième ligne pour un bouton quitter.

Quand vous ajouterez la Gtk::TreeView, vous aurez ceci, mais ignorez et cliquez sur **Valider**:

![Créer un GtkTreeView](/content/images/2017/08/gtkmm_treeview_model.png)

Donc, une fois fini vous devriez avoir ceci :

![gtkmm_treeview_glade](/content/images/2017/08/gtkmm_treeview_glade.png)

Donc, en résumé, nous devons lier notre classe à la fenêtre appelé **wMain**, puis attacher un bouton à **bClose** et manipuler la Gtk::TreeView **tvList**.

Sauvegardez votre projet glade dans un nouveau dossier, et passons au code !! 8-)

# Coder notre première Gtk::TreeView

Aller mes petits, c’est l’heure de faire du C++ !!
(Ca vous manquais j’espère !! :-D)

Donc, vous avez créer un dossier avec votre fichier glade, nous allons y ajouter notre `main.cpp` ainsi que `wmain.cpp` et `wmain.h`.
Je ne ré-explique pas le fichier `main.cpp` ainsi que la structure de base de `wmain`. Voyez mon article précédent si vous avez un trou.

Bon, vous avez la fenêtre qui s’ouvre, ainsi qui se ferme avec le bouton fermer. Bien.
Nous allons paramétrer et remplir notre Gtk::TreeView !

La Gtk::TreeView fonctionne avec des [Gtk::TreeModel](http://library.gnome.org/devel/gtkmm/unstable/classGtk_1_1TreeModel.html) qui représente une ligne contenant les valeurs pour chaque lignes qui sont à stocker dans un [Gtk::TreeStore](http://library.gnome.org/devel/gtkmm/unstable/classGtk_1_1TreeStore.html).
Donc il nous faut définir un **Gtk::TreeModel** dans lequel nous définirons le typage de chaque colonnes et nous finirons par **attacher ce Gtk::TreeModel à un Gtk::TreeStore** qui lui même sera attaché à notre Gtk::TreeView.
Ce qui nous permettra d’ajouter des lignes.

## wmain.h avant

Je vais créer une classe qui va représenter une ligne de mon tableau, héritant de Gtk::TreeModel pour facilité l’insertion de lignes.
Jusque là, vous devriez avoir votre wmain.h comme ceci:

<script src="https://gist.github.com/zedtux/8388658/59fcc6880a1adff5ab9d1c7e1bce1c6e482df5e8.js"></script>

## wmain.h après

Ajoutons notre classe dans la classe, héritant de Gtk::TreeModel et même `Gtk::TreeModel::ColumnRecord` pour être précis :

<script src="https://gist.github.com/zedtux/8388658.js"></script>

Ha oui, j’ai aussi créé ma référence à **Gtk::TreeStore**.

# Lié les objets dans le constructeur

Maintenant, passons au fichier cpp.
Il nous faut attacher les objets entre eux, et remplir la Gtk::TreeView.

Donc dans le constructeur, ajoutez ceci:

<script src="https://gist.github.com/zedtux/8388707.js"></script>

**Explications:** Après avoir attaché notre objet `gtkTreeViewList` à la list appelé `tvList`, il suffit de créer un pointeur de `Gtk::TreeStore` en instanciant et en passant en paramètre, notre classe `ModelColumns`.
Une fois `Gtk::TreeStore` instancié, nous devons le lié à la `Gtk::TreeView` avec `set_model()`.
(Si vous vous rappelez de la boite de dialogue que vous aviez ignoré en ajoutant la Gtk::TreeView, c’est ce que cette ligne fait)

Les deux dernières lignes créer deux colonnes, et les lient avec notre classe `ModelColumns`.

# Ajouter une ligne au Gtk::ListView

Pour ajouter une ligne, il faut créer un pointeur de Gtk::TreeModel::Row, remplir chaque champs (donc columnId et columnName) et l’ajouter au `Gtk::TreeStore`.

<script src="https://gist.github.com/zedtux/8388766.js"></script>

La première ligne créer une ligne vide, attaché au `Gtk::TreeStore`.
Les deux suivante injecte les valeurs.

Rien de bien difficile !! ;)
Maintenant. vous devriez pouvoir créer un bouton Ajouter par exemple !

# Ajouter des sous lignes

Tient ! Profitons-en pour voire les lignes dites “enfant”.
En ajoutant une ligne enfant, vous aurez un ligne caché que vous pourrez afficher grâce au petit triangle devant la ligne parente.

Pour ajouter une ligne enfant à notre ligne fraichement créé :

<script src="https://gist.github.com/zedtux/8388795.js"></script>


# Ajouter une colonne avec une image

Dans la capture au tout début, vous avez put voire une icône au début.
Je vais vous expliquer comment l’ajouter.

Avant tout, peut-être des idées ont jaillit dans vos tête !
Comme par exemple, ajouter une `Gtk::ProgressBar` ou encore un `Gtk::Button`…
Attention ! Malheureusement, il y a certaines limites. (limite de GTKmm je suppose).

La `Gtk::ProgressBar`, pas de problème. Une classe existe bien pour l’ajouter, vous l’avez sûrement vue dans des exemples avant de lire cet article.
Mais le `Gtk::Button`, quand à lui, ne peux pas être ajouté comme le `Gtk::ProgressBar`. Il n’existe pas de classe approprié. (il est possible d’en ajouter … mais faut être expert en GTK! ;-))

Tout ca pour vous faire comprendre que vous ne pouvez pas tout faire non plus.

Revenons à notre icône.
Nous allons donc ajouter une colonne qui permettra d’afficher notre image.
Pour être précis, la colonne va contenir un pointeur d’un `Gdk::Pixbuf`.

Ajoutons la colonne à la classe ModelColumns:

<script src="https://gist.github.com/zedtux/8388830.js"></script>

Puis, il faut créer la colonne dans le `Gtk::TreeView` et la lié à notre nouvelle colonne de notre classe **ModelColumns**.

<script src="https://gist.github.com/zedtux/8388842.js"></script>

## Créer une ligne avec une image

Maintenant que notre colonne existe, utilisons la.

Je vous avez dis que cette colonne stockerai un pointeur de `Gdk::Pixbuf`, alors allons y:

<script src="https://gist.github.com/zedtux/8388864.js"></script>

Les deux ’50′ sont là pour redimensionner l’image, et le `true` à la fin permet de garder les proportion de l’image.

## Vider la liste

Pour vider la liste de ses lignes, il suffit d’appeler la method `clear()` de notre `Gtk::TreeStore` !
Imaginons un bouton Vider :

<script src="https://gist.github.com/zedtux/8388882.js"></script>

## Supprimer une seule ligne

Pour supprimer une ligne sélectionné il faut, dans un premier temps, chercher la ligne sélectionné dans le `Gtk::TreeModel` avec `get_selected()`, puis demander, comme pour vider la liste, au `Gtk::TreeStore` de supprimer la ligne sélectionné avec la méthode `erase()`.

Imaginons un bouton Supprimer :

<script src="https://gist.github.com/zedtux/8388895.js"></script>

Donc le if créer un itérateur de la ligne retourné par `get_selected()`. Donc avec cet itérateur nous avons une référence à la ligne séléctionné.
Il suffit d’appeler `erase()` en passant notre itération en paramètre, et hop ! La ligne disparait !

# Conclusion

Le C++ c’est pas difficile, grâce à ces jolies bibliothèques !
Maintenant j’ai hâte de voire la version 2.8 de gtkmm… Y aura des controls GTK en plus qui claque bien !! :-)