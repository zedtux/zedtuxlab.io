---
layout: post
title: 'Metal band : Deficiency'
date: '2014-09-18 12:02:53'
---

![deficiency_logo-1](/content/images/2017/08/deficiency_logo-1.png)

The last week-end I was in an event including a concert of metal music. I went there without big expectation but I have been really really well surprised by the band !

They succeed in giving good time to people who aren't listening to metal at all and also to people, like me, who listen metal everydays.

Deficiency is a French band composed of 4 members: A lead guitar, rythmic guitar, bass and drum. They all are nice and cool and it was with great pleasure that I brought their albums and T-shirt.

![deficiency_band-1](/content/images/2017/08/deficiency_band-1.jpg)

To keep it simple, just try it :-)

First of all, I suggest you to have a look to the video I did during the concert, recording by chance one of the best moment of the concert, which is on Youtube: https://www.youtube.com/watch?v=eQtkqaL8EzE.

Then you can listen their albums on Spotify by clicking [this link](http://open.spotify.com/artist/2U3uwxpSBsfzUwwCpeZ2T9).

You can finaly buy they're goodies (Albums and T-Shits) on [the Deficiency official website](http://www.deficiency.fr/)!