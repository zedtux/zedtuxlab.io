---
layout: post
title: 'Lynis: Suggestion: Harden the system by removing unneeded compilers. This
  can decrease the chance of customized trojans, ... [test:HRDN-7220]'
date: '2014-01-17 18:30:24'
tags:
- server
- security
---

In the case Lynis list the following suggestion:

>  Suggestion: Harden the system by removing unneeded compilers. This can decrease the chance of customized trojans, backdoors and rootkits to be compiled and installed [test:HRDN-7220]

Then you have to remove the compilers.
To do so do the following:

    sudo apt-get remove binutils && sudo apt-get autoremove && sudo apt-get clean