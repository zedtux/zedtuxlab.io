---
layout: post
title: A simple coming soon page in a minute
date: '2017-01-21 08:56:00'
tags:
- docker
---

You just got an idea, you bought the domain name, but you don't want the default server provider page and you also don't want to spend time on a "coming soon" page.

That's what happened to me a month ago and was like "Is there any Docker image running a "coming soon" page I could use?".
Unfortunately there wasn't.

So, it was the time to make it! :)

### A dockerised "coming soon" page

So I searched and found [this free and sexy "coming soon" page](http://www.creative-tim.com/product/coming-sssoon-page) made by [Creative Tim](http://www.creative-tim.com/) and dockerised it so that now you can run your own one in a minute.


###### How to set the page title and product name?

You can configure this using some environment variables like in the example of the screenshot on the top of this article:

```
TITLE="Awesome App" # Set the page title in the <head/> tag
PRODUCT_NAME="My Awesome App" # Set the <h1/> value
```

Now using Docker:

```
docker run --name coming-soon -d -p 80:80 -e TITLE="Awesome App" -e PRODUCT_NAME="My Awesome App" zedtux/docker-coming-soon
```

Open your web browser and got to http://localhost/ and you're done.

###### Now what about those Facebook, Twitter and Email buttons?

Finally you don't want to lose your visitors so let's add some buttons on the top right of the "coming soon" page:

```
FACEBOOK_URL="https://www.facebook.com/me" # Show the Facebook button and set its URL to https://www.facebook.com/me
TWITTER_URL="https://www.twitter.com/me"
```

And so on.

__You can find all the available variables in [the Github Docker image repository](https://github.com/zedtux/docker-coming-soon) [README.md](https://github.com/zedtux/docker-coming-soon/blob/master/README.md) file.__

### With Docker Cloud

If you're using Docker Cloud like me, then here is how to set it up in a Stack:

<script src="https://gist.github.com/zedtux/05b9db7306a533b5e80f8e6141b46975.js"></script>