---
layout: post
title: Try GNOME Software 3.12
date: '2014-03-30 09:30:54'
tags:
- gnome
---

As I want to distribute my current project [Douane](http://douaneapp.com/), I'm looking for possible Linux app stores.

Ubuntu has the well known Ubuntu Software Center but the GNOME team is working on a GNOME app store which is named **Software**.
(By the way, I like the simplicity of the application names: Music, Documents, Maps, ...)

I was wondering about the current status of this project, espacially after having looked at [the project offical page](https://wiki.gnome.org/Apps/Software) and found [this article](http://blogs.gnome.org/hughsie/2014/03/24/gnome-software-3-12-0-released/).

## Compilation

In order to compile Software, you need to download a tar ball from https://download.gnome.org/sources/gnome-software/.

I have done the compilation of the 3.12 version as it is the last version as of today.

### Dependencies

Of course you need some dependencies. I'm providing you an `apt-get` command line with all the required packages that was missing on my machine:

    sudo apt-get install gnome-common libpackagekit-glib2-dev libsoup2.4-dev gsettings-desktop-schemas-dev libgnome-desktop-3-dev docbook-xsl


### Compilation & installation

Last step is the compilation and installation. In the extracted archive folder:

    ./autogen.sh
    make
    sudo make install

Now you only need to open the Activities area and type 'Software' in the search and you should see the Software icon.

![Screenshot_from_2014_03_30_11_30_04](/content/images/2017/08/Screenshot_from_2014_03_30_11_30_04.png)