---
layout: post
title: Démarrer une application Rails de zéro avec Docker
date: '2018-03-22 13:52:41'
---

Cet article décrit comment créer une nouvelle application de zéro, en utilisant Docker, pas à pas.

## Image de base

Première étape, l'image de départ que vous allez utiliser pour démarrer Docker.

Dans notre cas, ce sera Ruby, dernière version, et l'image la plus petite possible.

Rendez-vous sur https://hub.docker.com/\_/ruby/ et dans la Description trouvez la version qui vous convient le mieux.

Dans mon cas, la version courant de Ruby est la 2.5.0, et je vais utiliser l'image `-slim`.

## Démarrage du container

L'initialisation de notre projet Rails sera faite à l'interieur du container (tous les fichiers et paquets qui aurons permits la création du projet ne nous serons plus utile, puisque le projet sera, encore par la suite, géré dans Docker avec [Docker Compose](https://docs.docker.com/compose/).

Démarrons le container:

```
docker run --rm -it -v "$PWD":/devs -w /devs ruby:2.5.0-slim sh
```

`docker run` permet de créer un container à partir d'une image, `--rm` effacera le container quand nous l'arrêterons (donc quand nous quitterons le container), `-it` pour `--interactive` et `--tty` ce qui permet d'agire dans le container (dans notre cas, pouvoir executer des commandes dans `sh`), `-v "$PWD":/devs` pour monter le dossier en cours, à la racine du container dans un dossier `/devs`, `-w /devs` pour être immédiatement dans le dossier `/devsa`, puis le nom de l'image Docker de Ruby, et finalement la commande `sh`.

Résultat, nous avons un terminal où Ruby y est déjà installé.

```
# ruby --version
ruby 2.5.0p0 (2017-12-25 revision 61468) [x86_64-linux-musl]
```

## Installer les dépendences

### Système
Pour installer la dernière version de Rails (5.1.5 au moment où j'écris cet article), il faut intaller le paquet `build-essential` qui contient les outils de compilaton que [la gem Nokogiri](https://rubygems.org/gems/nokogiri) requis, mais aussi le paquet libsqlite3-dev pour l'installation de la gem sqlite3 et enfin il nous faudra `curl` pour installer Node.js:

```
apt-get update && apt-get install -y build-essential curl libsqlite3-dev
```

### Node.js

```
curl -sL https://deb.nodesource.com/setup_9.x | bash -
```

Puis

```
apt-get install -y nodejs
```

### Yarn

Il faut ajouter leur clé GPG et leur dépôt dans APT:
```
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
```

Puis installer Yarn:

```
apt-get update && apt-get install -y yarn
```


## Installer Rails

Maintenant il ne reste plus qu'à installer Rails dans le container:

```
gem install rails
```

Et voilà!

```
# rails --version
Rails 5.1.5
```

## Initialiser le projet

Là rien de magique ou de nouveau, il suffit de lire la documentation de Rails (ou `rails new --help`) pour choisir ce que vous désirez.

Pour moi, je l'ai fait avec cette commande:

```
rails new --database=sqlite3 --api --webpack=react rails5-react
```

Votre projet est généré, il vous suffit de quitter le container, et voilà.

J'ai posté le projet sur Github ici: https://github.com/zedtux/Rails5-react.