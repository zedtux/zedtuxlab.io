---
layout: post
title: Paquet Ubuntu/Debian pour CRRCSim 32/64bits
date: '2014-01-12 20:24:24'
tags:
- packaging
---

![crrcsim_04a](/content/images/2017/08/crrcsim_04a.jpg)

Voilà, je viens de faire un dépôt PPA pour CRRCSim, un simulateur de modèles réduits, afin d’y héberger un paquet pour Ubuntu lucid et Maverick !

Vous pourrez le retrouver dans ma page [Mes Paquets Ubuntu/Debian](http://blog.zedroot.org/mes-paquet-ubuntudebian/).