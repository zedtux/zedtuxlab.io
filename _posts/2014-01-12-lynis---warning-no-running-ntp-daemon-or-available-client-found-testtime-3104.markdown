---
layout: post
title: 'Lynis – Warning: No running NTP daemon or available client found [test:TIME-3104]'
date: '2014-01-12 20:47:29'
tags:
- linux
- server
- security
---

If Lynis list the following warning:

> Warning: No running NTP daemon or available client found [test:TIME-3104]

You will fix it by installing the ntp package:

	sudo apt-get install ntp