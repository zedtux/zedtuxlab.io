---
layout: post
title: Taille par defaut de gnome-terminal
date: '2014-01-12 02:00:51'
tags:
- linux
- gnome
---

J’en ai marre de ce terminal qui s’ouvre en 80 colonnes par 24 lignes …

Un petit tour dans les préférences …. mais rien pour changer ca !
Grrr…. Mais … effet … c’est normal… gnome-terminal n’est rien d’autre qu’un émulateur d’xterm. Il faut donc changer les paramètres dans xterm.

J’ai trouvé [la solution sur un forum](http://forums.fedoraforum.org/showthread.php?t=45607), posté par __dbnichol__ :

    sudo nano /usr/share/vte/termcap/xterm

Dans ce fichier contient des tas de lignes. Nous, il nous faut juste modifier la ligne

    :co#80:it#8:li#24:\

par cette ligne, par exemple, pour garder la même hauteur, mais avoir 120 colonnes au lieu de 80:

    :co#120:it#8:li#24:\

Fermez tout les gnome-terminal, et ouvrez en un, et ……… TADA !!! :-) 