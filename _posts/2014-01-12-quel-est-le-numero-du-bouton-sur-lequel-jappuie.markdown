---
layout: post
title: Quel est le numero du bouton sur lequel j’appuie !?
date: '2014-01-12 13:04:22'
tags:
- linux
---

Quand il s’agit de configurer un programme, du moins sous Linux, pour définir un ordre de touches, par exemple, sur lesquels appuyer pour activer quelque chose, la liste des boutons est afficher comme ceci:

 - Bouton 1
 - Bouton 2
 - Bouton 3
 - Bouton 4
 - …

Du coup … savoir quel bouton est le bouton 4… c’est pas simple..

Soit vous tatonez … soit vous demandez au système ! :)

Ouvrez un terminal, et tapez-y :

    xev | grep button

Ensuite, appuyez sur le bouton souhaitez, et hop ! Son nom apparaîtra ! ;)