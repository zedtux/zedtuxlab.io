---
layout: post
title: 'Linux On Mac: Replacement for Spotlight'
date: '2015-02-28 18:42:33'
tags:
- linux-on-mac
---

Since OS X Yosemite, you can start any application, look for anything from Spotlight just by pressing `CMD + Space`.
This is really handy and guess what !? GNOME 3 can do the same !

GNOME 3 has implemented the Activities feature which is a mix of Mission control, Launchpad and Spotlight (for people coming from OS X). You can change the shortcut to access it in order to use `CMD + Space` so that you will continue to use the same shortcut to do the same action.

Open the User & system settings and click the Keyboard

![user-system-settings](/content/images/2017/08/user-system-settings.png)

Then change the following element with the `CMD + Space` accelerator

![system-settings_keyboard_system](/content/images/2017/08/system-settings_keyboard_system.png)
