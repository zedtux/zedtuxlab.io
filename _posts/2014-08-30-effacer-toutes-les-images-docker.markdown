---
layout: post
title: Effacer toutes les images Docker
date: '2014-08-30 17:31:02'
tags:
- docker
---

Parfois, après avoir fais plein d'essaies vous vous retrouvez avec un tas d'images inutile prennant de la place sur votre disque.

Pour faire un nettoyage de docker exécutez ces deux commandes:

    docker rm $(docker ps -a -q)
    docker rmi $(docker images -q)

Une fois fini Docker est propre comme si de rien n'était :)

Source: http://techoverflow.net/blog/2013/10/22/docker-remove-all-images-and-containers/