---
layout: post
title: Nice looking Docker contains management platform
date: '2015-05-29 17:48:02'
tags:
- docker
---

I have just discovered [LastBackend](https://lastbackend.com/) which is a nice platform to manage your [Docker](https://www.docker.com/) containers with a nice UI and drag & drop principle.

![CAbELiJWMAAkpKZ](/content/images/2017/08/CAbELiJWMAAkpKZ.png)

Have a look to the following video and try it !

<iframe width="560" height="315" src="https://www.youtube.com/embed/bO0sGz8qN7I" frameborder="0" allowfullscreen></iframe>
