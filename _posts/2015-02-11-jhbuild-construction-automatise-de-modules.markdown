---
layout: post
title: 'JHBuild : Construction automatisé de modules'
date: '2015-02-11 07:52:02'
tags:
- gnome
---

[JHBuild](https://wiki.gnome.org/action/show/Projects/Jhbuild) est un outil pour automatiser la compilation de modules.

Vous pouvez l'utiliser pour compiler un peu tout et n'importe quoi mais ici il est surtout utilisé pour compiler [GNOME](https://www.gnome.org/) et ses applications.

Tout son interêt prend son sense lorsque vous désirer développer une application GNOME et la tester dans la dernière version des bibliothèques ou lorsque vous désirez tester une application en cours de développement, ce qui est mon cas en ce moment.

## Un système d'isolation

JHBuild créer un environnement séparé de votre système principale ce qui signifie vous ne casserez pas votre machine.

## Un exemple

Ayant participé au [financement de GNOME Builder](https://www.indiegogo.com/projects/builder-an-ide-of-our-gnome), j'aimerai l'essayer même si sont développement n'est pas encore fini, voir l'évolution etc ...

### Les dépendances

Comme je disais plus haut, l'interêt de JHBuild est d'automatiser la compilation de modules et donc des dépendances.

JHBuild facilite donc grandement la compilation des dépendances et du projet cible.

Voici la commande pour vérifier et installer les dépendances du projet qui vous intéresse, [gnome-builder](https://wiki.gnome.org/Apps/Builder) dans notre cas:

    $ jhbuild sysdeps --install gnome-builder

Vous aurez des dizaine de bibliothèques qui vont être installé (J'ai vue au minimum 3 GB installé sur ma machine).

#### En cas de pépin ?

Il se peut que jhbuild ne trouve pas le paquet qui permettra de satisfaire toutes les dépendances. Dans ce cas vous obtiendrez ce genre de message:

    Required packages:
     Packages too old:
     (none)
    No match with system package
     soundtouch (soundtouch-1.4.pc, required=0)
     libicu (icu-i18n.pc, required=4)
     libunistring
     yajl
     device-mapper
    ...

La première chose que vous devez garder à l'esprit c'est que vous devez installer les paquets de développement. Il sont nommé généralement avec le suffix `-dev`.

Par exemple la bibliothèque nommé **libunistring** sera satisfaite en installant le paquet `libunistring-dev`.

Pour les autres cas comme **libicu**, jhbuild vous indique le fichier qu'il recherche (ici `icu-i18n.pc`). Encore une fois, une solution simple existe pour trouver le paquet qui va installer le fichier et donc satisfaire la dépendance qui s'appelle [apt-file](https://packages.debian.org/fr/sid/apt-file).

Pour installer `apt-file`: 

    $ sudo apt-get install apt-file
    $ sudo apt-file update

Ceci va installer `apt-file` puis mettre à jour son dictionnaire afin que vous puissiez chercher dedans.

Maintenant pour rechercher le paquet qui satisfera la dépendance `libicu`:

    $ sudo apt-file search "icu-i18n.pc"
    libicu-dev: /usr/lib/x86_64-linux-gnu/pkgconfig/icu-i18n.pc

Donc ici en installant le paquet **libicu-dev** vous résolvez cette dépendance.

## Compiler GNOME Builder

Maintenant que les dépendances sont installés, il ne reste plus qu'à compiler GNOME Builder:

    $ jhbuild -m gnome-world build gnome-builder

Traditionnellement il suffit de lancer `jhbuild build` puis le nom de l'application mais dans ce cas `gnome-world` est requis pour la compilation de GNOME Builder.

## Lancer l'application

Finalement pour lancer l'application, puisqu'elle se trouve dans un environnement isolé, vous ne trouverez pas l'application avec vos autres applications.

Pour lancer l'application, exécutez la commande suivante:

    $ jhbuild run gnome-builder

## Conclusion

JHBuild est un outil très puissant qui vous permet de gagner un temps monstre comme dans le cas de GNOME où vous devez compiler 45 dépendances avant de pouvoir lancer la compilation de l'application finale. 