---
layout: post
title: Créer une nouvelle application Rails de zero avec Docker
date: '2017-02-18 11:39:46'
tags:
- docker
- rails
---

Dans l'optique d'utiliser Docker pour tout faire (ne pas installer quoi que ce soit sur votre machine), voici un petit article pour vous expliquer comment partir d'un Docker vide, et créer votre application Ruby On Rails de zero (À noter que bien entendu ce principe fonctionnera avec n'importe quelle technologie, pas que Ruby On Rails).

### Étape 1: Récupérer une image de Ruby

Le point de départ  est ici Ruby. La première étape consiste donc à aller rechercher votre image Ruby sur https://hub.docker.com/.

Actuellement, la version la plus récente de Ruby est la 2.4.0 et il existe une image avec le tag 2.4.0-slim que nous allons utiliser (`-slim` signifie que tout ce qui n'est pas necessaire à Ruby a été retiré afin de réduire au maximum la taille de l'image finale).

Voici donc la première commande à lancer:

```
docker pull ruby:2.4.0-slim
```

Une fois le téléchargement fini, vous pourrez utiliser la commande `docker images` afin de voir l'image, sa taille, etc ...

### Étape 2: Installer Rails

Maintenant que nous avons une image Ruby, il faut la lancer et y installer Rails dedans.

Pour se faire, nous allons créer un container, y monter le dossier où nous créerons notre application (afin de percister les fichiers créés) et installer les dépendences afin que Rails s'installe correctement.

Voici la commande à lancer **depuis le dossier où vous stockez vos projets**:

```
docker run --rm \
           -it \
           --volume "$PWD:/devs" \
           --workdir=/devs \
           ruby:2.4.0-slim bash
```

Avec `--volume` nous mountons le dossier courant afin que les fichiers créés dans le containers soient enregistré sur votre disque dur, `--workdir` permet de démarrer notre commande `bash` directement dans le bon dossier.

Avant d'installer Rails, il nous faut installer les outils de compilation. L'image de Ruby est basé sur Debian, il nous faut donc utiliser `apt`:

```
apt-get update && apt-get install -y build-essential
```

Maintenant nous pouvons procéder à l'installation de Rails:

```
gem install rails
```

Pour finir, vérifions que rails est bien installé:

```
rails --version
```
_Si tous s'est bien passé, vous verrez quelle version est installée._

### Étape 3: Créer le projet Rails

Il ne vous reste plus qu'à créer le projet:

```
rails new mon_nom_de_projet
```

Voilà.

_PS: Afin d'éviter les soucis de permissions (les fichiers sont créés en tant que `root`), avant de quitter le container n'oubliez pas de lancer un petit `chown 1000:1000 -R mon_nom_de_projet/`_