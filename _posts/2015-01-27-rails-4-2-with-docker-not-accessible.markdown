---
layout: post
title: Rails 4.2 with Docker not accessible
date: '2015-01-27 21:45:32'
tags:
- docker
- rails
---

In the case you're a Rails developer working with Docker, migrating your application to Rails 4.2 introduced the issue that when starting the application, you cannot access it anymore.

Having a closer look to the logs shows us why:

    => Booting Puma
    => Rails 4.2.0 application starting in development on http://localhost:3000
    => Run `rails server -h` for more startup options
    => Ctrl-C to shutdown server
    Puma 2.11.0 starting...
    * Min threads: 0, max threads: 16
    * Environment: development
    * Listening on tcp://localhost:3000

As you can see the Rails application is listening to **localhost** instead of **0.0.0.0**.

The Rack default option of the binding parameter has been updated as described in this [Github issue](https://github.com/rails/rails/issues/16578).

## Fixing the issue with Fig

With Fig, updating the `fig.yml` file solves the issue:

    web:
      build: .
      command: bundle exec bin/rails server -b 0.0.0.0
      ...

Adding the `-b 0.0.0.0` fixes the issue.