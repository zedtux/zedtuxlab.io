---
layout: post
title: Attention, le loup rode !
date: '2014-01-12 18:14:26'
tags:
- information
- open-source
---

![loup_0020_assi_0020_peluche_0020_nici](/content/images/2017/08/loup_0020_assi_0020_peluche_0020_nici.gif)

Ce n’est pas mon genre de faire de la politique sur mon blog, mais là, j’ai étais vraiment surpris de ce que j’ai découvert ce matin grâce à l’April (http://www.april.org/) dont je suis membre depuis bientôt 1 an.

L’éditeur du système d’exploitation propriétaire vient de mettre une peau de mouton sur son dos de loup !

En effet, cette peau de mouton s’appelle CodePlex, et est une société de conception de logiciels libres, financé par le géant.

Le but de cette société est très clair: Injecter du code à licence soit disant Open Source, afin de mettre la main sur le code libre de GNU/Linux, pour, par la suite, détruire le travail de milliard d’être humain, et leur liberté !

Il faut se méfier comme de la peste du geant, car ce n’est nullement pas dans son interêt de nous aider, puisque son but premier, pour nous, est de tuer GNU/Linux!

J’attends les paroles de [notre Gourou](http://fr.wikipedia.org/wiki/Richard_Stallman), avec l’espoir que nous allons tous l’entendre, pas comme pour Mono !