---
layout: post
title: 'Sudo: Annuler les privilèges root'
date: '2014-01-13 12:53:07'
tags:
- linux
- command-line
---

Pour exécuter une commande en tant que root sans se connecter comme root il suffit de faire:
	
	sudo ma_commande

Le mot de passe root vous sera demandé, puis la commande sera exécuté comme root.

Mais maintenant que vous avez exécuté cette commande en tant que root, toutes les prochaine commandes pourront être exécuté comme root sans redemandé votre mot de passe (dépendant de votre configuration bien sur).

Alors comment annuler, avant la fin du temps autorisé, que les commandes lancé avec sudo ne demande plus le mot de passe ?

C’est très simple, il suffit de lancer ce qui suit:
	
	sudo -k

 