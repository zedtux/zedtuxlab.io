---
layout: post
title: 'MySQL: Maintenance automatique avec log'
date: '2014-01-12 11:47:43'
tags:
- maintenance
- database
- mysql
---

Je vient de constater dans mes logs que MySQL râle sur quelques tables…
C’est plus exactement `mysqlcheck` qui est à l’origine de ces messages.

J’ai donc trouvé comment corriger tout ca en ligne de commande, et comme je n’aime pas faire 2 fois la même chose ont va laisser notre serveur se débrouiller seul ! :-)
(C’est un grand garçon maintenant !! xD)

Donc, je vais utiliser [cron](http://fr.wikipedia.org/wiki/Cron).

Disons que nous l’exécuterons 1 fois tout les jours, à 1h du matin. Ca sera suffisant.

Je vais donc demander à `mysqlcheck` de vérifier toutes les tables (A), de les analyser (a), de les optimiser (o) et surtout de les réparer automatiquement (–auto-repair).

Donc voici la commande :

    mysqlcheck -Aao –auto-repair -u root -p[password]

La commande retourne des lignes avec __OK__ si il y a eut des changements, et sinon

 > Table is already up to date.

Sachant ceci, il reste plus qu’a utiliser `grep` ! ;-)

## Création du Job Cron !

Rien de bien compliqué !
On ouvre le registre des tâches:

    crontab -e

Là on ajoute cette ligne :

    0 1 * * * mysqlcheck -Aao –auto-repair -u root -p[password] | grep OK > /var/log/mysqlcheck.log

Et maintenant, tous les jours, à 1h du matin, il exécutera ma commande, et écrira dans le fichier de log les lignes qui ressortent en OK ! :-)