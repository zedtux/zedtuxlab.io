---
layout: post
title: 'kubectl : Use multiple account accesses'
date: '2019-01-18 10:45:55'
tags:
- docker
- kubernetes
---

Kubernetes has released [RBAC](https://kubernetes.io/docs/reference/access-authn-authz/rbac/) (Role-based access control) which allows you to manage and restrict user accesses in the cluster.

I decided to create a [ServiceAccount](https://kubernetes.io/docs/tasks/configure-pod-container/configure-service-account/) in order to limite the access to the cluster when running the tests in the Gitlab CI pipelines.

A ServiceAccount `ci` restricted to a `ci` [Namespace](https://kubernetes.io/docs/concepts/overview/working-with-objects/namespaces/) with a limited list of resources and actions.

To avoid having to wait on a build in order to try running a `kubectl` command, I configured it in order to get access to the `ci` ServiceAccount from my local machine.

As it requires some steps, and isn't really easy to remeber, I decided to write this blog post as a kind of reminder.

## Add a context to kubectl

#### Create the gitlab-ci cluster

First step is to create a cluster and pass the cluster URL :

```
$ kubectl config set-cluster gitlab-ci --server=$KUBERNETES_URL
Cluster "gitlab-ci" set.
```

If you have the cluster CA as a file locally, you can pass it to the `--certificate-authority` flag, but in my case I don't, so I will reuse the same trick as the one I described in my previous post [kubectl : x509: certificate signed by unknown authority](https://blog.zedroot.org/2019/01/18/kubectl/) and pass the base64 string directly :

```
$ kubectl config set clusters.gitlab-ci.certificate-authority-data $KUBERNETES_CA --set-raw-bytes=false
Property "clusters.gitlab-ci.certificate-authority-data" set.
```

Now let's add the user :

```
$ kubectl config set-credentials gitlab-ci --token=$KUBERNETES_TOKEN
User "gitlab-ci" set.
```

And finally let's connect all this together as a `ci` context :

```
$ kubectl config set-context ci --cluster=gitlab-ci --user=gitlab-ci --namespace=ci
Context "ci" created.
```

Last but not least let's check the config :

```
$ kubectl config view
clusters:
- cluster:
    certificate-authority-data: DATA+OMITTED
    server: https://<UUID>.k8s.ondigitalocean.com
  name: do-fra1-k8s-gitlab
- cluster:
    certificate-authority-data: DATA+OMITTED
    server: https://<UUID>.k8s.ondigitalocean.com
  name: gitlab-ci
contexts:
- context:
    cluster: gitlab-ci
    user: gitlab-ci
  name: ci
- context:
    cluster: do-fra1-k8s-gitlab
    user: do-fra1-k8s-gitlab-admin
  name: do-fra1-k8s-gitlab
current-context: do-fra1-k8s-gitlab
kind: Config
preferences: {}
users:
- name: do-fra1-k8s-gitlab-admin
  user:
    client-certificate-data: REDACTED
    client-key-data: REDACTED
- name: gitlab-ci
  user:
    token: REDACTED
```

## Use the added context

Now to switch to the `ci` context you just have to :

```
$ kubectl config use-context ci
Switched to context "ci".
```

Now you are logged in as the `ci` ServiceAccount and your requests will be executed in the `ci` Namespace by default (you can override it with the `--namespace` flag).

## Delete the context

In the case you'd like to delete the added ci context :

```
$ kubectl config delete-context ci
deleted context ci from /Users/zedtux/.kube/config
```
