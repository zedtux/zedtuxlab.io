---
layout: post
title: 'Linux On Mac: Fix ugly fonts'
featured: true
date: '2015-11-05 18:19:25'
tags:
- linux-on-mac
---

Another issue in Debian is the font rendering.

To solve this, you need to perform few steps:

#### Application fonts

First thing, changing the fonts of the applications, if you'd like to. Thanks to the Tweak tool, it's a super easy task.

Open the **Tweak Tool** application and select the **Fonts** tab:

![Screenshot-from-2015-11-05-19-09-50](/content/images/2017/08/Screenshot-from-2015-11-05-19-09-50.png)

As you can see, I've used the Apple San Francisco font for the *Interface* and *Documents*.

##### Iceweasel fonts rendering

A very boring example talks more than words:

![Screenshot-from-2015-11-05-19-15-07](/content/images/2017/08/Screenshot-from-2015-11-05-19-15-07.png)

The rendering of the text is really ugly. To solve this, first execute the following in a terminal:

```
$ sudo dpkg-reconfigure fontconfig-config
```

1. Font tuning method for screen (system default): Autohinter
2. Enable subpixel rendering for screen: Automatic
3. Enable bitmapped fonts by default?: Yes

Then edit/create the file `~/.fonts.conf` and set it as the following:

```
<?xml version="1.0"?> 
<!DOCTYPE fontconfig SYSTEM "fonts.dtd">

<fontconfig>

  <!-- Helvetica is a non true type font, and will look bad. This
  replaces it with whatever is the default sans-serif font -->

  <match target="pattern" name="family" >
    <test name="family" qual="any" >
      <string>Helvetica</string>
    </test>
    <edit mode="assign" name="family" >
      <string>sans-serif</string>
    </edit>
  </match>
  <dir>~/.fonts</dir>

</fontconfig>
```

Source: https://wiki.debian.org/Fonts/FAQ

Now restart Iceweasel and you should have good looking fonts:

![Screenshot-from-2015-11-05-19-13-27](/content/images/2017/08/Screenshot-from-2015-11-05-19-13-27.png)
