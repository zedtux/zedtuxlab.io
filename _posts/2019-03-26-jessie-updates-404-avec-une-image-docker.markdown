---
layout: post
title: jessie-updates 404 avec une image Docker
date: '2019-03-26 07:23:15'
tags:
- docker
---

Dans le cas où votre `Dockerfile` utilise une image source qui utilises une ancienne version de Debian (par exemple [ruby:2.2.8-slim](https://hub.docker.com/_/ruby?tab=description)) et que [le nettoyage des dépôts](https://lists.debian.org/debian-devel-announce/2019/03/msg00006.html) est fait, vous vous retrouvez avec des erreurs 404.

```
Err http://deb.debian.org jessie-updates/main amd64 Packages
  404  Not Found
Fetched 10.1 MB in 12s (793 kB/s)
W: Failed to fetch http://deb.debian.org/debian/dists/jessie-updates/main/binary-amd64/Packages  404  Not Found

E: Some index files failed to download. They have been ignored, or old ones used instead.
```

Comme c'est très bien expliqué dans [cette réponse StackOverflow](https://unix.stackexchange.com/a/210405), Le dépôt `jessie-updates` permet de faire passer plus rapidement des mises à jours de paquets pour jessie.

Aujourd'hui Debian Jessie est une ancienne version, elle ne recoit plus de nouveaux paquets, seulement les corrections de sécurités, ce qui fait que ce dépôt `jessie-updates` n'a plus de raisons d'être.

## L'idéal

Ce problème est un signal fort que vous devez passez à une version plus récente de Debian !

Passez à une version supérieure de l'image source (dans mon cas à un Ruby plus récent), ou sinon trouvez ou construisez une image avec une version plus récente de Debian.

## La réalité

La réalité fait que généralement, vous ne pouvez simplement pas mettre à jour comme ca. Ca prend du temps et ce doit être organisé.

Donc en attendant de pouvoir mettre à jour Debian, pour résoudre les erreurs 404, il vous suffit de retirer le dépôt `jessie-updates` des sources.

Pour se faire, après le `FROM` de votre `Dockerfile`, ajoutez ceci :

```
RUN bash -c 'echo -e "deb http://deb.debian.org/debian jessie main\ndeb http://security.debian.org jessie/updates main" \
             > /etc/apt/sources.list'
```
_Ici j'ai pris le contenu du fichier original et retiré la ligne qui ne m'interesse plus._

Maintenant la construction de votre image doit re-fonctionner de nouveau.