---
layout: post
title: Douane refusé du Ubuntu Software Center
date: '2014-01-13 13:02:49'
tags:
- mes-developpements
- douane
---

Je viens juste de recevoir un dernier retour de l’équipe de vérification des applications du Ubuntu Software Center (enfin de la personne qui appris en charge mon application), et Douane est simplement rejeté.

La raison du rejet est que Douane utilise un module de noyau, que je publie dans un Personal Package Archive (PPA). La personne de l’équipe de vérification craint que le paquet debian soit “hacké” et cela les rends moins ouvert a accepter mon application.

Mon point de vue sur la situation est que cette personne, dont dépend si mon application est accepté ou non, ne m’aide absolument pas. J’ai jeté un œil a sa bio, et il se décrit comme “an non-tech head”, ce qui peut expliquer pourquoi il ne me donne pas de conseil pour adapter mon application afin de la rendre disponible dans l’Ubuntu Software Center. Pour moi, ce sont ces week end, et soirées de travail pour attendre un objectif que je ne pourrais pas atteindre.

Maintenant je vais attendre de voir la réponse de cette personne, demander des conseilles (ne pas baisser les bras au premier obstacle), et si la encore il n’y a pas d’évolution, je vais devoir trouver autre chose.

Si vous avez des suggestions a me faire, n’hésitez pas, elles seront les bienvenues.