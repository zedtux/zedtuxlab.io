---
layout: post
title: Démarrer une application Elixir/Phoenix de zéro avec Docker
date: '2017-12-05 07:52:40'
tags:
- elixir
- docker
---

Un petit article pour décrire succinctement comment démarrer un projet Elixir/Phoenix lorsque l'on travail avec Docker.

Pour résumer nous allons démarrer une image Elixir où nous pourrons ainsi créer notre application (sans installer Elixir sur l'OS de la machine), puis de là vous pourrez créer le Dockerfile traditionnel.

Le but étant donc de ne jamais installer, sur sa machine, autre chose que Docker, et  donc garder une machine propre (Supprimez les images Docker et votre machine est comme neuve).

## Les étapes

### Ouvrir un terminal, et se rendre dans le dossier où vous regroupez vos projets:

```
cd ~/Developments
```

### Démarrer une image Docker pour Elixir, en y montant le dossier courant:

```
docker run --rm -it -v "$PWD":/devs --workdir=/devs elixir:1.5.2-slim sh
```

 - `--rm` permet d'effacer le container lorsque vous l'arrêterez
 - `-it` permet d'avoir une console interactive
 - `-v "$PWD":/devs` va monter le dossier en cours dans le container à la position `/devs`
 - `--workdir=/devs` définir le chemin par defaut du container à `/devs`

### Installer Phoenix

Maintenant que nous avons notre container qui tourne, et que nous avons une session `sh`, nous pouvons suivre [la documentation de Phoenix](https://hexdocs.pm/phoenix/installation.html) afin de l'installer:

```
# mix local.hex
Are you sure you want to install "https://repo.hex.pm/installs/1.5.0/hex-0.17.1.ez"? [Yn]
* creating /root/.mix/archives/hex-0.17.1

# mix archive.install https://github.com/phoenixframework/archives/raw/master/phx_new.ez
Are you sure you want to install "https://github.com/phoenixframework/archives/raw/master/phx_new.ez"? [Yn]
* creating /root/.mix/archives/phx_new

# mix phoenix.new -v
Phoenix v1.3.0
```

### Créer notre nouveau projet

Ultime étape, la création du project Phoenix:

```
# mix phx.new myapp
* creating myapp/config/config.exs
* creating myapp/config/dev.exs
* creating myapp/config/prod.exs
* creating myapp/config/prod.secret.exs
* creating myapp/config/test.exs
* creating myapp/lib/myapp/application.ex
* creating myapp/lib/myapp.ex
...

We are almost there! The following steps are missing:

    $ cd myapp
    $ mix deps.get
    $ cd assets && npm install && node node_modules/brunch/bin/brunch build

Then configure your database in config/dev.exs and run:

    $ mix ecto.create

Start your Phoenix app with:

    $ mix phx.server

You can also run your app inside IEx (Interactive Elixir) as:

    $ iex -S mix phx.server
```

Maintenant quittez le container (`CRTL + D`) et votre projet est prêt :)