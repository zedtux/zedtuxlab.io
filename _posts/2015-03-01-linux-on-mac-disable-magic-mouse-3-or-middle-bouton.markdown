---
layout: post
title: 'Linux On Mac: Disable magic mouse 3 or middle bouton'
date: '2015-03-01 16:47:12'
tags:
- linux-on-mac
---

Sometimes you will encounter the issue that right clicking with the Apple Magic Mouse will past the content of your clipboard.

This is due to the fact that you've middle clicked, meaning you've more or less clicked on the mouse's touch wheel. First solution is to always right click with your finger at the right position (it's really hard ...) or to disable the 3rd button:

    $ echo N | sudo tee /sys/module/hid_magicmouse/parameters/emulate_3button
