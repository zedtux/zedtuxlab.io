---
layout: post
title: Créer le fichier debian/compat en une ligne bash
date: '2014-01-12 20:21:37'
tags:
- howto
- packaging
---

J’avais besoin de générer simplement le fichier compat pour créer un paquet debian.

J’ai donc pondu cette petit ligne qui le fait tout simplement :

	echo dpkg -p debhelper | grep Version | cut -d ":" -f 3 | cut -d "." -f 1 | tr -d " " > debian/compat