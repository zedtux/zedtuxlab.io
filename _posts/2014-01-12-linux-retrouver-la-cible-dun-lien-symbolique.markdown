---
layout: post
title: 'Linux : Retrouver la cible d’un lien symbolique'
date: '2014-01-12 20:23:34'
tags:
- linux
- command-line
---

![Terminal_128x128](/content/images/2017/08/Terminal_128x128.png)

Je viens de tomber sur un petit problème résolu en moins de deux grâce à mon ami google :

Je devais effacer un fichier dont je ne connais que le lien symbolique. Pour commencer, retrouver le chemin du fichier symbolique est simple :

    which mon_executable

Cette commande retourne le chemin sur le disque dur en partant du root (/) jusqu’au fichier.

Maintenant, comment retrouver la cible du lien ?

On pourrait faire un

	ls -l `which mon_executable`

ce qui retourne le chemin du fichier, une flèche, puis la cible. Mais vue que c’est pour mettre dans un script, il me faut extraire ce chemin.
Faire des `cut`, `awk` ou autre est une solution… mais pas très fiable… Si l’affichage ne se fait pas pareil d’une version de ls à l’autre, mon script ne marchera plus !

La solution réside dans la commande `readlink` !

    readlink which mon_executable

Et voilà !