---
layout: post
title: My contributions to the Kitematic project
date: '2015-06-25 21:03:29'
tags:
- docker
---

A very short article to summarize my contributions to the [Kitematic](http://kitematic.com/) project, an easy and sexy user interface for [Docker](http://docker.com/).

## Linux support

My first contribution is the Linux support. I'm running Debian on my iMac since using Docker on OSX is too slow (running a cucumber feature file was taking up to 30 seconds before to execute entirely the first step ...) so I couldn't do anything else without this done.

This task wasn't hard as it was composed of only 2 points:

 * Skipping the installation wizard (Kitematic automatized the VirtualBox, boot2docker, docker-machine, etc.. installation)
 * Configure the dockernode library to use the local docker server using the unix socket

I have started this task like "Well ... how hard could be this to be done ?" and the day after I was communicating about my working fork.

Couple of people enjoy my [pull request](https://github.com/kitematic/kitematic/pull/696) (on the pull request and in [the original issue](https://github.com/kitematic/kitematic/issues/49)).

## A configurable 'Debug' tab

Now that Kitematic was running on my Linux box, I wanted to use some images like Irssi, Rainbowstream or Spotify from [Jessie Frazelle's Github repo](https://github.com/jfrazelle/dockerfiles), but when I've tried the first one, I needed to mount a volume which wasn't defined in the `Dockerfile` so I was blocked.

Looking around in the Kitematic issues I've found [the issue 376](https://github.com/kitematic/kitematic/issues/376) from [Jeffrey Morgan](https://github.com/jeffdm), one of the 3 main developers of Kitematic. While working on this task, I was bored to copy/past the output from the console of the Docker server JSON in [jsonlint](http://jsonlint.com/) in order to format it quickly so I have developed a developer tool.

As described in the pull request, now you have a checkbox to enable the debug mode which will add a **Debug** tab to the selected container which will show you the formatted JSON.

![](https://cloud.githubusercontent.com/assets/478564/8361821/915ae0a2-1b75-11e5-9d49-9a30f13ae866.png)

Now, after having performed an action which update the container configuration, I can easily checkout how the JSON looks like in order to check if the configuration is fine.
This allowed me to discover and solve [a bug in Kitematic when trying to update the `Binds` configuration of a container](https://github.com/zedtux/kitematic/commit/b67bfbbd208e68a898cc869edf06fb08e68a9c03#diff-5178d267c7db60fefd1059a67c8b82aaR41).

## Allow mounting custom volume directories

Finally the last contribution as of today is the possibility to add/remove volumes in a container no matter if they have been defined in the Dockerfile or not used to build the image (actually Volumes in Docker aren't linked to the Dockerfile at all here).

![](https://cloud.githubusercontent.com/assets/478564/8362928/0b8d9da0-1b7c-11e5-9d44-3a5cc14589e9.png)

Docker is an amazing peace of software ! It has been developed in a beautiful way as you update a container by posting on a REST API. What could be more easy ? It's the best IPC way in my opinion.

Anyway the development was more than the half about the UI than the container update. Have a look at [the pull request](https://github.com/kitematic/kitematic/pull/722) where I'm describing carefully the way to use it.