---
layout: post
title: Protégez votre serveur des scanner SSH
date: '2014-01-18 13:49:09'
tags:
- maintenance
- server
---

Combien de fois par jour votre serveur est scanné pour essayer de trouver un nom d'utilisateur et mot de passe fonctionnant ? (Brute force)

Je viens de découvrir [DenyHosts](http://denyhosts.sourceforge.net/) qui est une application qui va se charger de bloquer l'accés aux machines qui bombarde votre serveur.

Sous Ubuntu ca s'installe toujours aussi simplement:

    sudo apt-get install denyhosts

Dés l'or votre serveur est protégé des attaques sur votre SSH en mettant l'adresse IP des attaquants dans le fichier `/etc/hosts.deny` ce qui a pour effet de completement ignorer les connections de ces adresses.

Vous pourrez configurer DenyHosts afin qu'il vous envoie un email quand il bloque une adresse IP ou encore pour qu'il se synchronise et recoive une liste d'adresse IP identifié comme attaquant afin de vous protéger avant meme d'être attaqué.