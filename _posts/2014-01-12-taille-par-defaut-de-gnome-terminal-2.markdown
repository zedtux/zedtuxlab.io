---
layout: post
title: Taille par defaut de gnome-terminal
date: '2014-01-12 19:25:45'
tags:
- linux
- gnome
---

![gnome_terminal](/content/images/2017/08/gnome_terminal.png)

J’avais écris un article pour expliquer comment modifier la taille par défaut du terminal d’Ubuntu dans [cet article](/taille-par-defaut-de-gnome-terminal/).

Ce soir, je viens d’installer Ubuntu Lucid Lynx (10.04), la futur version d’Ubuntu, et voilà ce que je viens de découvrir :

![terminal_default_size](/content/images/2017/08/terminal_default_size.png)

On peux voire a la fin du panneau de configuration 2 champs pour configurer la taille par défaut !

Voilà… c’est tout :-p