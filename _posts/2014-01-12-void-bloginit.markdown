---
layout: post
title: void Blog::init();
date: '2014-01-12 01:43:48'
---

Salut à toi visiteur.

Voici donc l’ouverture de mon blog… que personne n’attendais, donc tout vas bien.
Je ne sais pas si il y aura beaucoup à dire, donc, faut pas t’attendre à devoir venir toutes les heures lire un nouvel article.

Bon, globalement, j’essaie d’être développeur. ( Ca s’est peut-être vue au titre de mon article ? ).

J’aime beaucoup l’Open Source, au point d’être membre de l’April ( depuis ce mois ci d’ailleurs ), ainsi donc, j’aime Linux.
Je ne déteste pas windows, j’ai eut m’être amusé grâce à lui sur des serveurs nippons et Américain, mais pour ma machine perso ( et celle de ma copine, ou encore de mes Parents ) il est inconcevable d’avoir un OS payant quand il est buggé.
J’ai donc découvert le top ( Gratuit, non buggé… Bisard non ? ) Linux, avec toute sa philosophie ( j’en ai pas trouvé sous windows ), et surtout … sa communautée ! ( Non, Je ne suis pas communiste ! ).

Arrêtons là les conneries.
Ce que je voulais dire c’est que je vais poster des articles sur le développement, le libre / l’Open Source, Linux en gros.
Cela dit, je n’ai pas que ca dans ma vie. La musique joue un rôle important, tout comme la Bretagne, d’où je suis originaire.

Sur ceux, j’ai du taff, donc espèrons que je vais avoir de l’inspiration ^^