---
layout: post
title: 'FirefoxNotify: Notification des téléchargement fini'
date: '2014-01-12 12:21:52'
tags:
- linux
---

![FirefoxNotify](/content/images/2014/Jan/firefoxnotify.jpg)

Voici une extension pour Firefox, afin de permettre à ce dernier d’utiliser le nouveau système de notification implanté dans Ubuntu Jaunty.
Il est utilisé pour afficher les téléchargements terminés.

Son petit nom est __FirefoxNotify__ et vous le trouverez à cette adresse: https://addons.mozilla.org/fr/firefox/addon/9622

(lien direct: https://addons.mozilla.org/fr/firefox/downloads/file/52064/firefoxnotify-1.5.1r2-fx-linux.xpi?confirmed)

Cela dit, vous aurez toujours la notification de firefox qui s’affichera en bas à droite de l’écran.
Si vous désirez n’avoir que FirefoxNotify, il suffit de se rendre à l’adresse __about:config__ puis de chercher la ligne :

    browser.download.manager.showAlertOnComplete

Et voilà !! :)