---
layout: post
title: 'Bazaar: KnitPackRepository is not compatible with CHKInventoryRepository'
date: '2014-01-12 18:37:48'
tags:
- vcs
- bazaar
---

![bazaar_logo](/content/images/2017/08/bazaar_logo1.png)

Aujourd’hui, petit soucis pendant un push de ma branche sur mon serveur :

    [14:01:53] sh58[zedtux@zUbuntu][1774]~/Developpement/bzr/master $ bzr push :parent
    zedtux@bzr.zedroot.org’s password:
    bzr: ERROR: KnitPackRepository(‘sftp://zedtux@bzr.zedroot.org/~/Elcaro/.bzr/repository/’)
    is not compatible with
    CHKInventoryRepository(‘file:///home/zedtux/Developpement/bzr/.bzr/repository/’)
    different rich-root support
    [14:02:05] sh59[zedtux@zUbuntu][1775]~/Developpement/bzr/master $

Après avoir poussé mon cris d’étonnement très particulier, j’ai effectué quelques recherches et hop ! Résolut ! Ouf !

## La solution

Le problème est bien décrit dans le message d’erreur… mais ils auraient put mettre une explication ou un lien vers une page qui explique…
Tout vient du fais que si vous travaillez sur un poste avec bazaar version 1.x par exemple, et à la maison avec bazaar 2.x… vous allez avoir ce problème de non compatibilité.

Pas de panic, encore une fois !

Un petit update du repository en version 2, et tout va rentrer dans l’ordre ! :)
(Il faut donc exécuter `bzr upgrade –2a :parent`)

    [14:02:05] sh59[zedtux@zUbuntu][1775]~/Developpement/bzr/master $ bzr upgrade –2a :parent
    zedtux@bzr.zedroot.org’s password:
    starting upgrade of sftp://zedtux@bzr.zedroot.org/~/Elcaro/
    making backup of sftp://zedtux@bzr.zedroot.org/~/Elcaro/.bzr
    to sftp://zedtux@bzr.zedroot.org/~/Elcaro/backup.bzr
    starting repository conversion
    repository converted
    finished
    [14:06:16] sh60[zedtux@zUbuntu][1776]~/Developpement/bzr/master $ bzr push :parent
    zedtux@bzr.zedroot.org’s password:
    Pushed up to revision 26.
    [14:06:32] sh61[zedtux@zUbuntu][1777]~/Developpement/bzr/master
    $