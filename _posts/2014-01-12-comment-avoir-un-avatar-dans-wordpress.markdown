---
layout: post
title: Comment avoir un avatar dans WordPress ?
date: '2014-01-12 18:09:33'
---

![LastFM Neighbour](/content/images/2017/08/lastfm_neighbour.png)

La première fois que j’ai utilisé WordPress, et que j’ai posté un commentaire, j’ai tout de suite vue que j’avais un avatar inconnu :

![Gravatar unknown](http://www.gravatar.com/avatar/ad516503a11cd5ca435acc9bb6523536?s=32)

Tout de suite, je suis aller voire mon compte dans wordpress … mais pas moyen de trouver un élément pour uploader un avatar !

Alors j’ai googlelé et la réponse m’es venu: **Gravatar** !

L’idée de Gravatar est de centraliser son avatar pour pouvoir l’utiliser sur différents services, comme WordPress, sans avoir à ré envoyer votre avatar pour chaque sites.

L’idée est bonne.

Le tout est lié à votre adresse email.

Donc lorsque vous remplissez un commentaire, vous entrez forcément votre adresse email, et donc, gravatar est directement appelé pour aller chercher votre avatar.

## Avoir son avatar

Il vous faut créer un compte sur le site de Gravatar.com, puis envoyer votre avatar préféré et c’est tout !

Ensuite, tout les commentaire que vous avez posté dans les blogs du monde entier (avec l’adresse email correspondante) afficherons fièrement votre avatar !

Il est bon de savoir aussi, que vous pouvez avoir plusieurs avatar pour le même compte si vous possédé plusieurs adresse email.