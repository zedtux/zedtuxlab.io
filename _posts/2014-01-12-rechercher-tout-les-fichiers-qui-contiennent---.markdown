---
layout: post
title: Rechercher tout les fichiers qui contiennent…
date: '2014-01-12 16:50:54'
tags:
- linux
- command-line
---

![Rechercher dans des fichiers](/content/images/2017/08/find_with_rgrep.png)

Moi il m’arrive très souvent de vouloir savoir dans quels fichiers se trouve tel mot ou ensemble de mots.
Mais comment faire ?

Je ne connais pas d’outils graphique pour le faire, mais en revanche, je connais des outils console !
( Ca pourrait être une bonne idée de projet… Intégrer dans la recherche de nautilus la recherche de string dans un ensemble de fichiers… ;-) )

## L’outil magique: Grep !

[Grep](http://www.gnu.org/savannah-checkouts/gnu/grep/manual/grep.html) est un outils vraiment puissant !

Pour ceux qui ne connaissent pas, en gros, c’est comme un entonnoir. Vous lui passer des tas et des tas de données, et vous lui précisez comment filtrer tout ca, et il vous retourne uniquement les données correspondant à vos critères.

De base, grep n’affiche pas les numéro de lignes où se situent les résultats, et ne met pas en couleur les résultats.
Je vous propose de remédier définitivement à ca en modifier votre fichier `~/.bash_aliases` comme ceci:

Ajouter cette ligne:

    alias grep=’grep –color -n’

Ouvrez un nouveau terminal (pour qu’il prenne notre alias en compte), et maintenant, quand vous taperez grep, automatiquement ca voudra dire grep –color -n.

## Rechercher un mot dans tout les fichiers d’un dossier

Premier exercice, rechercher le mot test dans un dossier !

Il suffit pour ca de taper:

    grep “test” *

Et vous devriez obtenir un résultat similaire à ceci:

    COPYING:625: If you develop a new program, and you want it to be of the greatest
    Doxyfile:116:# to NO the shortest path that makes the file name unique will be used.
    Doxyfile:441:# disable (NO) the test list. This list is created by putting \test
    Doxyfile:589:# blank the following patterns are tested:
    Doxyfile:646:# against the file with absolute path, so to exclude all test directories
    Doxyfile:647:# for example use the pattern */test/*

Le tout en couleur ! :)

Et pour la recherche récursif ?

Et bien c’est EXTRÊMEMENT simple ! :)
Il suffit d’utiliser Grep Récursif !! :D

Donc, au lieu de taper grep, vous tapez rgrep ! :)

Par contre… si vous désirez la couleur et les numéro de lignes, il faut re-modifier le fichier `~/.bash_aliases` comme ceci:

Ajouter cette ligne:

    alias rgrep=’rgrep –color -n’

Ou alors, vous ajouter à l’alias grep l’argument -r !
Donc la ligne :

    alias grep=’grep –color -n’

Deviendrait:

    alias grep=’grep –color -n -r’

L’inconvénient de cette méthode, c’est que grep recherche TOUT LE TEMPS en récursif… ce qui n’est pas forcément intéressant.