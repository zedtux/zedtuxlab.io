---
layout: post
title: 'PlayOnLinux: Black & White 2'
date: '2014-01-12 19:57:09'
tags:
- linux
- game
- wine
---

![Black-White-2-1-icon](/content/images/2017/08/Black-White-2-1-icon.png)

Peut-être avez vous remarqué ma page [Scripts PlayOnLinux](http://blog.zedroot.org/2010/08/playonlinux-black-white-2/scripts-playonlinux/) où je publie mes contribution au projet [PlayOnLinux](http://www.playonlinux.com/) qui pour rappelle est un outils qui permet la gestion de multiple environnement [Wine](http://www.winehq.org/)[[Wikipedia](http://fr.wikipedia.org/wiki/Wine)]. Ce qui permet de configurer une instance de Wine spécifiquement pour tel application/jeu.

PlayOnLinux s’occupe de gérer les multiples environnements, tendis que les développeurs (de scripts) s’occupe d’automatiser l’installation du programme/jeu et de la configuration de l’instance de Wine pour que le jeu fonctionne de manière optimale.

C’est donc ici ce que j’ai fais pour le jeu [Black & White 2](http://lionhead.com/Games/BW2/Default.aspx). Le jeu sortis en 2005, est le seule du genre: Vous incarnez un dieu, et vous dirigez vos adorateurs, ainsi qu’une créature à votre image: Bien au Mal.

## Résultat

Le jeu s’installe parfaitement, les patchs aussi.

La vidéo 3D du démarrage fonctionne sans bug, ainsi que le menu. Dans le jeu, par contre, un carré transparent gène un peu (on peux voire au travers des montagnes dans ce carré), mais le jeu est jouable.

![](http://imageshack.us/scaled/landing/3/bw22.png)
![](http://imageshack.us/scaled/landing/571/bw23.png)