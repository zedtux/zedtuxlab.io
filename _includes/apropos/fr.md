## Mon parcours

Autodidacte dans la programmation en général, je suis plus orienté sur le développement web de par mon activité professionnelle, je continue de développer d'autres choses dans un tout autre domaine.

Mon parcours professionnel commence en région parisienne où je travaille, en alternance pendant 2 années, dans un service informatique chez [Amec SPIE](https://www.spie.com/), pour y préparer des machines Windows et effectuer des maintenances réseaux.

Par la suite, je déménage dans l'Est de la France et je vais travailler au Luxembourg à partir de 2006, comme développeur chez [KNEIP](https://www.kneip.com/), une entreprise du secteur bancaire.
Je vais y travailler une dizaine d'années où je vais évoluer autant dans les languages de programmations, que dans mes rôles.
Chez KNEIP j'y ai travaillé principalement avec Ruby On Rails.

Je quittes KNEIP pour aller travailler chez [Yellow.lu](https://www.yellow.lu) ([Linc SA](https://www.linc.lu/)) pour 1 an avant de retourner faire une année supplémentaire chez KNEIP.

C'est en 2017 que je quitte définitivement KNEIP pour aller chez [Pharmony SA](https://www.pharmony.eu) où je travaillais avec Ruby On Rails, mais aussi React, Bootstrap (Commis), ou encore React-Native (Application mobile propriétaire Companion).

Aujourd'hui j'ai créé ma propre entreprise, depuis Août 2020, en Bretagne là où j'habite maintenant, comme développeur web/mobile à distance.

Mon parcours est consultable en détails sur [ma page Linkedin](https://www.linkedin.com/in/guillaumehain).

## Projets

Voici une liste non exhaustive des projets publiques que j'ai réalisés:

 * [Brewformulas.org](https://gitlab.com/zedtux/brewformulas.org) - Liste des formules Homebrew avec une description récupérée de façon automatique depuis le site officiel de la formule.
 * [Douane](https://douaneapp.com/) - Par-feu logiciel pour Linux (bloquer le trafic pour une application précise).
 * [Commis](https://gitlab.com/pharmony/commis) - Un outil d'orchestration pour Chef/Knife/Knife-zero.

Vous pouvez retrouver tous mes dépôts publiques sur [Gitlab](https://gitlab.com/users/zedtux/projects).
